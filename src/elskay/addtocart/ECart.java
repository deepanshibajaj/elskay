package elskay.addtocart;



import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import elskay.app.DJSONParserPost;
import elskay.app.MainTabActivity;
import elskay.app.R;
import elskay.checkout.UserAddress;
import elskay.login.DatabaseHandler;


public class ECart extends Activity {
	
	ArrayList<ECartList> actorsList;
	
	ECartAdapter adapter;
	JSONObject object;
	Button checkoutaddress;
	 ArrayList<String> ProductNameArray;
	 String productname;

	
	private static String url = "http://128.199.176.163/elskayapp/elskayshop/product_getfromcart.php";
	
	//private static final String TAG_NEW = "totalprice";
   	private static final String TAG_OS = "product";
    private static final String TAG_NAME = "name";
    private static final String TAG_IMAGE = "image1";
    private static final String TAG_PRICE = "price";
    private static final String TAG_PRODUCTID = "product_id";
    private static final String TAG_PRODUCTSCOUNT = "count";

	
	String uid;
	ImageView cartempty;
	int countproduct;
	TextView totalprice;
	String countproducts;
	TextView total;
	
	StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
	 SharedPreferences settings;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.cart_list);
		
		DatabaseHandler db = new DatabaseHandler(getApplicationContext());
        HashMap<String,String> user = new HashMap<String, String>();
        user = db.getUserDetails();
        uid= user.get("uid");
        settings = PreferenceManager.getDefaultSharedPreferences(this);
        
        
        
		actorsList = new ArrayList<ECartList>();
		
		if(isNetworkAvailable()){
            // do network operation here     
			new JSONAsyncTask().execute();
         }else{
            Toast.makeText(getApplicationContext(), "No Available Network. Please try again later", Toast.LENGTH_LONG).show();
            return;
          
         }
		
		
		cartempty = (ImageView)findViewById(R.id.cartimage);
        ImageButton back = (ImageButton)findViewById(R.id.backbutn);
        checkoutaddress = (Button)findViewById(R.id.checkoutbutton);
        totalprice = (TextView)findViewById(R.id.totalprice);
        total = (TextView)findViewById(R.id.totall);
        Button cart = (Button)findViewById(R.id.cart);
        
        Typeface custom_font3 = Typeface.createFromAsset(getAssets(), "AvenirNextLTPro-Regular.otf");
        cart.setTypeface(custom_font3);
        
        Typeface custom_font = Typeface.createFromAsset(getAssets(), "AvenirNextLTPro-Regular.otf");
        total.setTypeface(custom_font);
        
        Typeface custom_font1 = Typeface.createFromAsset(getAssets(), "AvenirNextLTPro-Demi.otf");
        totalprice.setTypeface(custom_font1);
        
        Typeface custom_font2 = Typeface.createFromAsset(getAssets(), "AvenirNextLTPro-Demi.otf");
        checkoutaddress.setTypeface(custom_font2);
        
        
        checkoutaddress.setOnClickListener(new OnClickListener()
		{

		@Override
		public void onClick(View v) {
			
			 String total=totalprice.getTag().toString();
			 
			 Intent myIntent = new Intent(getApplicationContext(), UserAddress.class); 
			/* for (String value : ProductNameArray) {
					// System.out.println(value); 
				 myIntent.putExtra("strings", value);
				 
					} */
			 myIntent.putStringArrayListExtra("productarray", ProductNameArray);
			 myIntent.putExtra("totalamount", total);
			 myIntent.putExtra("productcount", countproducts);
			 Bundle b=new Bundle();
             b.putString("condition","CART");
             myIntent.putExtras(b);
             startActivityForResult(myIntent, 0);
             finish();

		
		}


		}); 
        
        
        back.setOnClickListener(new OnClickListener()
		{

		@Override
		public void onClick(View v) {
			
			 Intent myIntent = new Intent(getApplicationContext(), MainTabActivity.class);
             startActivityForResult(myIntent, 0);
             finish();

			 

		}


		}); 
	     
		
		ListView listview = (ListView)findViewById(android.R.id.list);
		adapter = new ECartAdapter(getApplicationContext(), R.layout.cart_list_item, actorsList);
		
		listview.setAdapter(adapter);
		
		
        // on seleting single product
        // launching pdfviewer activity
		/*listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
	        @Override
	        public void onItemClick(AdapterView<?> parent, View rowView, int positon,long id) {
	            Toast.makeText(rowView.getContext(), "Product Removed", Toast.LENGTH_LONG).show();
	            removeListItem(rowView,positon);
	        }
	    }); */
	}

	/*protected void removeListItem(View rowView, final int positon) {
		 
	      final Animation animation = AnimationUtils.loadAnimation(ECart.this,android.R.anim.slide_out_right); 
	      rowView.startAnimation(animation);
	      Handler handle = new Handler();
	      handle.postDelayed(new Runnable() {
	 
	        @Override
	          public void run() {
	        
	              
	          	 
            	   	String result = null;
            	   	InputStream is = null;
            	  
            
            	   	ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

            	    String productid= actorsList.get(positon).getcatid();
            	   
   				
            	   	nameValuePairs.add(new BasicNameValuePair("productid",productid));
            	  nameValuePairs.add(new BasicNameValuePair("userid",uid));
            	
            	    
          

            	   	StrictMode.setThreadPolicy(policy); 


        	//http post
        	try{
        	        HttpClient httpclient = new DefaultHttpClient();
        	        HttpPost httppost = new HttpPost("http://128.199.176.163/elskayapp/elskayshop/product_deletefromcart.php");
        	        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
        	        HttpResponse response = httpclient.execute(httppost); 
        	        HttpEntity entity = response.getEntity();
        	        is = entity.getContent();

        	        Log.e("log_tag", "connection success ");
        	       
        	        //Toast.makeText(getApplicationContext(), "", Toast.LENGTH_SHORT).show();
        	   }
                           
           
        	
        	catch(Exception e)
        	{
        	        Log.e("log_tag", "Error in http connection "+e.toString());
        	        Toast.makeText(getApplicationContext(), "Connection fail", Toast.LENGTH_SHORT).show();

        	}
        	//convert response to string
        	try{
        	        BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
        	        StringBuilder sb = new StringBuilder();
        	        String line = null;
        	        while ((line = reader.readLine()) != null) 
        	        {
        	                sb.append(line + "\n");
        	             
        	              Intent intent = getIntent();
        	            overridePendingTransition(0, 0);
        	            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        	            finish();
        	           overridePendingTransition(0, 0);
        	            startActivity(intent);  
        	         
        	          
      	             
        	        }
        	        is.close();

        	        result=sb.toString();
        	      Log.i("response##########",result);
        	   // Toast.makeText(getApplicationContext(),result,Toast.LENGTH_LONG).show();
        	      
           
        	}
        	catch(Exception e)
        	{
        	       Log.e("log_tag", "Error converting result "+e.toString());
           	}


        	try{
        		
        					JSONObject json_data = new JSONObject(result);

        	                CharSequence w= (CharSequence) json_data.get("re");
        	             
        	              
        	               // Toast.makeText(getApplicationContext(), w, Toast.LENGTH_SHORT).show();
        	                

        	      
        	     }
        	catch(JSONException e)
        	   {
        	        Log.e("log_tag", "Error parsing data "+e.toString());
        	        Toast.makeText(getApplicationContext(), "JsonArray fail", Toast.LENGTH_SHORT).show();
        	    }
        	
        	actorsList.remove(positon);
        	adapter.notifyDataSetChanged();
              animation.cancel();
	              
	          }
	      },500);
	 
	  }*/
	class JSONAsyncTask extends AsyncTask<String, String, JSONObject> {
		
		 private ProgressDialog pDialog;
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			  pDialog = new ProgressDialog(ECart.this);
	            pDialog.setMessage("Add some amazing stuff from our collection to cart..");
	            pDialog.setIndeterminate(false);
	            pDialog.setCancelable(true);
	            pDialog.show(); 
		}
		
		@Override
		 protected JSONObject doInBackground(String... args)  {
			
				
				
				DJSONParserPost jParser = new DJSONParserPost();
	    	   List<NameValuePair> params = new ArrayList<NameValuePair>();
	    	   Log.i("params@@@@@@@",uid);
	  	       params.add(new BasicNameValuePair("uid", uid));
	  	      /*  for(String productname : ProductNameArray) {

	  	        	params.add(new BasicNameValuePair("PRODUCTNAME[]", productname));

	  	         } */
	  	     // System.out.print(params);
	  		   JSONObject json = jParser.getJSONFromUrl(url,params);
	  		   
	  		   //System.out.print(params.toString());
	    		return json;
	    		

		}
		
		
		@Override
        protected void onPostExecute(final JSONObject json) {
			pDialog.dismiss();
			adapter.notifyDataSetChanged();
	    		 try {
				
				
	    			
	    			 String price = json.getString("price");
	    			 Log.i("totalprice",price);
	    			 totalprice.setText("₹"+price);
	    			 totalprice.setTag(price);
	                
	    			 countproducts = json.getString("count");
						countproduct= Integer.parseInt(countproducts);
	    			 
					ProductNameArray = new ArrayList<String>();
					JSONArray jarray = json.getJSONArray(TAG_OS);
					for (int i = 0; i < jarray.length(); i++) {
						 object = jarray.getJSONObject(i);
						 ProductNameArray.add(object.toString());
						
						 
							
						ECartList actor = new ECartList();
						
						actor.setName(object.getString(TAG_NAME));
						actor.setImage(object.getString(TAG_IMAGE));
						actor.setcatid(object.getString(TAG_PRODUCTID));
						actor.setPrice(object.getString(TAG_PRICE));
						/*String countproducts = object.getString(TAG_PRODUCTSCOUNT);
						countproduct= Integer.parseInt(countproducts);*/
						productname=object.getString(TAG_NAME);
						actorsList.add(actor);
					}
	    		 }
	    		 
	    		 catch (JSONException ignored) {
	      			 
	      			 
	      		 }
	      			if (object == null) {
	      			    cartempty.setVisibility(View.VISIBLE);
	      			  countproduct= Integer.parseInt("0");
						SharedPreferences.Editor editor = settings.edit(); 
                      editor.putInt("counts",countproduct);
                      editor.commit();
                      checkoutaddress.setEnabled(false); 
	      			}
	      			else {
	      				cartempty.setVisibility(View.GONE);
	      				SharedPreferences.Editor editor = settings.edit(); 
                        editor.putInt("counts",countproduct);
                        editor.commit();
	      			}

	       		}
                 
                 
	    }
	    
	 public boolean isNetworkAvailable() {
	       ConnectivityManager connectivityManager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
	       NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	       return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	   }

		     		
	}
	
	
	
	
	
	


