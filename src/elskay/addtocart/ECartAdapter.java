package elskay.addtocart;


import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import elskay.app.R;
import elskay.login.DatabaseHandler;

public class ECartAdapter extends ArrayAdapter<ECartList> {
	ArrayList<ECartList> actorList;
	LayoutInflater vi;
	int Resource;
	ViewHolder holder;
	String uid;

	
	StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

	public ECartAdapter(Context context, int resource, ArrayList<ECartList> objects) {
		super(context, resource, objects);
		vi = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		Resource = resource;
		actorList = objects;
		
		
		
	}
 
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// convert view = design
		View v = convertView;
		

				

		
		if (v == null) {
			holder = new ViewHolder();
			v = vi.inflate(Resource, null);
			holder.cproductimage = (ImageView) v.findViewById(R.id.cproductimage);
			holder.cproductname = (TextView) v.findViewById(R.id.cproductname);
			holder.cproductprice = (TextView) v.findViewById(R.id.cproductprice);
			holder.cproductremove = (Button) v.findViewById(R.id.productremove);
			
			DatabaseHandler db = new DatabaseHandler(getContext());
	        HashMap<String,String> user = new HashMap<String, String>();
	        user = db.getUserDetails();
	        uid= user.get("uid");
	        
			v.setTag(holder);
		} else {
			holder = (ViewHolder) v.getTag();
		}
		holder.cproductimage.setImageResource(R.drawable.ic_launcher);
		
		  Picasso.with(getContext())
          .load(actorList.get(position).getImage())
          .into(holder.cproductimage);
		  
		holder.cproductprice.setText("₹"+actorList.get(position).getPrice());
		Typeface custom_font4 = Typeface.createFromAsset(vi.getContext().getAssets(), "AvenirNextLTPro-Regular.otf");
		holder.cproductprice.setTypeface(custom_font4);
		
		holder.cproductname.setText(actorList.get(position).getName());
		Typeface custom_font3 = Typeface.createFromAsset(vi.getContext().getAssets(), "AvenirNextLTPro-Regular.otf");
		holder.cproductname.setTypeface(custom_font3);
		
		
		Typeface custom_font5 = Typeface.createFromAsset(vi.getContext().getAssets(), "AvenirNextLTPro-Demi.otf");
		holder.cproductremove.setTypeface(custom_font5);
		holder.cproductremove.setOnClickListener(new View.OnClickListener()
        {   
			
               public void onClick(View arg)
               {
            	   Log.d("TEST", "TEST");
            	  
   	            removeListItem(arg, position);
                  /* new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
               	
                          	   	 
                      //holder.likeid.setImageResource(R.drawable.faviconblack);      	 
            	   	String result = null;
            	   	InputStream is = null;
            	  
            	   
            	   String productid= actorList.get(position).getcatid();
            	   notifyDataSetChanged();
            
            	   	ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

            	 
   				
            	   	nameValuePairs.add(new BasicNameValuePair("productid",productid));
            	  nameValuePairs.add(new BasicNameValuePair("userid",uid));
            	

            	   	StrictMode.setThreadPolicy(policy); 


        	//http post
        	try{
        	        HttpClient httpclient = new DefaultHttpClient();
        	        HttpPost httppost = new HttpPost("http://128.199.176.163/elskayapp/elskayshop/product_deletefromcart.php");
        	        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
        	        HttpResponse response = httpclient.execute(httppost); 
        	        HttpEntity entity = response.getEntity();
        	        is = entity.getContent();

        	        Log.e("log_tag", "connection success ");
        	       
        	        //Toast.makeText(getApplicationContext(), "", Toast.LENGTH_SHORT).show();
        	   }
                           
           
        	
        	catch(Exception e)
        	{
        	        Log.e("log_tag", "Error in http connection "+e.toString());
        	        Toast.makeText(getContext(), "Connection fail", Toast.LENGTH_SHORT).show();

        	}
        	//convert response to string
        	try{
        	        BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
        	        StringBuilder sb = new StringBuilder();
        	        String line = null;
        	        while ((line = reader.readLine()) != null) 
        	        {
        	        	 Intent myIntent = new Intent();
        	        	 myIntent = new Intent(getContext(), ECart.class);
        				 myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        				 vi.getContext().startActivity(myIntent);
        				 
      	             
        	        }
        	        is.close();

        	        result=sb.toString();
        	      Log.i("response##########",result);
        	    Toast.makeText(getContext(),result,Toast.LENGTH_LONG).show();
        	      
           
        	}
        	catch(Exception e)
        	{
        	       Log.e("log_tag", "Error converting result "+e.toString());
           	}


        	try{
        		
        					JSONObject json_data = new JSONObject(result);

        	                CharSequence w= (CharSequence) json_data.get("re");
        	             
        	                Toast.makeText(getContext(), w, Toast.LENGTH_SHORT).show();
        	                

        	      
        	     }
        	catch(JSONException e)
        	   {
        	        Log.e("log_tag", "Error parsing data "+e.toString());
        	        Toast.makeText(getContext(), "JsonArray fail", Toast.LENGTH_SHORT).show();
        	    }
        	 Thread.sleep(3000); // Let's wait for some time
                            } catch (Exception e) {
                                 
                            }
                            //progress.dismiss();
                          
                           
                        }
                    }).start(); */
                     
                } 
               
            }); 
		
	 
		return v;

	}

	static class ViewHolder {
		
		public ImageView cproductimage;
		public TextView cproductname;
		public TextView cproductprice;
	    public Button cproductremove;
	    

	}

	private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
		ImageView bmImage;

		public DownloadImageTask(ImageView bmImage) {
			this.bmImage = bmImage;
		}

		protected Bitmap doInBackground(String... urls) {
			String urldisplay = urls[0];
			Bitmap mIcon11 = null;
			try {
				InputStream in = new java.net.URL(urldisplay).openStream();
				mIcon11 = BitmapFactory.decodeStream(in);
			} catch (Exception e) {
				Log.e("Error", e.getMessage());
				e.printStackTrace();
			}
			return mIcon11;
		}

		protected void onPostExecute(Bitmap result) {
			bmImage.setImageBitmap(result);
		}

	}
	protected void removeListItem(final View rowView, final int position) {
		 
		 /*final Animation animation = AnimationUtils.loadAnimation(getContext(),android.R.anim.fade_out); 
  	      rowView.startAnimation(animation);*/
	     
	      Handler handle = new Handler();
	      handle.postDelayed(new Runnable() {
	 
	        @Override
	          public void run() {
	        
	        	
          	   	String result = null;
          	   	InputStream is = null;
          	  
          
          	  String productid= actorList.get(position).getcatid();
       	  
       
       	   	ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

       	 
				
       	   	nameValuePairs.add(new BasicNameValuePair("productid",productid));
       	  nameValuePairs.add(new BasicNameValuePair("userid",uid));
          	
          	    
        

          	   	StrictMode.setThreadPolicy(policy); 


      	//http post
      	try{
      	        HttpClient httpclient = new DefaultHttpClient();
      	        HttpPost httppost = new HttpPost("http://128.199.176.163/elskayapp/elskayshop/product_deletefromcart.php");
      	        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
      	        HttpResponse response = httpclient.execute(httppost); 
      	        HttpEntity entity = response.getEntity();
      	        is = entity.getContent();

      	        Log.e("log_tag", "connection success ");
      	       
      	        //Toast.makeText(getApplicationContext(), "", Toast.LENGTH_SHORT).show();
      	   }
                         
         
      	
      	catch(Exception e)
      	{
      	        Log.e("log_tag", "Error in http connection "+e.toString());
      	        Toast.makeText(getContext(), "Connection fail", Toast.LENGTH_SHORT).show();

      	}
      	//convert response to string
      	try{
      	        BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
      	        StringBuilder sb = new StringBuilder();
      	        String line = null;
      	        while ((line = reader.readLine()) != null) 
      	        {
      	                sb.append(line + "\n");
      	             
      	         	 Intent myIntent = new Intent();
    	        	 myIntent = new Intent(getContext(), ECart.class);
    				 myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    				 vi.getContext().startActivity(myIntent);
    				 
      	         
      	          
    	             
      	        }
      	        is.close();

      	        result=sb.toString();
      	      Log.i("response##########",result);
      	   // Toast.makeText(getApplicationContext(),result,Toast.LENGTH_LONG).show();
      	      
         
      	}
      	catch(Exception e)
      	{
      	       Log.e("log_tag", "Error converting result "+e.toString());
         	}


      	try{
      		
      					JSONObject json_data = new JSONObject(result);

      	                CharSequence w= (CharSequence) json_data.get("re");
      	             
      	              
      	               // Toast.makeText(getApplicationContext(), w, Toast.LENGTH_SHORT).show();
      	                

      	      
      	     }
      	catch(JSONException e)
      	   {
      	        Log.e("log_tag", "Error parsing data "+e.toString());
      	        Toast.makeText(getContext(), "JsonArray fail", Toast.LENGTH_SHORT).show();
      	    }
      	
      	actorList.remove(position);
      	notifyDataSetChanged();
           // animation.cancel();
	              
	          }
	      },500);
	 
	  }

}