package elskay.addtocart;


public class ECartList {

	private String name;
	private String description;
	private String image;
	private String catid;
	private String price;
	private String productscount;

	public ECartList() {
		// TODO Auto-generated constructor stub
	}

	public ECartList(String name, String description ,String image,String catid,String price,String productscount) {
		super();
		this.name = name;
		this.description = description;
		this.image = image;
		this.catid = catid;
		this.price = price;
		this.productscount = productscount;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getProductsCount() {
		return productscount;
	}

	public void setProductsCount(String productscount) {
		this.productscount = productscount;
	}
	
	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}


	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}
	
	public String getcatid() {
		return catid;
	}

	public void setcatid(String catid) {
		this.catid = catid;
	}

}
