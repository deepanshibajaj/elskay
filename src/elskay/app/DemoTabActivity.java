package elskay.app;


import android.app.TabActivity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import elskay.category.ECategories;
import elskay.feed.FeedElskay;
import elskay.login.Login;
import elskay.login.SessionManager;
import elskay.login.UserFunctions;
import elskay.login.UserProfile;
import elskay.styleadvice.AdviceTabs;



//@SuppressLint("NewApi") @SuppressWarnings("deprecation")
public class DemoTabActivity extends TabActivity  {
private SessionManager session;


public void onCreate(Bundle savedInstanceState) {
super.onCreate(savedInstanceState);
setContentView(R.layout.demotab);
session = new SessionManager(getApplicationContext());

Resources ressources = getResources(); 
TabHost tabHost = getTabHost(); 
// Android tab
Intent intentAndroid = new Intent().setClass(this,FeedElskay.class);
TabSpec tabSpecAndroid = tabHost
.newTabSpec("Feed")
.setIndicator("Feed")
.setContent(intentAndroid);


// Apple tab
Intent intentApple = new Intent().setClass(this, ECategories.class);
TabSpec tabSpecApple = tabHost
.newTabSpec("SHOP")
.setIndicator("SHOP")
.setContent(intentApple); 
/*TabSpec songspec = tabHost.newTabSpec("Songs");        
       songspec.setIndicator("", getResources().getDrawable(R.drawable.diamond));
       Intent songsIntent = new Intent(this, ListDemo.class);
       songspec.setContent(songsIntent); */
        
//Apple tab
Intent intentAp = new Intent().setClass(this, AdviceTabs.class);
TabSpec tabSpecAp = tabHost
.newTabSpec("ADVICE")
.setIndicator("ADVICE")
.setContent(intentAp);  
// add all tabs 
   
tabHost.addTab(tabSpecAndroid);
tabHost.addTab(tabSpecApple);
tabHost.addTab(tabSpecAp);
getActionBar().setHomeButtonEnabled(true);
  //tabHost.addTab(songspec);
/*tabHost.getTabWidget().getChildAt(0).getLayoutParams().height = 150;
tabHost.getTabWidget().getChildAt(0).setBackgroundColor(Color.parseColor("#cccccc"));
tabHost.getTabWidget().getChildAt(1).getLayoutParams().height = 150; 
tabHost.getTabWidget().getChildAt(1).setBackgroundColor(Color.parseColor("#cccccc")); 
//tabHost.getTabWidget().getChildAt(2).getLayoutParams().height = 150; 
//tabHost.getTabWidget().getChildAt(2).setBackgroundColor(Color.parseColor("#000000")); 
for(int i=0;i<tabHost.getTabWidget().getChildCount();i++) 
        {
            TextView tv = (TextView) tabHost.getTabWidget().getChildAt(i).findViewById(android.R.id.title);
            tv.setTextColor(Color.parseColor("#ffffff"));
        }

*/
//set Windows tab as default (zero based)
tabHost.setCurrentTab(3);

Window window = getWindow();

// clear FLAG_TRANSLUCENT_STATUS flag:
window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
//window.setStatusBarColor(getResources().getColor(R.color.sab_yellow));
}
 @Override
      public boolean onCreateOptionsMenu(Menu menu) {
          getMenuInflater().inflate(R.menu.main, menu);
          return true;
      }
       public boolean onOptionsItemSelected(MenuItem item) {
          switch (item.getItemId()) {
          case R.id.action_settings: 
        
         UserFunctions logout = new UserFunctions();
              logout.logoutUser(getApplicationContext());
              session.setLogin(false);
              Intent login = new Intent(getApplicationContext(),Login.class);
              login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
              startActivity(login);
              finish();
              return true;
              
          case R.id.notification:
              Intent profile = new Intent(getApplicationContext(),UserProfile.class);
              startActivity(profile);
              finish();
        
             
              return true;
                
          default:
              return super.onOptionsItemSelected(item);
          
   
          }

 
}



}