package elskay.app;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import elskay.addtocart.ECart;
import elskay.feed.DiscoverNotification;
import elskay.login.DatabaseHandler;
import elskay.login.JSONParser;
import elskay.login.Register;

 
public class EditProfile extends BaseActivity {
 
    EditText userdesc;
    EditText userhashtag;
    Button btnSave;
    EditText username;
    EditText useremail;
	ImageView userprofileimage;
	ImageView usercoverimage;
	
	String encodedString;
	String imgPath, imagename,profileimage,filename;
	String cpimgPath, cpimagename,coverimage,cpfilename;
	Bitmap bitmap;
	ImageView imgView,cpimgView;;
	TextView txtView;
	
    
 
    String uid;
    boolean clicked=false;
    boolean clickedcp=false;
    private static int RESULT_LOAD_IMG = 1;
    private static int RESULT_LOAD_COVERPIC = 2;
    
    private Menu mToolbarMenu;
    int coot;
    // Progress Dialog
    private ProgressDialog pDialog;
 
    // JSON parser class
  
 
    // single product url
    private static final String url_product_detials = "http://128.199.176.163/elskayapp/elskayapp_login/get_userprofiledetail.php";
 
    // url to update product
    private static final String url_update_product = "http://128.199.176.163/elskayapp/elskayapp_login/update_userprofile.php";
 
    // url to delete product
 
    // JSON Node names
    //private static final String TAG_SUCCESS = "success";
    private static final String TAG_PRODUCT = "userprofile";
    private static final String TAG_UID = "unique_id";
    private static final String TAG_USERDESC = "userdescription";
    private static final String TAG_USERHASHTAG = "userhashname";
    private static final String TAG_USERNAME = "firstname";
    private static final String TAG_USEREMAIL = "email";
    private static final String TAG_USERIMAGE = "profileimage";
    private static final String TAG_USERCOVERIMAGE = "coverimage";

 
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.user_profile, frameLayout);
 
        // save button
        btnSave = (Button) findViewById(R.id.butnsave);
        
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        coot = settings.getInt("counts",0);
        
        getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM); 
        getActionBar().setCustomView(R.layout.actionbar_editprofile);
        TextView ac = (TextView)findViewById(R.id.acb);
        Typeface font = Typeface.createFromAsset(getAssets(),
        "AvenirNextLTPro-Regular.otf");
        ac.setTypeface(font); 
        getActionBar().setHomeButtonEnabled(true);
        
        getActionBar().setDisplayShowTitleEnabled(false);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        
        userhashtag = (EditText) findViewById(R.id.userhashname);
        username = (EditText) findViewById(R.id.profilefname);
        userprofileimage = (ImageView)findViewById(R.id.profileimage);
        usercoverimage = (ImageView)findViewById(R.id.coverimage);
        
        
        Typeface custom_font = Typeface.createFromAsset(getAssets(),
      	      "AvenirNextLTPro-Regular.otf");
      	      username.setTypeface(custom_font);
      	      userhashtag.setTypeface(custom_font);

        
 
        // getting product details from intent
        DatabaseHandler db = new DatabaseHandler(getApplicationContext());

        HashMap<String,String> user = new HashMap<String, String>();
        user = db.getUserDetails();
        uid= user.get("uid"); 
        
        
        userprofileimage.setOnClickListener(new View.OnClickListener()
        {
        public void onClick(View view) {
        	
        	 // Create intent to Open Image applications like Gallery, Google Photos
        	selectImage();
        	//profileimage.setVisibility(View.GONE);
        	clicked=true;
        	
       }
       });
        
        
        usercoverimage.setOnClickListener(new View.OnClickListener()
        {
        public void onClick(View view) {
        	
        	 // Create intent to Open Image applications like Gallery, Google Photos
        	selectCoverPic();
        	//profileimage.setVisibility(View.GONE);
        	clickedcp=true;
        	
       }
       });
 
      
    	if(isNetworkAvailable()){
            // do network operation here     
    		 new GetProductDetails().execute();
         }else{
            Toast.makeText(getApplicationContext(), "No Available Network. Please try again later", Toast.LENGTH_LONG).show();
           return;
         }
 
        // save button click event
        btnSave.setOnClickListener(new View.OnClickListener() {
 
            @Override
            public void onClick(View arg0) {
            	
                
                if (  ( !userhashtag.getText().toString().equals("")) )
                {
                	//new SaveProductDetails().execute();
                	
                	if(isNetworkAvailable()){
                        // do network operation here     
                		new SaveProductDetails().execute();
                     }else{
                        Toast.makeText(getApplicationContext(), "No Available Network. Please try again later", Toast.LENGTH_LONG).show();
                       return;
                     }
                }
                
                else if ( ( !userhashtag.getText().toString().equals("")) )
                {
                    Toast.makeText(getApplicationContext(),
                            "Hashtag Name field empty", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Toast.makeText(getApplicationContext(),
                            "Some field is empty", Toast.LENGTH_SHORT).show();
                }
            }
        });
 

       
    }
 
    
    
    
    
    private void selectImage() {
		 
        final CharSequence[] options = {"Choose from Gallery","Cancel"};
 
        AlertDialog.Builder builder = new AlertDialog.Builder(EditProfile.this);
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
            	
            	  if (options[item].equals("Choose from Gallery"))
                {
                    Intent intent = new   Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, 1);
                     
                }
                else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                    clicked=false;
                }
            }
        });
        builder.show();
    }
    
    private void selectCoverPic() {
		 
        final CharSequence[] options = {"Choose from Gallery","Cancel"};
 
        AlertDialog.Builder builder = new AlertDialog.Builder(EditProfile.this);
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
            	
            	  if (options[item].equals("Choose from Gallery"))
                {
                    Intent intent = new   Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, 2);
                     
                }
                else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                    clickedcp=false;
                }
            }
        });
        builder.show();
    }
    
    
    @Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    super.onActivityResult(requestCode, resultCode, data);
	    try {
	        // When an Image is picked
	        if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK  && data != null && data.getData() != null)
	                {
	            // Get the Image from data
	        	
	            Uri selectedImage = data.getData();
	            String[] filePathColumn = {MediaStore.Images.Media.DATA};

	            // Get the cursor
	            Cursor cursor = getContentResolver().query(selectedImage,
	                    filePathColumn, null, null, null);
	            // Move to first row
	            cursor.moveToFirst();

	            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
	            imgPath = cursor.getString(columnIndex);
	            cursor.close();
	           
	           
	           imgView = (ImageView) findViewById(R.id.profileimage);
		            // Set the Image in ImageView after decoding the String
		       imgView.setImageBitmap(BitmapFactory
		                    .decodeFile(imgPath));
		           
		            
		        Bitmap icon = BitmapFactory.decodeFile(imgPath);
		        imgView.setImageBitmap(icon);
	            
	           
	        } 
	        
	   else if (requestCode == RESULT_LOAD_COVERPIC && resultCode == RESULT_OK  && data != null && data.getData() != null)
            {
        // Get the Image from data
    	
        Uri selectedImage = data.getData();
        String[] filePathColumn = {MediaStore.Images.Media.DATA};

        // Get the cursor
        Cursor cursor = getContentResolver().query(selectedImage,
                filePathColumn, null, null, null);
        // Move to first row
        cursor.moveToFirst();

        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        cpimgPath = cursor.getString(columnIndex);
        cursor.close();
       
       
       cpimgView = (ImageView) findViewById(R.id.coverimage);
            // Set the Image in ImageView after decoding the String
       cpimgView.setImageBitmap(BitmapFactory
                    .decodeFile(cpimgPath));
           
            
        Bitmap icon = BitmapFactory.decodeFile(cpimgPath);
        cpimgView.setImageBitmap(icon);
        
       
         } 
	        else {
	            Toast.makeText(this, "You haven't picked Image",
	                    Toast.LENGTH_LONG).show();
	            clicked=false;
	            clickedcp=false;
	        }
	    } catch (Exception e) {
	        Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
	                .show();
	    }
    }
    
    
    
    /**
     * Background Async Task to Get complete product details
     * */
    
    class GetProductDetails extends AsyncTask<String, String, JSONObject> {
 
        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(EditProfile.this);
            pDialog.setMessage("Loading Profile Details. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }
 
        /**
         * Getting product details in background thread
         * */
        
        @Override
        protected JSONObject doInBackground(String... args) {
    		
        	 JSONParser jParser = new JSONParser();
       		 List<NameValuePair> params = new ArrayList<NameValuePair>();
     	       params.add(new BasicNameValuePair("uid",uid));
     		   JSONObject json = jParser.getJSONFromUrl(url_product_detials,params);
       		   
    		return json;
    		
   		    
    	}
       
        /**
         * After completing background task Dismiss the progress dialog
         * **/
        @Override
        protected void onPostExecute(final JSONObject json) {
   		
   	
   		 pDialog.dismiss();
   		
   		 try {
   			 
   			 
   			JSONArray productObj = json
                    .getJSONArray(TAG_PRODUCT); // JSON Array

            // get first product object from JSON Array
            JSONObject product = productObj.getJSONObject(0);

           
            if(product.getString(TAG_USERHASHTAG).equals("null"))
            {
            	userhashtag.setText("");
            }
            else
            // display product data in EditText
            {
            	userhashtag.setText(product.getString(TAG_USERHASHTAG));
            
            }
            username.setText(product.getString(TAG_USERNAME));
			//useremail.setText(product.getString(TAG_USEREMAIL));
			
			String userimage =product.getString(TAG_USERIMAGE).replace(" ", "%20");
			
			Picasso.with(getApplicationContext())
	          .load(userimage)
	          .resize(250, 250)
	          .transform(new RoundedTransformation(240, 120))
	          .centerCrop()
              //.transform(new RoundedTransformation(100, 20))
 	          .into(userprofileimage);
			
			String usercoverpic =product.getString(TAG_USERCOVERIMAGE);
			if(usercoverpic=="null")
				{
					usercoverimage.setBackgroundResource(R.drawable.rg2);
				}
				else{
            String ufinalcoverimage =usercoverpic.replace(" ", "%20");
				
				Picasso.with(getApplicationContext())
		          .load(ufinalcoverimage)
		           .resize(350, 350)
                .centerCrop()
		          .into(usercoverimage);
				}
           
   		}
		 
         
         catch (JSONException e) {
			
			e.printStackTrace();
		}
    }
    }
    /**
     * Background Async Task to  Save product Details
     * */
       class SaveProductDetails extends AsyncTask<String, String, JSONObject> {
 
        /**
         * Before starting background thread Show Progress Dialog
         * */
       @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(EditProfile.this);
            pDialog.setMessage("Saving 	Profile ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }
 
        /**
         * Saving product
         * */
        
        @Override
        protected JSONObject doInBackground(String... args) {
    		
        	 // getting updated data from EditTexts
            String userhashname = userhashtag.getText().toString();
            String userrname = username.getText().toString();
            
            
            if (clicked)
            {  

       	   String fileNameSegments[] = imgPath.split("/");
              filename = fileNameSegments[fileNameSegments.length - 1];
             
              BitmapFactory.Options options = null;
              options = new BitmapFactory.Options();
              options.inSampleSize=2;
               bitmap = BitmapFactory.decodeFile(imgPath,
                options);
               /*bitmap = Bitmap.createScaledBitmap(bitmap,
                       350, 350, false);*/
              ByteArrayOutputStream stream2 = new ByteArrayOutputStream();
              bitmap.compress(Bitmap.CompressFormat.JPEG, 90, stream2);
              byte[] byte_arr2 = stream2.toByteArray();
              profileimage = Base64.encodeToString(byte_arr2,0);
            }
          
            else
            {
            	
            }
            
            if (clickedcp)
            {  

       	      String fileNameSegments[] = cpimgPath.split("/");
              cpfilename = fileNameSegments[fileNameSegments.length - 1];
             
              BitmapFactory.Options options = null;
              options = new BitmapFactory.Options();
              options.inSampleSize=2;
               bitmap = BitmapFactory.decodeFile(cpimgPath,
                options);
               /*bitmap = Bitmap.createScaledBitmap(bitmap,
                       350, 350, false);*/
              ByteArrayOutputStream stream2 = new ByteArrayOutputStream();
              bitmap.compress(Bitmap.CompressFormat.JPEG, 90, stream2);
              byte[] byte_arr2 = stream2.toByteArray();
              coverimage = Base64.encodeToString(byte_arr2,0);
            }
          
            else
            {
            	
            }
 
            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair(TAG_UID, uid));
            params.add(new BasicNameValuePair(TAG_USERHASHTAG, userhashname));
            params.add(new BasicNameValuePair(TAG_USERNAME, userrname));
        
            
            if (filename != null && !filename.isEmpty())            {
            params.add(new BasicNameValuePair("profileimage", profileimage));
            params.add(new BasicNameValuePair("profilefilename", filename));
            }
          
            
            if (cpfilename != null && !cpfilename.isEmpty())            {
                params.add(new BasicNameValuePair("coverimage", coverimage));
                params.add(new BasicNameValuePair("coverfilename", cpfilename));
                }
            // sending modified data through http request
            // Notice that update product url accepts POST method
            JSONParser jParser = new JSONParser();
            JSONObject json2 = jParser.getJSONFromUrl(url_update_product, params);
       		   
    		return json2;
    		
   		    
    	} 
        
       
        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(final JSONObject json2) {
            // dismiss the dialog once product uupdated
        	Toast.makeText(getApplicationContext(), "Profile Updated",
                    Toast.LENGTH_LONG).show();
            pDialog.dismiss();
            
        }
    } 
       public boolean isNetworkAvailable() {
	       ConnectivityManager connectivityManager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
	       NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	       return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	   }
 
       
       @Override
       public boolean onCreateOptionsMenu(Menu menu) {
           getMenuInflater().inflate(R.menu.main, menu);
           return true;
       }
        public boolean onOptionsItemSelected(MenuItem item) {
           switch (item.getItemId()) {
           case R.id.action_settings: 
              
           	 Intent profile = new Intent(getApplicationContext(),ECart.class);
                startActivity(profile);
                finish();
               return true;
               
           case R.id.notification: 
               
             	 Intent notification = new Intent(getApplicationContext(),DiscoverNotification.class);
                  startActivity(notification);
                  finish();
                 return true;  
           
         
                 
           default:
               return super.onOptionsItemSelected(item);
           
    
           }
        } 
       public boolean onPrepareOptionsMenu(Menu paramMenu) {
    		mToolbarMenu = paramMenu;
    		createCartBadge(10);
    		return super.onPrepareOptionsMenu(paramMenu);
    	}
        private void createCartBadge(int paramInt) {
    		/*if (Build.VERSION.SDK_INT <= 15) {
    			return;
    		}*/
    		MenuItem cartItem = this.mToolbarMenu.findItem(R.id.action_settings);
    		LayerDrawable localLayerDrawable = (LayerDrawable) cartItem.getIcon();
    		Drawable cartBadgeDrawable = localLayerDrawable
    				.findDrawableByLayerId(R.id.ic_badge);
    		BadgeDrawable badgeDrawable;
    		if ((cartBadgeDrawable != null)
    				&& ((cartBadgeDrawable instanceof BadgeDrawable))
    				&& (paramInt < 10)) {
    			badgeDrawable = (BadgeDrawable) cartBadgeDrawable;
    		} else {
    			badgeDrawable = new BadgeDrawable(this);
    		}
    		int counttwo=3;
    		badgeDrawable.setCount(coot);
    		localLayerDrawable.mutate();
    		localLayerDrawable.setDrawableByLayerId(R.id.ic_badge, badgeDrawable);
    		cartItem.setIcon(localLayerDrawable);
    	}

       
    }
