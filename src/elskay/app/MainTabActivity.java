package elskay.app;




import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;
import elskay.addtocart.ECart;
import elskay.feed.DiscoverNotification;
import elskay.feed.FeedElskay;
import elskay.login.SessionManager;



 
public class MainTabActivity extends BaseActivity implements
        ActionBar.TabListener {
 
    private CustomViewPager viewPager;
    private TabsPageAdapter mAdapter;
    private Menu mToolbarMenu;

    private ActionBar actionBar;
    private SessionManager session;
    // Tab titles
    private String[] tabs = { "FAME", "BUY", "TALK" };
    //final int pos = 0;
    String con;
    String countset;
    int coot;
    
 
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.maintab);
        getLayoutInflater().inflate(R.layout.maintab, frameLayout);
        
       /* Thread.setDefaultUncaughtExceptionHandler(new MyExceptionHandler(this,
                Splash.class));*/
        
       // Bundle b=getIntent().getExtras();
       // con=b.getString("condition");
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        coot = settings.getInt("counts",0);
        
        
       
        session = new SessionManager(getApplicationContext());
        //getActionBar().setHomeButtonEnabled(true);
        //getActionBar().setDisplayHomeAsUpEnabled(true); 
        getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM); 
        getActionBar().setCustomView(R.layout.actionbar_postboard);
        
        getActionBar().setStackedBackgroundDrawable(new ColorDrawable(Color.parseColor("#ffffff")));
        TextView ac = (TextView)findViewById(R.id.acb);
        Typeface font = Typeface.createFromAsset(getAssets(),
        "AvenirNextLTPro-Regular.otf");
        ac.setTypeface(font); 
        
        
 
        // Initilization
        viewPager = (CustomViewPager) findViewById(R.id.pager);
        

        
        actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true); 
        Drawable res = getResources().getDrawable(R.drawable.singleline);
        getActionBar().setBackgroundDrawable(res);
        mAdapter = new TabsPageAdapter(getSupportFragmentManager());
        
        
        viewPager.setAdapter(mAdapter);

        
        //viewPager.setBackgroundResource(R.color.sab_yellow);

        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);    
        
        Typeface custom_font3 = Typeface.createFromAsset(getAssets(), "AvenirNextLTPro-Regular.otf");
		
 
        // Adding Tabs
        for (String tab_name : tabs) {
        	
        	
        	TextView t = new TextView(this);
            t.setAllCaps(true);
            t.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT, Gravity.CENTER));
            t.setGravity(Gravity.CENTER);
            t.setTextColor(Color.BLACK);
            t.setText(tab_name);
            t.setTextSize(14);
            t.setTypeface(custom_font3);
           
            actionBar.addTab(actionBar.newTab()
                    .setCustomView(t)
                   
                    .setTabListener(this));
            
            
            
            /*if(con.equals("TALK"))
   	     {
   	      viewPager.setCurrentItem(pos);
   	      //actionBar.setSelectedNavigationItem(position);
   	    
   	     }
        else if(con.equals("FEED"))
        {
       	viewPager.setCurrentItem(pos);
        //actionBar.setSelectedNavigationItem(position);
       	
         }
        else if(con.equals("BUY"))
       {
    	   viewPager.setCurrentItem(pos);
    	   //actionBar.setSelectedNavigationItem(position);
    	  
        }*/
        	
     
           
            
            
            
        } 
 
        
        /**
         * on swiping the viewpager make respective tab selected
         * */
        viewPager.setOnPageChangeListener(new CustomViewPager.OnPageChangeListener() {
 
            @Override
            public void onPageSelected(int position) {
                // on changing the page
                // make respected tab selected
            	
       


                 actionBar.setSelectedNavigationItem(position);
                //actionBar.setSelectedNavigationItem(position);
               
                
                
            }
 
            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }
 
            @Override
            public void onPageScrollStateChanged(int arg0) {
            }
        });
    }
 
    @Override
    public void onTabReselected(Tab tab, FragmentTransaction ft) {
    }
    
   
 
    @Override
    public void onTabSelected(Tab tab, FragmentTransaction ft) {
        // on tab selected
        // show respected fragment view
        viewPager.setCurrentItem(tab.getPosition());
      
        
   
    }
 
    @Override
    public void onTabUnselected(Tab tab, FragmentTransaction ft) {
    }
    
    
 

	
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
     public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case R.id.action_settings: 
           
        	 Intent profile = new Intent(getApplicationContext(),ECart.class);
             startActivity(profile);
             finish();
            return true;
            
        case R.id.notification: 
            
       	 Intent notification = new Intent(getApplicationContext(),DiscoverNotification.class);
            startActivity(notification);
            finish();
           return true;   
        
      
              
        default:
            return super.onOptionsItemSelected(item);
        
 
        }
     } 
     public boolean onPrepareOptionsMenu(Menu paramMenu) {
 		mToolbarMenu = paramMenu;
 		createCartBadge(10);
 		return super.onPrepareOptionsMenu(paramMenu);
 	}
     private void createCartBadge(int paramInt) {
 		/*if (Build.VERSION.SDK_INT <= 15) {
 			return;
 		}*/
 		MenuItem cartItem = this.mToolbarMenu.findItem(R.id.action_settings);
 		LayerDrawable localLayerDrawable = (LayerDrawable) cartItem.getIcon();
 		Drawable cartBadgeDrawable = localLayerDrawable
 				.findDrawableByLayerId(R.id.ic_badge);
 		BadgeDrawable badgeDrawable;
 		if ((cartBadgeDrawable != null)
 				&& ((cartBadgeDrawable instanceof BadgeDrawable))
 				&& (paramInt < 10)) {
 			badgeDrawable = (BadgeDrawable) cartBadgeDrawable;
 		} else {
 			badgeDrawable = new BadgeDrawable(this);
 		}
 		int counttwo=3;
 		badgeDrawable.setCount(coot);
 		localLayerDrawable.mutate();
 		localLayerDrawable.setDrawableByLayerId(R.id.ic_badge, badgeDrawable);
 		cartItem.setIcon(localLayerDrawable);
 	}
     
    @Override
     public void onBackPressed() {
    	 finish();
     }
     }
   
 
