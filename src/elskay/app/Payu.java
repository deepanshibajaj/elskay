package elskay.app;



import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EncodingUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import elskay.checkout.OrderErrorPage;
import elskay.checkout.PaymentMethod;
import elskay.checkout.SuccessPage;
import elskay.checkout.UserAddress;

import elskay.login.DatabaseHandler;
import elskay.login.JSONParser;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.SyncStateContract.Constants;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

public class Payu extends Activity {

	
	private static final String TAG = "MainActivity";
	WebView webviewPayment;
    Handler mHandler = new Handler();
    final Activity activity =this;
    
     String TotalAmount;
	 ArrayList<String> ProductNameArray;
	 String uname;
	 String uaddress;
	 String ucity;
	 String upincode;
	 String uphonenumber;
	 String uid;
	 JSONObject ob;
	 JSONArray mJSONArray;
	 String countproducts;
	 String useremail;
	 String txnid;
	 
	 private ProgressDialog pDialog;
	  private static String KEY_SUCCESS = "success";
	  
	  private static final String url_buy_product = "http://128.199.176.163/elskayapp/elskayshop/shop_payu_orderplaced.php";
	 
	  StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
	 
    
    @SuppressLint("JavascriptInterface")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.payu_payment);
		
		
		Intent intent = getIntent();
		ProductNameArray = intent.getStringArrayListExtra("productarray");
	    for (String value : ProductNameArray) {	
				 //System.out.println(value);
				 Log.i("value",value);
					}
	     
	     txnid = intent.getStringExtra("transactionid");
	     TotalAmount = intent.getStringExtra("totalamount");
	     countproducts = intent.getStringExtra("countproducts");
	     uname=intent.getStringExtra("uname");
	     uaddress=intent.getStringExtra("uaddress");
	     ucity=intent.getStringExtra("ucity");
	     upincode=intent.getStringExtra("upincode");
	     uphonenumber=intent.getStringExtra("uphonenumber");
	     DatabaseHandler db = new DatabaseHandler(getApplicationContext());
	        HashMap<String,String> user = new HashMap<String, String>();
	        user = db.getUserDetails();
	        uid= user.get("uid");
	        useremail=user.get("email");
		
		webviewPayment = (WebView) findViewById(R.id.webviewPayment);
		webviewPayment.getSettings().setJavaScriptEnabled(true);
		webviewPayment.getSettings().setDomStorageEnabled(true);
		webviewPayment.getSettings().setLoadWithOverviewMode(true);
		webviewPayment.getSettings().setUseWideViewPort(true);
		
		    Button payutext = (Button)findViewById(R.id.btnpayuscreen);    
		    Typeface custom_font12 = Typeface.createFromAsset(getAssets(), "AvenirNextLTPro-Regular.otf");
		    payutext.setTypeface(custom_font12); 
		    
        ImageButton back = (ImageButton)findViewById(R.id.backbutn);
        
        back.setOnClickListener(new OnClickListener()
		{

		@Override
		public void onClick(View v) {

			Intent myIntent = new Intent(getApplicationContext(), PaymentMethod.class);
			 myIntent.putStringArrayListExtra("productarray", ProductNameArray);
			 myIntent.putExtra("totalamount", TotalAmount);
			 myIntent.putExtra("countproducts", countproducts);
			 myIntent.putExtra("uname",uname);
       	    myIntent.putExtra("uaddress",uaddress);
       	    myIntent.putExtra("ucity",ucity);
       	    myIntent.putExtra("upincode",upincode);
       	     myIntent.putExtra("uphonenumber",uphonenumber);
            startActivityForResult(myIntent, 0);
            finish();

		}


		});
		
		 
		//webviewPayment.loadUrl("http://www.google.com");
		/*webviewPayment
				.loadUrl("128.199.193.113/rakhi/payment/endpoint?order_id=aAbBcC45&amount=10");*/

		
	//	webviewPayment.loadUrl("http://timesofindia.com/");
		StringBuilder url_s = new StringBuilder();
		//http://merirakhi.com/processor/payment/endpoint?order_id=aAbBcC&amount=10&currency=USD
		url_s.append("https://secure.payu.in/_payment");
		
		Log.e(TAG, "call url " + url_s);
		
		//webviewPayment.loadUrl(url_s.toString());
		//String postData = "username=my_username&password=my_password";
		webviewPayment.postUrl(url_s.toString(),EncodingUtils.getBytes(getPostString(), "utf-8"));
		
	//	webviewPayment.loadUrl("http://128.199.193.113/rakhi/payment/endpoint?order_id=aAbBcC45&amount=0.10&currency=USD");
		
		webviewPayment.setWebViewClient(new WebViewClient() {
			@Override
			public void onPageFinished(WebView view, String url) {
				super.onPageFinished(view, url);
			}

			@SuppressWarnings("unused")
			public void onReceivedSslError(WebView view) {
				Log.e("Error", "Exception caught!");
			}

			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				view.loadUrl(url);
				return true;
			}

		});
		
		webviewPayment.addJavascriptInterface(new PayUJavaScriptInterface(this), "PayUMoney");  
		
	}
    
    
    public class PayUJavaScriptInterface {
        Context mContext;

        /** Instantiate the interface and set the context */
        PayUJavaScriptInterface(Context c) {
            mContext = c;
        }
        @JavascriptInterface
        public void success(long id, final String paymentId) {

            mHandler.post(new Runnable() {

            	public void run() {

                    mHandler = null;

                   /* Intent intent = new Intent(Payu.this, SuccessPage.class);

                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                    intent.putExtra("result", "success");

                    intent.putExtra("paymentId", paymentId);
                    Log.i("payid",paymentId);

                    startActivity(intent);

                    finish();*/
                    
                    new SaveProductDetails().execute();

                }
            });
        }
        
        @JavascriptInterface
        public void failure(long id, final String paymentId) {

            mHandler.post(new Runnable() {

            	public void run() {

                    mHandler = null;

                    Intent intent = new Intent(Payu.this, OrderErrorPage.class);

                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                    intent.putExtra("result", "failure");

                    intent.putExtra("paymentId", paymentId);

                    startActivity(intent);

                    finish();

                }
            });
        }
    }
 

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private String getPostString()
	{
		String key  = "RicqpL";
		String salt  = "OTJc0zqm";
		String transactionid = txnid;
		String amount = TotalAmount;
		String firstname = uname;
		String email = useremail;
		String productInfo = "Product3"; 
		
		StringBuilder post = new StringBuilder();
		post.append("key=");
		post.append(key);
		post.append("&");
		post.append("txnid=");
		post.append(transactionid);
		post.append("&");
		post.append("amount=");
		post.append(amount);
		post.append("&");
		post.append("productinfo=");
		post.append(productInfo);
		post.append("&");
		post.append("firstname=");
		post.append(firstname);
		post.append("&");
		post.append("email=");
		post.append(email);
		post.append("&");
		post.append("phone=");
		post.append(uphonenumber);
		post.append("&");
		post.append("surl=");
		post.append("http://128.199.176.163/elskayapp/elskayshop/payu_success.php");
		post.append("&");
		post.append("furl=");
		post.append("http://128.199.176.163/elskayapp/elskayshop/payu_failurepage.php");
		post.append("&");
	
		StringBuilder checkSumStr = new StringBuilder();
		/* =sha512(key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5||||||salt) */
	    MessageDigest digest=null;
	    String hash;
	    try {
	        digest = MessageDigest.getInstance("SHA-512");// MessageDigest.getInstance("SHA-256");
	        
	        checkSumStr.append(key);
	        checkSumStr.append("|");
	        checkSumStr.append(transactionid);
	        checkSumStr.append("|");
	        checkSumStr.append(amount);
	        checkSumStr.append("|");
	        checkSumStr.append(productInfo);
	        checkSumStr.append("|");
	        checkSumStr.append(firstname);
	        checkSumStr.append("|");
	        checkSumStr.append(email);
	        checkSumStr.append("|||||||||||");
	        checkSumStr.append(salt);
	        
	        digest.update(checkSumStr.toString().getBytes());

	        hash = bytesToHexString(digest.digest());
	    	post.append("hash=");
	        post.append(hash);
	        post.append("&");
	        Log.i(TAG, "SHA result is " + hash);
	    } catch (NoSuchAlgorithmException e1) {
	        // TODO Auto-generated catch block
	        e1.printStackTrace();
	    }
	    
		post.append("service_provider=");
		post.append("payu_paisa");
		return post.toString();	
	}
	
	
	class SaveProductDetails extends AsyncTask<String, String, JSONObject> {
		 
        /**
         * Before starting background thread Show Progress Dialog
         * */
       @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(Payu.this);
            pDialog.setMessage("Please wait ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }
 
        /**
         * Saving product
         * */
        
        @Override
        protected JSONObject doInBackground(String... args) {
    		
            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("uid", uid));
            params.add(new BasicNameValuePair("uname", uname));
            params.add(new BasicNameValuePair("uemail", useremail));
            params.add(new BasicNameValuePair("uaddress", uaddress));
            params.add(new BasicNameValuePair("ucity", ucity));
            params.add(new BasicNameValuePair("upincode", upincode));
            params.add(new BasicNameValuePair("uphonenumber", uphonenumber));
            params.add(new BasicNameValuePair("TotalAmount", TotalAmount));
            params.add(new BasicNameValuePair("transactionid", txnid));
            params.add(new BasicNameValuePair("countproducts", countproducts));
            
            //mJSONArray = new JSONArray(ProductNameArray);
            /*for(String products : mJSONArray) {

  	        	params.add(new BasicNameValuePair("products[]",mJSONArray.toString()));
  	        	System.out.print(products);

  	         }*/
            params.add(new BasicNameValuePair("product",ProductNameArray.toString()));
            //params.add(new BasicNameValuePair("product",ob.toString()));
            Log.i("params",params.toString());
            // sending modified data through http request
            // Notice that update product url accepts POST method
            JSONParser jParser = new JSONParser();
            
            JSONObject json2 = jParser.getJSONFromUrl(url_buy_product, params);
            //System.out.print(params.toString());
       		   
    		return json2;
    		
   		    
    	} 
        
       
        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(final JSONObject json2) {
            // dismiss the dialog once product uupdated
        	 try {
                 if (json2.getString(KEY_SUCCESS) != null) {

                      String res = json2.getString(KEY_SUCCESS);
                      Log.i("log_error",res);

                      if(Integer.parseInt(res) == 1){
                    	  Intent upanel = new Intent(getApplicationContext(), SuccessPage.class);
                          pDialog.dismiss();
                          startActivity(upanel);
                      }
                      else{

                    	  Intent upanel = new Intent(getApplicationContext(), OrderErrorPage.class);
                          pDialog.dismiss();
                          startActivity(upanel);
                          
                      }
                 
                 }
        	 }catch (JSONException e) {
                 e.printStackTrace();
             }
        }
	}
	
	
	private JSONObject getProductInfo()
	{
		try {
			//create payment part object
			JSONObject productInfo = new JSONObject();
			
			JSONObject jsonPaymentPart = new JSONObject();
			jsonPaymentPart.put("name", "TapFood");
			jsonPaymentPart.put("description", "Lunchcombo");
			jsonPaymentPart.put("value", "500");
			jsonPaymentPart.put("isRequired", "true");
			jsonPaymentPart.put("settlementEvent", "EmailConfirmation");
			
			//create payment part array
			JSONArray jsonPaymentPartsArr = new JSONArray();
			jsonPaymentPartsArr.put(jsonPaymentPart);
			
			//paymentIdentifiers
			JSONObject jsonPaymentIdent = new JSONObject();
			jsonPaymentIdent.put("field", "CompletionDate");
			jsonPaymentIdent.put("value", "31/10/2012");
			
			//create payment part array
			JSONArray jsonPaymentIdentArr = new JSONArray();
			jsonPaymentIdentArr.put(jsonPaymentIdent);
			
			productInfo.put("paymentParts", jsonPaymentPartsArr);
			productInfo.put("paymentIdentifiers", jsonPaymentIdentArr);
			
			Log.e(TAG, "product Info = " + productInfo.toString());
			return productInfo;
			
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	   
	private static String bytesToHexString(byte[] bytes) {
        // http://stackoverflow.com/questions/332079
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < bytes.length; i++) {
            String hex = Integer.toHexString(0xFF & bytes[i]);
            if (hex.length() == 1) {
                sb.append('0');
            }
            sb.append(hex);
        }
        return sb.toString();
    }
	
	
}

