

package elskay.app;




import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import elskay.fblogin.FbMainActivity;
import elskay.fblogin.FragmentSimpleLoginButton;
import elskay.login.Login;


public class Splash extends Activity{

	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);
		 
		
		final Boolean isFirstRun = getSharedPreferences("PREFERENCE", MODE_PRIVATE)
				.getBoolean("isfirstrun", true);
		

		
         Handler handler =new Handler();
		
		handler.postDelayed(new Runnable() 
		{
			
	
		 
		public void run()
		{
			
			Intent openMainActivity =  new Intent(Splash.this, FragmentSimpleLoginButton.class);
            startActivity(openMainActivity);
            finish();
           
			}
		},2000);
		
		}
		
		
		
	}

