package elskay.app;

import elskay.category.CategoryTabs;
import elskay.category.ECategories;
import elskay.feed.FeedElskay;
import elskay.feed.FeedTabs;
import elskay.styleadvice.AdviceMain;
import elskay.styleadvice.AdviceTabs;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
 
public class TabsPageAdapter extends FragmentPagerAdapter {
 
	
	//private String tabtitles[] = new String[] { "Tab1", "Tab2", "Tab3" };
    public TabsPageAdapter(FragmentManager fm) {
        super(fm);
        
    }
    
 
    @Override
    public Fragment getItem(int position) {
 
        switch (position) {
        case 0:
            // Top Rated fragment activity
        	 return new FeedTabs();
            
        case 1:
            // Games fragment activity
        	return new CategoryTabs();
        case 2:
            // Movies fragment activity
            return new AdviceTabs();
        }
 
        return null;
    }
 
    @Override
    public int getCount() {
        // get item count - equal to number of tabs
        return 3;
    }
   /* @Override
    public CharSequence getPageTitle(int position) {
		return tabtitles[position];
	} */
 
}