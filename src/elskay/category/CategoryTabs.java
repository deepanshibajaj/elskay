package elskay.category;




import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.ParseException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.PagerTitleStrip;
import android.support.v4.view.ViewPager;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ImageSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;





import com.viewpagerindicator.TitlePageIndicator;

import elskay.app.R;
import elskay.category.ECategories;
import elskay.category.ECategories.JSONAsyncTask;
import elskay.product.EDemoProduct;
import elskay.styleadvice.AdviceMain;

public class CategoryTabs extends Fragment  {

	private FragmentActivity myContext;
	 TitlePageIndicator titleIndicator;
	 JSONArray jarray;
		ECategoryAdapter adapter;
		JSONObject jsono;
		String pid;
		String catnamee;
		 ViewPager pager;
		
		 SectionsPagerAdapter mSectionsPagerAdapter;
		 ArrayList<String> Categoryidarray = new ArrayList<String>();
		 ArrayList<String> Categorynamearray = new ArrayList<String>();
		 ArrayList<String> Categoryimagearray = new ArrayList<String>();
		
		
		
		
	   	private static final String TAG_OS = "cat";
	    private static final String TAG_CATNAME = "cat_name";
	    private static final String TAG_CATDESC = "cat_details";
	    private static final String TAG_CATIMAGE = "cat_image";
	    private static final String TAG_CATID = "cat_id";
		
	
		
	//private static final CharSequence[] TITLES = new CharSequence[] { "Title 1", "Title 2" };
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
	
	@Override
	public void onAttach(Activity activity) {
	    myContext=(FragmentActivity) activity;
	    super.onAttach(activity);
	}


	 @Override
	    public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                             Bundle savedInstanceState) {
	        View v = inflater.inflate(R.layout.shop_viewpager_tabs, container, false);
	         
       
        pager = (ViewPager) v.findViewById(R.id.shopviewpager);
        PagerTabStrip Title = (PagerTabStrip)v.findViewById(R.id.shopviewpagertabstrip);
        
        
       Title.setTabIndicatorColor(Color.parseColor("#ffffff"));
        Typeface custom_font = Typeface.createFromAsset(getActivity().getAssets(), "AvenirNextLTPro-Regular.otf");
        for (int i = 0; i < Title.getChildCount(); ++i) {
            View nextChild = Title.getChildAt(i);
            if (nextChild instanceof TextView) {
               TextView textViewToConvert = (TextView) nextChild;
               textViewToConvert.setTypeface(custom_font);
               
               
            }
        }
       
        if(isNetworkAvailable()){
            // do network operation here     
        	new JSONAsyncTask().execute("http://128.199.176.163/elskayapp/getcategory.php");
         }else{
            Toast.makeText(getContext(), "No Available Network. Please try again later", Toast.LENGTH_LONG).show();
          
         } 
       // new JSONAsyncTask().execute("http://128.199.176.163/elskayapp/getcategory.php");
        
        mSectionsPagerAdapter = new SectionsPagerAdapter(getActivity(),
        		myContext.getSupportFragmentManager());
        pager.setAdapter(mSectionsPagerAdapter);
       
        
        
        
       
        return v;
    }
	
	   
	   
	 class JSONAsyncTask extends AsyncTask<String, Void, Boolean> {
			
			ProgressDialog dialog;
				
				@Override
				protected void onPreExecute() {
					super.onPreExecute();
					
					dialog = new ProgressDialog(getActivity());
					dialog.setMessage("Please give me a moment to open the box of treasure");
					//dialog.setTitle("Connecting server");
					dialog.show();
					dialog.setCancelable(false);
				}
				
				@Override
				protected Boolean doInBackground(String... urls) {
					try {
						
						//------------------>>
						HttpGet httppost = new HttpGet(urls[0]);
						HttpClient httpclient = new DefaultHttpClient();
						HttpResponse response = httpclient.execute(httppost);

						// StatusLine stat = response.getStatusLine();
						int status = response.getStatusLine().getStatusCode();

						if (status == 200) {
							HttpEntity entity = response.getEntity();
							String data = EntityUtils.toString(entity);
							
						
							jsono = new JSONObject(data);
							jarray = jsono.getJSONArray(TAG_OS);
							
							for (int i = 0; i < jarray.length(); i++) {
								JSONObject object = jarray.getJSONObject(i);
							
								
								
								final String catname=object.getString(TAG_CATNAME);
								final String catdetails=object.getString(TAG_CATDESC);
								final String catimage=object.getString(TAG_CATIMAGE);
								final String catid=object.getString(TAG_CATID);
								
								HashMap<String, String> map = new HashMap<String, String>();

					  	        map.put(TAG_CATID, catid);
								map.put(TAG_CATNAME, catname);
								map.put(TAG_CATDESC, catdetails);
								map.put(TAG_CATIMAGE, catimage);
								
								
								getActivity().runOnUiThread(new Runnable() {
					  		        @Override
					  		        public void run() {
					  		       
					  		        	Categoryidarray.add(catid);
					  		        	Categorynamearray.add(catname);
					  		        	Categoryimagearray.add(catimage);
					  		        	mSectionsPagerAdapter.notifyDataSetChanged();
					  		        	
					  		        
					  		        }
					  		    });
								
							}
							return true;
							
							
						}
						
						//------------------>>
						
					} catch (ParseException e1) {
						e1.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					} catch (JSONException e) {
						e.printStackTrace();
					}
					return false;
				}
				
				

				protected void onPostExecute(Boolean result) {
					dialog.cancel();
					
					if(result == false)
						Toast.makeText(getActivity(), "Oopss!! Our engineer fell asleep,Please try again Later", Toast.LENGTH_LONG).show();

				}
			}
	   private class SectionsPagerAdapter extends FragmentPagerAdapter {
		   Context c;
		    
		    
		    
		 public SectionsPagerAdapter(Context c, FragmentManager jm) {
		    super(jm);
		    this.c = c;
		    
		    
		 }
		 
		 @Override
		 public Fragment getItem(int i) {
			 
			 
		  Fragment fragment = null;
		  
		  
		  String catidd = Categoryidarray.get(i);
		  catnamee  = Categorynamearray.get(i);
		  String catimagee = Categoryimagearray.get(i);
		 
		  
		  Bundle bundle = new Bundle();
          bundle.putString("pathid", catidd);
          bundle.putString("catimage", catimagee);
          EDemoProduct fragInfo = new EDemoProduct();
          fragInfo.setArguments(bundle);
          return fragInfo;
		

		
		  
		 }
		 @Override
		 public int getCount() {
			 
		   return Categoryidarray.size();
		 }

		 @Override
		public CharSequence getPageTitle(int position) {
		 
			 return Categorynamearray.get(position);
		   //String s = catnamee;//setting the title
		  // return s;
			/* SpannableStringBuilder sb = new SpannableStringBuilder(" Page " + (position + 1)); // space added before text for convenience
			 
			    Drawable drawable = mContext.getResources().getDrawable( R.drawable.ic_launcher );
			    drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
			    ImageSpan span = new ImageSpan(drawable, ImageSpan.ALIGN_BASELINE);
			    sb.setSpan(span, 0, 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
			 
			    return sb;*/
		}
		 	 
		}
		  
	 
	 
	 
	 
	 
	 
	 

  /*  private class MyPagerAdapter extends FragmentPagerAdapter {

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

       /* @Override
        public Fragment getItem(int pos) {
            switch(pos) {

            case 0: return new AdviceMain();
            case 1: return new FeedElskay();
            
            default: return new AdviceMain();
            }
        }

        @Override
        public int getCount() {
            return 2;
        }       
    }*/
    
    /*  @Override
    public Fragment getItem(int index) {
 
        switch (index) {
        case 0:
            // Top Rated fragment activity
            return new AdviceMain();
        case 1:
            // Games fragment activity
            return new AdviceMain();
        case 2:
            // Games fragment activity
            //return new ECategories();
            Bundle bundle = new Bundle();
            bundle.putString("pathid", "1");
            EDemoProduct fragInfo = new EDemoProduct();
            fragInfo.setArguments(bundle);
            return fragInfo;
        case 3:
            // Games fragment activity
            //return new ECategories();
            Bundle bundle2 = new Bundle();
            bundle2.putString("pathid", "2");
            EDemoProduct fragInfoo = new EDemoProduct();
            fragInfoo.setArguments(bundle2);
            return fragInfoo;
            
       
        }
        
 
        return null;
    }
      
      @Override
      public int getCount() {
          // get item count - equal to number of tabs
          return 4;
      }
      @Override
      public CharSequence getPageTitle(int position) {
    	  switch (position) {
          case 0:
              return "Advice";
          case 1:
              return "Asked";
          case 2:
              return "Neckpieces";
          case 3:
              return "Earrings";
         
      }
    	   return null;
      }
      
    }*/
	   
	   public boolean isNetworkAvailable() {
	       ConnectivityManager connectivityManager = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
	       NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	       return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	   }
	   
	   
}
