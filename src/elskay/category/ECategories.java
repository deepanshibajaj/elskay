package elskay.category;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.ParseException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;
import elskay.app.R;
import elskay.product.EProduct;

public class ECategories extends Fragment {
	
	ArrayList<ECategoryList> actorsList;
	JSONArray jarray;
	ECategoryAdapter adapter;
	JSONObject jsono;
	String pid;
	
	 @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	    }
	
	/*@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.category_list);*/
	 
	 
	 public View onCreateView(LayoutInflater inflater, ViewGroup container,
             Bundle savedInstanceState) {
        View vie = inflater.inflate(R.layout.category_list, container, false);
        
		actorsList = new ArrayList<ECategoryList>();
		new JSONAsyncTask().execute("http://128.199.176.163/elskayapp/getcategory.php");
		
		ListView listview = (ListView)vie.findViewById(R.id.list);
		
		adapter = new ECategoryAdapter(getActivity(), R.layout.category_list_item, actorsList);
		
		listview.setAdapter(adapter);
		
		
        // on seleting single product
        // launching pdfviewer activity
		listview.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                    int position, long id) {
                // getting values from selected ListItem
               

                // Starting new intent
                Intent in = new Intent(getActivity(),
                        EProduct.class);
                // sending pid to next activity
                String name = actorsList.get(position).getcatid();
                in.putExtra("pathid", name);
                Log.i("naemmmmmeee@@@@@@@",name);
                // starting new activity and expecting some response back
                startActivity(in);
            }
        });
		
		  return vie;
	}


	class JSONAsyncTask extends AsyncTask<String, Void, Boolean> {
		
	ProgressDialog dialog;
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			dialog = new ProgressDialog(getActivity());
			dialog.setMessage("Please give me a moment to open the box of treasure");
			//dialog.setTitle("Connecting server");
			dialog.show();
			dialog.setCancelable(false);
		}
		
		@Override
		protected Boolean doInBackground(String... urls) {
			try {
				
				//------------------>>
				HttpGet httppost = new HttpGet(urls[0]);
				HttpClient httpclient = new DefaultHttpClient();
				HttpResponse response = httpclient.execute(httppost);

				// StatusLine stat = response.getStatusLine();
				int status = response.getStatusLine().getStatusCode();

				if (status == 200) {
					HttpEntity entity = response.getEntity();
					String data = EntityUtils.toString(entity);
					
				
					jsono = new JSONObject(data);
					jarray = jsono.getJSONArray("cat");
					
					for (int i = 0; i < jarray.length(); i++) {
						JSONObject object = jarray.getJSONObject(i);
					
						ECategoryList actor = new ECategoryList();
						
						actor.setName(object.getString("cat_name"));
						actor.setDescription(object.getString("cat_details"));
						actor.setImage(object.getString("cat_image"));
						actor.setcatid(object.getString("cat_id"));
						
						
						actorsList.add(actor);
					}
					return true;
				}
				
				//------------------>>
				
			} catch (ParseException e1) {
				e1.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return false;
		}
		
		protected void onPostExecute(Boolean result) {
			dialog.cancel();
			adapter.notifyDataSetChanged();
			if(result == false)
				Toast.makeText(getActivity(), "Oopss!! Our engineer fell asleep,Please try again Later", Toast.LENGTH_LONG).show();

		}
	}
	
	

	
	
	
}
