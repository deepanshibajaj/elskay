package elskay.category;

public class ECategoryList {

	private String name;
	private String description;
	private String image;
	private String catid;

	public ECategoryList() {
		// TODO Auto-generated constructor stub
	}

	public ECategoryList(String name, String description ,String image,String catid) {
		super();
		this.name = name;
		this.description = description;
		this.image = image;
		this.catid = catid;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}
	
	public String getcatid() {
		return catid;
	}

	public void setcatid(String catid) {
		this.catid = catid;
	}

}
