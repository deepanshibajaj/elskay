package elskay.checkout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import elskay.app.R;

import elskay.app.R.id;
import elskay.app.R.layout;
import elskay.checkout.UserAddress.GetUserAddress;
import elskay.login.DatabaseHandler;
import elskay.login.JSONParser;
import elskay.login.Registered;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class AddAddress extends Activity {
	/** Called when the activity is first created. */
 
	    EditText username;
	    EditText useraddress;
	    EditText usercity;
	    Button btnSave;
	    EditText userpincode;
	    EditText useremail;
	    EditText userphonenumber;
	    String uid;
	    ArrayList<String> ProductNameArray;
	    String TotalAmount;
	    String countproducts;
	    
	    
	    // Progress Dialog
	    private ProgressDialog pDialog;
	 
	    // JSON parser class
	  
	 
	    // url to update product
	    private static final String url_save_address = "http://128.199.176.163/elskayapp/elskayapp_login/add_useraddress.php";
	 
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.user_address_add);
		
		   Intent intent = getIntent();
	      
	       ProductNameArray = intent.getStringArrayListExtra("productarray");
	       TotalAmount = intent.getStringExtra("totalamount");
	       countproducts = intent.getStringExtra("countproducts");
		
	       
	       Button edit = (Button)findViewById(R.id.Button01);    
		    Typeface custom_font12 = Typeface.createFromAsset(getAssets(), "AvenirNextLTPro-Regular.otf");
		    edit.setTypeface(custom_font12);   
		ImageButton back = (ImageButton)findViewById(R.id.backbutn);
		username = (EditText) findViewById(R.id.name);
		useraddress = (EditText) findViewById(R.id.address);
		userpincode = (EditText) findViewById(R.id.pincode);
        useremail = (EditText) findViewById(R.id.email);
        usercity = (EditText) findViewById(R.id.city);
        userphonenumber = (EditText) findViewById(R.id.phonenumber);
        btnSave = (Button)findViewById(R.id.buttonsave);
        
        Typeface custom_font1 = Typeface.createFromAsset(getAssets(), "AvenirNextLTPro-Regular.otf");
        username.setTypeface(custom_font1);
        Typeface custom_font2 = Typeface.createFromAsset(getAssets(), "AvenirNextLTPro-Regular.otf");
        useraddress.setTypeface(custom_font2);
        Typeface custom_font3 = Typeface.createFromAsset(getAssets(), "AvenirNextLTPro-Regular.otf");
        userpincode.setTypeface(custom_font3);
        Typeface custom_font4 = Typeface.createFromAsset(getAssets(), "AvenirNextLTPro-Regular.otf");
        useremail.setTypeface(custom_font4);
        Typeface custom_font5 = Typeface.createFromAsset(getAssets(), "AvenirNextLTPro-Regular.otf");
        usercity.setTypeface(custom_font5);
        Typeface custom_font6 = Typeface.createFromAsset(getAssets(), "AvenirNextLTPro-Regular.otf");
        userphonenumber.setTypeface(custom_font6);
        
        
        DatabaseHandler db = new DatabaseHandler(getApplicationContext());

        HashMap<String,String> user = new HashMap<String, String>();
        user = db.getUserDetails();
        uid= user.get("uid");
	        
	        back.setOnClickListener(new OnClickListener()
			{

			@Override
			public void onClick(View v) {

				 Intent myIntent = new Intent(getApplicationContext(), UserAddress.class);
				 
				 Bundle b=new Bundle();
	             b.putString("condition","BACK");
	             myIntent.putExtras(b);
	             myIntent.putStringArrayListExtra("productarray", ProductNameArray);
				 myIntent.putExtra("totalamount", TotalAmount);
				 myIntent.putExtra("countproducts", countproducts);
	             startActivityForResult(myIntent, 0);
	             finish();
				 

			}


			}); 
	        
	       
	        
	        // save button click event
	        Typeface custom_font = Typeface.createFromAsset(getAssets(), "AvenirNextLTPro-Regular.otf");
	        btnSave.setTypeface(custom_font);
	        btnSave.setOnClickListener(new View.OnClickListener() {
	 
	            @Override
	            public void onClick(View arg0) {
	            	
	                
	                if (  ( !username.getText().toString().equals("")) && ( !useraddress.getText().toString().equals("")) 
	                		&& ( !userpincode.getText().toString().equals("")) && ( !useremail.getText().toString().equals(""))
	                		&& ( !userphonenumber.getText().toString().equals(""))
	                		&& ( !usercity.getText().toString().equals("")))
	                {
	                	//new SaveAddress().execute();
	                	
	                	if(isNetworkAvailable()){
	                        // do network operation here     
	                		new SaveAddress().execute();
	                     }else{
	                        Toast.makeText(getApplicationContext(), "No Available Network. Please try again later", Toast.LENGTH_LONG).show();
	                        return;
	                      
	                     }
	                    
	                }
	                else if ( ( !username.getText().toString().equals("")) )
	                {
	                    Toast.makeText(getApplicationContext(),
	                            "Name field empty", Toast.LENGTH_SHORT).show();
	                }
	                else if ( ( !userpincode.getText().toString().equals("")) )
	                {
	                    Toast.makeText(getApplicationContext(),
	                            "Pincode field empty", Toast.LENGTH_SHORT).show();
	                }
	                else if ( ( !useremail.getText().toString().equals("")) )
	                {
	                    Toast.makeText(getApplicationContext(),
	                            "Email field empty", Toast.LENGTH_SHORT).show();
	                }
	                else if ( ( !userphonenumber.getText().toString().equals("")) )
	                {
	                    Toast.makeText(getApplicationContext(),
	                            "Phone Number field empty", Toast.LENGTH_SHORT).show();
	                }
	                else if ( ( !useraddress.getText().toString().equals("")) )
	                {
	                    Toast.makeText(getApplicationContext(),
	                            "Address field empty", Toast.LENGTH_SHORT).show();
	                }
	                else if ( ( !usercity.getText().toString().equals("")) )
	                {
	                    Toast.makeText(getApplicationContext(),
	                            "City field empty", Toast.LENGTH_SHORT).show();
	                }
	                else
	                {
	                    Toast.makeText(getApplicationContext(),
	                            "Some field is empty", Toast.LENGTH_SHORT).show();
	                }
	            }
	        });
	 
		     
        
		
		

	}
 
	 class SaveAddress extends AsyncTask<String, String, JSONObject> {
		 
	        /**
	         * Before starting background thread Show Progress Dialog
	         * */
	       @Override
	        protected void onPreExecute() {
	            super.onPreExecute();
	            pDialog = new ProgressDialog(AddAddress.this);
	            pDialog.setMessage("Saving Address ...");
	            pDialog.setIndeterminate(false);
	            pDialog.setCancelable(true);
	            pDialog.show();
	        }
	 
	        /**
	         * Saving product
	         * */
	        
	        @Override
	        protected JSONObject doInBackground(String... args) {
	    		
	        	 // getting updated data from EditTexts
	            String name = username.getText().toString();
	            String address = useraddress.getText().toString();
	            String pincode = userpincode.getText().toString();
	            String email = useremail.getText().toString();
	            String phonenumber = userphonenumber.getText().toString();
	            String city = usercity.getText().toString();
	            
	 
	            // Building Parameters
	            List<NameValuePair> params = new ArrayList<NameValuePair>();
	            params.add(new BasicNameValuePair("uid", uid));
	            params.add(new BasicNameValuePair("name", name));
	            params.add(new BasicNameValuePair("address", address));
	            params.add(new BasicNameValuePair("city", city));
	            params.add(new BasicNameValuePair("pincode", pincode));
	            params.add(new BasicNameValuePair("email", email));
	            params.add(new BasicNameValuePair("phonenumber", phonenumber));
	           
	 
	            // sending modified data through http request
	            // Notice that update product url accepts POST method
	            JSONParser jParser = new JSONParser();
	            JSONObject json2 = jParser.getJSONFromUrl(url_save_address, params);
	       		   
	    		return json2;
	    		
	   		    
	    	} 
	        
	       
	        /**
	         * After completing background task Dismiss the progress dialog
	         * **/
	        protected void onPostExecute(final JSONObject json2) {
	            // dismiss the dialog once product uupdated
	        	Toast.makeText(getApplicationContext(), "Address Saved",
	                    Toast.LENGTH_LONG).show();
	        	  Intent addressadd = new Intent(getApplicationContext(), UserAddress.class);
	        	  Bundle b=new Bundle();
		          b.putString("condition","UPDATEDADDRESS");
		          addressadd.putExtras(b);
		          addressadd.putStringArrayListExtra("productarray", ProductNameArray);
		          addressadd.putExtra("totalamount", TotalAmount);
		          addressadd.putExtra("countproducts", countproducts);

                  /**
                   * Close all views before launching Registered screen
                  **/
		          addressadd.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                  pDialog.dismiss();
                  startActivity(addressadd);
	        }
	    } 
	 
	 public boolean isNetworkAvailable() {
	       ConnectivityManager connectivityManager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
	       NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	       return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	   }
}