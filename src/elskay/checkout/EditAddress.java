package elskay.checkout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import elskay.app.R;

import elskay.app.R.id;
import elskay.app.R.layout;
import elskay.checkout.AddAddress.SaveAddress;
import elskay.login.DatabaseHandler;
import elskay.login.JSONParser;
import elskay.login.Registered;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class EditAddress extends Activity {
	/** Called when the activity is first created. */
 
	    EditText username;
	    EditText useraddress;
	    EditText usercity;
	    Button btnSave;
	    EditText userpincode;
	    EditText userphonenumber;
	    String uid;
	    ArrayList<String> ProductNameArray;
	    String TotalAmount;
	    String countproducts;
	    
	    // Progress Dialog
	    private ProgressDialog pDialog;
	 
	    // JSON parser class
	  
	 
	    // url to update product
	    private static final String url_update_address = "http://128.199.176.163/elskayapp/elskayapp_login/update_useraddress.php";
	 
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.user_address_edit);
		
		  Intent intent = getIntent();
	       Bundle bundle = intent.getExtras();
	       ProductNameArray = intent.getStringArrayListExtra("productarray");
	       TotalAmount = intent.getStringExtra("totalamount");
	       countproducts = intent.getStringExtra("countproducts");
	       String getname= bundle.getString("uname"); 
	       String getaddress= bundle.getString("uaddress"); 
	       String getcity= bundle.getString("ucity"); 
	       String getpincode= bundle.getString("upincode"); 
	       String getphonenumber= bundle.getString("uphonenumber"); 
	       
	         
	    Button edit = (Button)findViewById(R.id.Button01);    
	    Typeface custom_font12 = Typeface.createFromAsset(getAssets(), "AvenirNextLTPro-Regular.otf");
	    edit.setTypeface(custom_font12);     
		ImageButton back = (ImageButton)findViewById(R.id.backbutn);
		username = (EditText) findViewById(R.id.editname);
		useraddress = (EditText) findViewById(R.id.editaddress);
		userpincode = (EditText) findViewById(R.id.editpincode);
        usercity = (EditText) findViewById(R.id.editcity);
        userphonenumber = (EditText) findViewById(R.id.editphonenumber);
        btnSave = (Button)findViewById(R.id.editbuttonsave);
        
        Typeface custom_font1 = Typeface.createFromAsset(getAssets(), "AvenirNextLTPro-Regular.otf");
        username.setTypeface(custom_font1);
        Typeface custom_font2 = Typeface.createFromAsset(getAssets(), "AvenirNextLTPro-Regular.otf");
        useraddress.setTypeface(custom_font2);
        Typeface custom_font3 = Typeface.createFromAsset(getAssets(), "AvenirNextLTPro-Regular.otf");
        userpincode.setTypeface(custom_font3);
        Typeface custom_font4 = Typeface.createFromAsset(getAssets(), "AvenirNextLTPro-Regular.otf");
        btnSave.setTypeface(custom_font4);
        Typeface custom_font5 = Typeface.createFromAsset(getAssets(), "AvenirNextLTPro-Regular.otf");
        usercity.setTypeface(custom_font5);
        Typeface custom_font6 = Typeface.createFromAsset(getAssets(), "AvenirNextLTPro-Regular.otf");
        userphonenumber.setTypeface(custom_font6);
        
        username.setText(getname);
        useraddress.setText(getaddress);
        usercity.setText(getcity);
        userpincode.setText(getpincode);
        userphonenumber.setText(getphonenumber);
        
        DatabaseHandler db = new DatabaseHandler(getApplicationContext());

        HashMap<String,String> user = new HashMap<String, String>();
        user = db.getUserDetails();
        uid= user.get("uid");
	        
	        back.setOnClickListener(new OnClickListener()
			{

			@Override
			public void onClick(View v) {

				 Intent myIntent = new Intent(getApplicationContext(), UserAddress.class);
				 Bundle b=new Bundle();
	             b.putString("condition","BACK");
	             myIntent.putExtras(b);
	             myIntent.putStringArrayListExtra("productarray", ProductNameArray);
				 myIntent.putExtra("totalamount", TotalAmount);
				 myIntent.putExtra("countproducts", countproducts);
	             startActivityForResult(myIntent, 0);
	             finish();
				 

			}


			}); 
	        
	       
	        
	        // save button click event
	        btnSave.setOnClickListener(new View.OnClickListener() {
	 
	            @Override
	            public void onClick(View arg0) {
	            	
	                	
	                	
	                	if(isNetworkAvailable()){
	                        // do network operation here     
	                		new UpdateAddress().execute();
	                     }else{
	                        Toast.makeText(getApplicationContext(), "No Available Network. Please try again later", Toast.LENGTH_LONG).show();
	                        return;
	                      
	                     }
	               
	            }
	        });
	 
		     
        
		
		

	}
 
	 class UpdateAddress extends AsyncTask<String, String, JSONObject> {
		 
	        /**
	         * Before starting background thread Show Progress Dialog
	         * */
	       @Override
	        protected void onPreExecute() {
	            super.onPreExecute();
	            pDialog = new ProgressDialog(EditAddress.this);
	            pDialog.setMessage("Saving 	Address ...");
	            pDialog.setIndeterminate(false);
	            pDialog.setCancelable(true);
	            pDialog.show();
	        }
	 
	        /**
	         * Saving product
	         * */
	        
	        @Override
	        protected JSONObject doInBackground(String... args) {
	    		
	        	 // getting updated data from EditTexts
	            String name = username.getText().toString();
	            String address = useraddress.getText().toString();
	            String pincode = userpincode.getText().toString();
	            String phonenumber = userphonenumber.getText().toString();
	            String city = usercity.getText().toString();
	            
	 
	            // Building Parameters
	            List<NameValuePair> params = new ArrayList<NameValuePair>();
	            params.add(new BasicNameValuePair("uid", uid));
	            params.add(new BasicNameValuePair("name", name));
	            params.add(new BasicNameValuePair("address", address));
	            params.add(new BasicNameValuePair("city", city));
	            params.add(new BasicNameValuePair("pincode", pincode));
	            params.add(new BasicNameValuePair("phonenumber", phonenumber));
	           
	 
	            // sending modified data through http request
	            // Notice that update product url accepts POST method
	            JSONParser jParser = new JSONParser();
	            JSONObject json2 = jParser.getJSONFromUrl(url_update_address, params);
	       		   
	    		return json2;
	    		
	   		    
	    	} 
	        
	       
	        /**
	         * After completing background task Dismiss the progress dialog
	         * **/
	        protected void onPostExecute(final JSONObject json2) {
	            // dismiss the dialog once product uupdated
	        	Toast.makeText(getApplicationContext(), "Address Updated",
	                    Toast.LENGTH_SHORT).show();
	        	  Intent addressedited = new Intent(getApplicationContext(), UserAddress.class);
	        	  Bundle b=new Bundle();
		          b.putString("condition","UPDATEDADDRESS");
		          addressedited.putExtras(b);
	        	  addressedited.putStringArrayListExtra("productarray", ProductNameArray);
	        	  addressedited.putExtra("totalamount", TotalAmount);
	        	  addressedited.putExtra("countproducts", countproducts);

                  /**
                   * Close all views before launching Registered screen
                  **/
	        	  addressedited.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                  pDialog.dismiss();
                  startActivity(addressedited);
	        }
	    } 
	 
	 
	 public boolean isNetworkAvailable() {
	       ConnectivityManager connectivityManager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
	       NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	       return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	   }
	 
}