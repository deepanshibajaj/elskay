package elskay.checkout;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;
import elskay.addtocart.ECart;
import elskay.app.DJSONParserGet;
import elskay.app.EditProfile;
import elskay.app.MainTabActivity;
import elskay.app.Payu;
import elskay.app.R;
import elskay.checkout.EditAddress.UpdateAddress;

import elskay.login.DatabaseHandler;
import elskay.login.JSONParser;
import elskay.styleadvice.AdviceAsk;

public class PaymentMethod extends Activity {
	
	 String TotalAmount;
	 ArrayList<String> ProductNameArray;
	 String uname;
	 String uaddress;
	 String ucity;
	 String upincode;
	 String uphonenumber;
	 String uid;
	 JSONObject ob;
	 JSONArray mJSONArray;
	 String countproducts;
	 String useremail;
	 JSONObject transaction;
	 String txnid;
	 

	   // Progress Dialog
	  private ProgressDialog pDialog;
	  private static String KEY_SUCCESS = "success";
	  private static String KEY_TXNID = "tid";
	  private static final String url_get_txnid = "http://128.199.176.163/elskayapp/elskayshop/generate_txnid.php";
	  private static final String url_buy_product = "http://128.199.176.163/elskayapp/elskayshop/shop_cod_orderplaced.php";
	 
	  StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
	
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.product_paymentmethod);
		
		
		Intent intent = getIntent();
		ProductNameArray = intent.getStringArrayListExtra("productarray");
	    for (String value : ProductNameArray) {	
				 //System.out.println(value);
				 Log.i("value",value);
					}
	     
	     
	     TotalAmount = intent.getStringExtra("totalamount");
	     countproducts = intent.getStringExtra("countproducts");
	     uname=intent.getStringExtra("uname");
	     uaddress=intent.getStringExtra("uaddress");
	     ucity=intent.getStringExtra("ucity");
	     upincode=intent.getStringExtra("upincode");
	     uphonenumber=intent.getStringExtra("uphonenumber");
	     DatabaseHandler db = new DatabaseHandler(getApplicationContext());
	        HashMap<String,String> user = new HashMap<String, String>();
	        user = db.getUserDetails();
	        uid= user.get("uid");
	        useremail=user.get("email");
	   

         	if(isNetworkAvailable()){
                 // do network operation here     
         		new Gettransactionid().execute();
              }else{
                 Toast.makeText(getApplicationContext(), "No Available Network. Please try again later", Toast.LENGTH_LONG).show();
                 return;
               
              }   
		
         	 Button paytext = (Button)findViewById(R.id.Button01);    
 		    Typeface custom_font12 = Typeface.createFromAsset(getAssets(), "AvenirNextLTPro-Regular.otf");
 		    paytext.setTypeface(custom_font12); 
 		    
 		    
        ImageButton back = (ImageButton)findViewById(R.id.backbutn);
        
        back.setOnClickListener(new OnClickListener()
		{

		@Override
		public void onClick(View v) {

			 Intent myIntent = new Intent(getApplicationContext(), UserAddress.class);
			 Bundle b=new Bundle();
             b.putString("condition","BACK");
             myIntent.putExtras(b);
             myIntent.putStringArrayListExtra("productarray", ProductNameArray);
			 myIntent.putExtra("totalamount", TotalAmount);
			 myIntent.putExtra("countproducts", countproducts);
             startActivityForResult(myIntent, 0);
             finish();
			 

		}


		});
        
        Button payuu = (Button)findViewById(R.id.payubtn);
        
        payuu.setOnClickListener(new OnClickListener()
		{

		@Override
		public void onClick(View v) {

			 Intent myIntent = new Intent(getApplicationContext(), Payu.class);
			 myIntent.putStringArrayListExtra("productarray", ProductNameArray);
			 myIntent.putExtra("totalamount", TotalAmount);
			 myIntent.putExtra("countproducts", countproducts);
			 myIntent.putExtra("uname",uname);
        	 myIntent.putExtra("uaddress",uaddress);
        	 myIntent.putExtra("ucity",ucity);
        	 myIntent.putExtra("upincode",upincode);
        	 myIntent.putExtra("uphonenumber",uphonenumber);
        	 myIntent.putExtra("transactionid",txnid);
             startActivityForResult(myIntent, 0);
             finish();
			 

		}


		});
        
        
  
        Button ab = (Button)findViewById(R.id.btncashondelivery);
        ab.setOnClickListener(new OnClickListener()
		{

		@Override
		public void onClick(View v) {

			confirmDialog();
         	/*if(isNetworkAvailable()){
                 // do network operation here     
         		new SaveProductDetails().execute();
              }else{
                 Toast.makeText(getApplicationContext(), "No Available Network. Please try again later", Toast.LENGTH_LONG).show();
                 return;
               
              }
			 */

		}


		});
        
        
    
}
    
   private void confirmDialog() {      
       AlertDialog.Builder builder = new AlertDialog.Builder(PaymentMethod.this);
                
       builder
       .setMessage("Do you want to confirm this order?")
       .setPositiveButton("Confirm",  new DialogInterface.OnClickListener() {
           @Override
           public void onClick(DialogInterface dialog, int id) {
               // Yes-code
        	   if(isNetworkAvailable()){
                   // do network operation here     
           		new SaveProductDetails().execute();
                }else{
                   Toast.makeText(getApplicationContext(), "No Available Network. Please try again later", Toast.LENGTH_LONG).show();
                   return;
                 
                }
  			 
           }
       })
       .setNegativeButton("No", new DialogInterface.OnClickListener() {
           @Override
           public void onClick(DialogInterface dialog,int id) {
               dialog.cancel();
           }
       })
       .show();
   }
		
	class SaveProductDetails extends AsyncTask<String, String, JSONObject> {
		 
        /**
         * Before starting background thread Show Progress Dialog
         * */
       @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(PaymentMethod.this);
            pDialog.setMessage("Please wait ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }
 
        /**
         * Saving product
         * */
        
        @Override
        protected JSONObject doInBackground(String... args) {
    		
            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("uid", uid));
            params.add(new BasicNameValuePair("uname", uname));
            params.add(new BasicNameValuePair("uemail", useremail));
            params.add(new BasicNameValuePair("uaddress", uaddress));
            params.add(new BasicNameValuePair("ucity", ucity));
            params.add(new BasicNameValuePair("upincode", upincode));
            params.add(new BasicNameValuePair("uphonenumber", uphonenumber));
            params.add(new BasicNameValuePair("TotalAmount", TotalAmount));
            params.add(new BasicNameValuePair("countproducts", countproducts));
            
            //mJSONArray = new JSONArray(ProductNameArray);
            /*for(String products : mJSONArray) {

  	        	params.add(new BasicNameValuePair("products[]",mJSONArray.toString()));
  	        	System.out.print(products);

  	         }*/
            params.add(new BasicNameValuePair("product",ProductNameArray.toString()));
            //params.add(new BasicNameValuePair("product",ob.toString()));
            Log.i("params",params.toString());
            // sending modified data through http request
            // Notice that update product url accepts POST method
            JSONParser jParser = new JSONParser();
            
            JSONObject json2 = jParser.getJSONFromUrl(url_buy_product, params);
            //System.out.print(params.toString());
       		   
    		return json2;
    		
   		    
    	} 
        
       
        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(final JSONObject json2) {
            // dismiss the dialog once product uupdated
        	 try {
                 if (json2.getString(KEY_SUCCESS) != null) {

                      String res = json2.getString(KEY_SUCCESS);
                      Log.i("log_error",res);

                      if(Integer.parseInt(res) == 1){
                    	  Intent upanel = new Intent(getApplicationContext(), SuccessPage.class);
                          pDialog.dismiss();
                          startActivity(upanel);
                      }
                      else{

                    	  Intent upanel = new Intent(getApplicationContext(), OrderErrorPage.class);
                          pDialog.dismiss();
                          startActivity(upanel);
                          
                      }
                 
                 }
        	 }catch (JSONException e) {
                 e.printStackTrace();
             }
        }
    } 
	
	 public boolean isNetworkAvailable() {
	       ConnectivityManager connectivityManager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
	       NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	       return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	   }
	 
	 
	 class Gettransactionid extends AsyncTask<String, String, JSONObject> {
		  
	        /**
	         * Before starting background thread Show Progress Dialog
	         * */
	        @Override
	        protected void onPreExecute() {
	            super.onPreExecute();
	            pDialog = new ProgressDialog(PaymentMethod.this);
	            pDialog.setMessage("Loading...");
	            pDialog.setIndeterminate(false);
	            pDialog.setCancelable(true);
	            pDialog.show();
	        }
	 
	        /**
	         * Getting product details in background thread
	         * */
	        
	        @Override
	        protected JSONObject doInBackground(String... args) {
	    		
	        	DJSONParserGet jParser = new DJSONParserGet();
	       		JSONObject json = jParser.getJSONFromUrl(url_get_txnid);
	       		   
	    		return json;
	    		
	   		    
	    	}
	       
	        /**
	         * After completing background task Dismiss the progress dialog
	         * **/
	        @Override
	        protected void onPostExecute(final JSONObject json) {
	   		
	   	
	   		 pDialog.dismiss();
	   		
	   		 try {
	   			 
	   		  txnid = json.getString(KEY_TXNID);
	   		  Log.i("transaction", txnid);
	   		  pDialog.dismiss();
				
	           
	   		}
			 
	         
	        
	   		 catch (JSONException ignored) {
   			 
   			 
   		 }
   			

	    }
	    }
}
