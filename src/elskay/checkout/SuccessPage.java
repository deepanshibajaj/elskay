package elskay.checkout;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import elskay.app.MainTabActivity;
import elskay.app.R;

public class SuccessPage extends Activity {
	
	 SharedPreferences settings;
	@Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.order_successpage);
        
        Button buttonshop = (Button)findViewById(R.id.buttonshoppage);
        settings = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = settings.edit();
        editor.clear();
        editor.commit();
        
        buttonshop.setOnClickListener(new OnClickListener()
		{

		@Override
		public void onClick(View v) {

			 Intent myIntent = new Intent(getApplicationContext(), MainTabActivity.class);
             
             startActivityForResult(myIntent, 0);
             finish();
			 

		}


		});





} 

}
