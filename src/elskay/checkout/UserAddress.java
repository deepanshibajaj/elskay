package elskay.checkout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.squareup.picasso.Picasso;

import elskay.addtocart.ECart;
import elskay.app.R;
import elskay.app.R.id;
import elskay.app.R.layout;
import elskay.login.DatabaseHandler;
import elskay.login.JSONParser;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

 
public class UserAddress extends Activity {
	/** Called when the activity is first created. */
    
	

	TextView address;
    String uidd;
    
    String uname;
    String uaddress;
    String upincode;
    String ucity;
    String uphonenumber;
    Button paymentpg;
    Button addaddress;
    String con;
    String TotalAmount;
    String countproducts;
    ArrayList<String> ProductNameArray;
    
    // Progress Dialog
    private ProgressDialog pDialog;
    JSONObject addressobj;
	
    private static final String url = "http://128.199.176.163/elskayapp/elskayapp_login/get_useraddress.php";
    
    private static final String TAG_USER = "useraddress";
    private static final String TAG_UID = "uid";
    private static final String TAG_USERNAME = "name";
    private static final String TAG_USERADDRESS = "address";
    private static final String TAG_USERPINCODE = "pincode";
    private static final String TAG_USERCITY = "city";
    private static final String TAG_USERPHONENUMBER = "phonenumber";
    
    SharedPreferences storeproducts;
    
    
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.user_address);
		
		storeproducts = PreferenceManager.getDefaultSharedPreferences(this);
		
		Bundle b=getIntent().getExtras();
	     con=b.getString("condition");
		  
         if(con.equals("CART"))
	     {
        	 Intent intent = getIntent();
    		 ProductNameArray = intent.getStringArrayListExtra("productarray");
    			
    			 for (String value : ProductNameArray) {
    					
    				 System.out.println(value);
    				 
    					}
    			 TotalAmount = intent.getStringExtra("totalamount");
    			 countproducts = intent.getStringExtra("productcount");
    			 Log.i("totalamount",TotalAmount);
    			 Log.i("countproducts",countproducts);
    			 
    			
	    
	     }
         else if(con.equals("BACK"))
          {  
        	 Intent intent = getIntent();
        	 ProductNameArray = intent.getStringArrayListExtra("productarray");
        	 TotalAmount = intent.getStringExtra("totalamount");
        	 countproducts = intent.getStringExtra("countproducts");
           }
         else if(con.equals("UPDATEDADDRESS"))
         {  
       	 Intent intent = getIntent();
       	 ProductNameArray = intent.getStringArrayListExtra("productarray");
       	 TotalAmount = intent.getStringExtra("totalamount");
       	countproducts = intent.getStringExtra("countproducts");
          }
		
			 
         Button add = (Button)findViewById(R.id.Button01);
         
         Typeface custom_font3 = Typeface.createFromAsset(getAssets(), "AvenirNextLTPro-Regular.otf");
         add.setTypeface(custom_font3);
		 
		
		address = (TextView)findViewById(R.id.address);
		Typeface custom_font = Typeface.createFromAsset(getAssets(), "AvenirNextLTPro-Demi.otf");
		address.setTypeface(custom_font);
		//address.setVisibility(View.GONE);
		
		
		paymentpg = (Button)findViewById(R.id.btnpaymentpage);
		Typeface custom_font2 = Typeface.createFromAsset(getAssets(), "AvenirNextLTPro-Regular.otf");
		paymentpg.setTypeface(custom_font2);
		paymentpg.setOnClickListener(new OnClickListener()
		{

		@Override
		public void onClick(View v) {
			
			
			 Intent myIntent = new Intent(getApplicationContext(), PaymentMethod.class);
			 myIntent.putStringArrayListExtra("productarray", ProductNameArray);
			 myIntent.putExtra("totalamount", TotalAmount);
			 myIntent.putExtra("countproducts", countproducts);
			 myIntent.putExtra("uname",uname);
        	 myIntent.putExtra("uaddress",uaddress);
        	 myIntent.putExtra("ucity",ucity);
        	 myIntent.putExtra("upincode",upincode);
        	 myIntent.putExtra("uphonenumber",uphonenumber);
             startActivityForResult(myIntent, 0);
             finish();

		
		}


		}); 
		
		addaddress = (Button)findViewById(R.id.addaddress);
		Typeface custom_font4 = Typeface.createFromAsset(getAssets(), "AvenirNextLTPro-Regular.otf");
		addaddress.setTypeface(custom_font4);
        addaddress.setOnClickListener(new OnClickListener()
		{

		@Override
		public void onClick(View v) {
			
			 Intent myIntent = new Intent(getApplicationContext(), AddAddress.class);
			 myIntent.putStringArrayListExtra("productarray", ProductNameArray);
			 myIntent.putExtra("countproducts", countproducts);
			 myIntent.putExtra("totalamount", TotalAmount);
             startActivityForResult(myIntent, 0);
             finish();

		
		}


		}); 
        
        DatabaseHandler db = new DatabaseHandler(getApplicationContext());

        HashMap<String,String> user = new HashMap<String, String>();
        user = db.getUserDetails();
        uidd= user.get("uid");
        
        //new GetUserAddress().execute();
        
    	if(isNetworkAvailable()){
            // do network operation here     
    		 new GetUserAddress().execute();
         }else{
            Toast.makeText(getApplicationContext(), "No Available Network. Please try again later", Toast.LENGTH_LONG).show();
            return;
          
         }
        
        ImageButton back = (ImageButton)findViewById(R.id.backbutn);
        
        back.setOnClickListener(new OnClickListener()
		{

		@Override
		public void onClick(View v) {

			 Intent myIntent = new Intent(getApplicationContext(), ECart.class);
			 
             startActivityForResult(myIntent, 0);
             finish();
			 

		}


		});
        
        address.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
            	
            	 String getaddress = address.getText().toString();
            	 
            	 Intent myIntent = new Intent(getApplicationContext(), EditAddress.class);
            	 myIntent.putStringArrayListExtra("productarray", ProductNameArray);
    			 myIntent.putExtra("countproducts", countproducts);
    			 myIntent.putExtra("totalamount", TotalAmount);
            	 myIntent.putExtra("uname",uname);
            	 myIntent.putExtra("uaddress",uaddress);
            	 myIntent.putExtra("ucity",ucity);
            	 myIntent.putExtra("upincode",upincode);
            	 myIntent.putExtra("uphonenumber",uphonenumber);
                 startActivityForResult(myIntent, 0);
                 finish();
            }
        });
        
		
		

	}
	
	  class GetUserAddress extends AsyncTask<String, String, JSONObject> {
		  
	        /**
	         * Before starting background thread Show Progress Dialog
	         * */
	        @Override
	        protected void onPreExecute() {
	            super.onPreExecute();
	            pDialog = new ProgressDialog(UserAddress.this);
	            pDialog.setMessage("Loading...");
	            pDialog.setIndeterminate(false);
	            pDialog.setCancelable(true);
	            pDialog.show();
	        }
	 
	        /**
	         * Getting product details in background thread
	         * */
	        
	        @Override
	        protected JSONObject doInBackground(String... args) {
	    		
	        	 JSONParser jParser = new JSONParser();
	       		 List<NameValuePair> params = new ArrayList<NameValuePair>();
	     	       params.add(new BasicNameValuePair("uid",uidd));
	     		   JSONObject json = jParser.getJSONFromUrl(url,params);
	       		   
	    		return json;
	    		
	   		    
	    	}
	       
	        /**
	         * After completing background task Dismiss the progress dialog
	         * **/
	        @Override
	        protected void onPostExecute(final JSONObject json) {
	   		
	   	
	   		 pDialog.dismiss();
	   		
	   		 try {
	   			 
	   			 
	   			JSONArray addObj = json
	                    .getJSONArray(TAG_USER); // JSON Array

	            // get first product object from JSON Array
	            addressobj = addObj.getJSONObject(0);

	            // product with this pid found
	            // Edit Text
	            
	           
	            address.setText(addressobj.getString(TAG_USERNAME)+"\n"+addressobj.getString(TAG_USERADDRESS)+"\n"+addressobj.getString(TAG_USERCITY)
	            		+"-"+addressobj.getString(TAG_USERPINCODE)
	            		+"\n\n"+addressobj.getString(TAG_USERPHONENUMBER));
				
	            
				uname=addressobj.getString(TAG_USERNAME);
				uaddress=addressobj.getString(TAG_USERADDRESS);
				ucity=addressobj.getString(TAG_USERCITY);
				upincode=addressobj.getString(TAG_USERPINCODE);
				uphonenumber=addressobj.getString(TAG_USERPHONENUMBER);
				
	           
	   		}
			 
	         
	        
	   		 catch (JSONException ignored) {
      			 
      			 
      		 }
      			if (addressobj == null) {
      				address.setVisibility(View.GONE);
      				addaddress.setVisibility(View.VISIBLE);
      			    Toast.makeText(getApplicationContext(), "Please Add Your Address details", Toast.LENGTH_SHORT).show();
      			   paymentpg.setEnabled(false); 
      			}
      			else {
      				address.setVisibility(View.VISIBLE);
      				addaddress.setVisibility(View.GONE);
      			   
      			}

       		
	    }
	    }
	  
	  public boolean isNetworkAvailable() {
	       ConnectivityManager connectivityManager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
	       NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	       return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	   }
 
}