package elskay.fblogin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.facebook.login.widget.ProfilePictureView;

import elskay.app.MainTabActivity;
import elskay.app.R;
import elskay.login.DatabaseHandler;
import elskay.login.JSONParser;
import elskay.login.Login;
import elskay.login.PublicProfile;
import elskay.login.Register;
import elskay.login.SessionManager;
import elskay.login.UserFunctions;




/**
 * A placeholder fragment containing a simple view.
 */
public class FragmentSimpleLoginButton extends FragmentActivity {

    private CallbackManager mCallbackManager;
    private AccessTokenTracker mTokenTracker;
    private ProfileTracker mProfileTracker;
    String facebook_id,f_name, m_name, l_name, gender, profile_image, full_name, email_id,name;
    private static final String url_update_product = "http://128.199.176.163/elskayapp/elskayapp_login/elskay_fblogin.php";
    CallbackManager callbackManager;
    private SessionManager session;
    GraphRequest request;
	//private DatabaseHandler db;
    
    private FacebookCallback<LoginResult> mFacebookCallback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            Log.d("VIVZ", "onSuccess");
            AccessToken accessToken = loginResult.getAccessToken();
           
            request = GraphRequest.newMeRequest(
                    loginResult.getAccessToken(),
                    new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                      public void onCompleted(
                        JSONObject object,
                        GraphResponse response) {
                        // Application code 
                    	if (response != null) {
                        try {
                         facebook_id = object.getString("id");
                        // name = object.getString("name");
                          f_name = object.getString("first_name");
                          l_name = object.getString("last_name");
                          if (object.has("email"))
                          {
                        	  email_id =object.getString("email");
                          }
                          else{
                            email_id= "";
                            
                          }
                          profile_image ="https://graph.facebook.com/"+facebook_id+"/picture?width=300&height=300";
                          /*Log.i("facebookid",facebook_id);
                          Log.i("f_name",f_name);
                          Log.i("l_name",l_name);
                          Log.i("email_id",email_id);
                          Log.i("profile_image",profile_image);*/
                         
                        
                          new ProcessRegister().execute();
                         // Toast.makeText(getApplicationContext(), "HI," + l_name + f_name+ facebook_id+ "email_id: " + email_id, Toast.LENGTH_SHORT).show();
                         /* Log.i("filename@@",f_name);
                          db = new DatabaseHandler(getApplicationContext());
                          
                          if (email_id != null)
                          {
                          db.resetTables(); 	  
                          db.addUser(f_name,l_name,email_id,facebook_id,l_name);
                          }
                          else
                          {
                        	db.resetTables();
                        	db.addUser("guest","guest","guest@gmail.com","1234","guest");  
                          }*/
                          
                          
                        } catch (JSONException e) {
                          Toast.makeText(getApplicationContext(), "Sorry,some error occured"+e, Toast.LENGTH_SHORT).show();
                          e.printStackTrace(); 

                        }
                    }
                    }
                    });
            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,first_name,email,last_name");
            request.setParameters(parameters);
            request.executeAsync();
            
            
           // new ProcessRegister().execute();
            
            
           
            
        }

        @Override
        public void onCancel() {
            Log.d("elskay", "onCancel");
        }

        @Override
        public void onError(FacebookException e) {
            Log.d("elskay", "onError " + e);
        }
        
        
        
                
    };
	

	
	

    
    public FragmentSimpleLoginButton() {
    }

    
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); 
        setContentView(R.layout.fragment_simple_login_button);
        
        session = new SessionManager(getApplicationContext());
        
         //Check if user is already logged in or not
        if (session.isLoggedIn()) {
            // User is already logged in. Take him to main activity
            Intent intent = new Intent(FragmentSimpleLoginButton.this, MainTabActivity.class);
            /*Bundle b=new Bundle();
             b.putString("condition","BUY");
             intent.putExtras(b);*/
            startActivity(intent);
            finish();
        } 
        
        
        Button login = (Button)findViewById(R.id.login);
        
        login.setOnClickListener(new OnClickListener()
		{

		@Override
		public void onClick(View v) {
			
			
			Intent myIntent = new Intent();
			myIntent = new Intent(FragmentSimpleLoginButton.this, Login.class); 
			startActivity(myIntent);
			 

		}

		});
        
        
       Button signup = (Button)findViewById(R.id.signup);
        
       signup.setOnClickListener(new OnClickListener()
		{

		@Override
		public void onClick(View v) {
			
			
			Intent myIntent = new Intent();
			myIntent = new Intent(FragmentSimpleLoginButton.this, Register.class); 
			startActivity(myIntent);
			 

		}

		});
      
        mCallbackManager = CallbackManager.Factory.create();
        setupTokenTracker();
        setupProfileTracker();

        mTokenTracker.startTracking();
        mProfileTracker.startTracking();
        
        setupLoginButton();
        
        
        
    }


	@Override
    public void onResume() {
        super.onResume();
       
    }

    @Override
    public void onStop() {
        super.onStop();
        mTokenTracker.stopTracking();
       mProfileTracker.stopTracking();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
       
        
        
    }


    private void setupTokenTracker() {
        mTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
                Log.d("elskay", "" + currentAccessToken);
            }
        };
    }

    private void setupProfileTracker() {
        mProfileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {
                Log.d("elskay", "" + currentProfile);
               
               
            }
        };
    }

    private void setupLoginButton() {
    	
        LoginButton mButtonLogin = (LoginButton)findViewById(R.id.login_button);
        mButtonLogin.setReadPermissions(Arrays.asList("email","public_profile"));
        mButtonLogin.registerCallback(mCallbackManager, mFacebookCallback);
        

       

    }
    
    

    private class ProcessRegister extends AsyncTask<String, String, JSONObject> {

    	/**
    	 * Defining Process dialog
    	 **/
    	        private ProgressDialog pDialog;
                
    	       
    	        @Override
    	        protected void onPreExecute() {
    	            super.onPreExecute();
    	          
    	         
    	            pDialog = new ProgressDialog(FragmentSimpleLoginButton.this);
    	            //pDialog.setTitle("Contacting Servers");
    	            pDialog.setMessage("Searching for a hanger to put your information in  our wardrobe.");
    	            pDialog.setIndeterminate(false);
    	            pDialog.setCancelable(true);
    	            pDialog.show();
    	        }

    	        
    	        
    	        
    	        @Override
    	        protected JSONObject doInBackground(String... args) {


    	        	 // Building Parameters
    	        	 

    	            /* HashMap<String,String> user = new HashMap<String, String>();
    	             user = db.getUserDetails();
    	             String fname= user.get("fname"); 
    	             String lname= user.get("lname"); 
    	             String emailid= user.get("email"); 
    	             String fbiid= user.get("uid"); 
    	             String profileimage= "https://graph.facebook.com/"+fbiid+"/picture?width=300&height=300";*/
    	             
    	             
    	             String fname= f_name; 
    	             String lname= l_name; 
    	             String emailid= email_id; 
    	             String fbiid= facebook_id; 
    	             String profileimage= "https://graph.facebook.com/"+fbiid+"/picture?width=300&height=300";
    	             
    	             DatabaseHandler db = new DatabaseHandler(getApplicationContext());
    	             db.resetTables();
    	             db.addUser(fname,lname,emailid,fbiid,profileimage);
    	             
    	        
    	            List<NameValuePair> params = new ArrayList<NameValuePair>();
    	            params.add(new BasicNameValuePair("firstname", fname));
    	            params.add(new BasicNameValuePair("lastname", lname));
    	            params.add(new BasicNameValuePair("email", emailid));
    	            params.add(new BasicNameValuePair("fbid", fbiid));
    	            params.add(new BasicNameValuePair("profileimage", profileimage));
    	           
    	            JSONParser jParser = new JSONParser();
    	            JSONObject json = jParser.getJSONFromUrl(url_update_product, params);
    	       		   

    	            return json;


    	        }
    	        protected void onPostExecute(final JSONObject json) {
    	            // dismiss the dialog once product uupdated
    	        	session.setLogin(true);
    	            Intent upanel = new Intent(FragmentSimpleLoginButton.this, MainTabActivity.class);
                    upanel.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    /*Bundle b=new Bundle();
                    b.putString("condition","BUY");
                    upanel.putExtras(b);*/
                    pDialog.dismiss();
                    startActivity(upanel);
    	        }
    	            }
    
 
    
}
