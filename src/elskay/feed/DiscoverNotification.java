package elskay.feed;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.bhavin.horizontallistview.HorizontalListView;
import com.squareup.picasso.Picasso;

import elskay.app.DJSONParserPost;
import elskay.app.MainTabActivity;
import elskay.app.R;
import elskay.login.DatabaseHandler;
import elskay.orders.ManageOrderItems;
import elskay.orders.ManageOrders;


public class DiscoverNotification extends Activity{
	
	
	

	EditText tx;
	ImageButton button;
	ImageView img;
	String commentcount;
	String username;
	JSONObject c;
	JSONObject feedob;
	

    private ProgressDialog pDialog;
    private ProgressBar bar;
    
    HorizontalListView hlvSimple;
    
    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
    // JSON parser class
    DJSONParserPost jsonParser = new DJSONParserPost();  
    List<HashMap<String, String>> oslist = new ArrayList<HashMap<String, String>>();
    private static String url_getuserlikes = "http://128.199.176.163/elskayapp/elskayfeed/get_userfeedlikes.php";

   	  private static final String TAG_OS = "feed";
      	private static final String TAG_LIKE_ID = "lid";
      private static final String TAG_FEED_ID = "fid";
      private static final String TAG_FEEDUSER_ID = "feeduserid";
      private static final String TAG_LIKEUSERID = "likeuid";
      private static final String TAG_LIKEUNAME = "likeuname";
      private static final String TAG_FEEDIMAGE = "feedimage";
      private static final String TAG_FEEDHASHTAG = "feedhashtag";
      private static final String TAG_FEEDUNAME = "feeduname";
      private static final String TAG_FEEDLOCATION = "feedlocation";
      private static final String TAG_FEEDCREATEDAT = "created_at";
      private static final String TAG_FEEDPROFILEIMAGE= "profileimage";
      private static final String TAG_FEEDUNIQUEID = "unique_id";
      
  
      
   	JSONArray recomments = null;
   	JSONArray userfeed = null;

	private String qqid;
	int countt;
	
     int pidd;
	 String pid ;
	 String us;
	 String uid;
     String usernameee;
	 String imgcomment;
	 String adviceques;
	 ListView listView;
	 TextView ques;
	 String ccount;
	 String discoverusername;
	 String discoverprofileimage;
	 String discoverlocation;
	 String discoveruid;
	 String feedimage;
	 private ActionBar actionBar;
	 private Menu mToolbarMenu;
	 TextView orderid;
	 ImageView bg;
     TextView ebg;
	 HashMap<String,String> user;
	 HashMap<String, String> map2 = new HashMap<String, String>();
	 
	 int coot;
 
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); 
        setContentView(R.layout.discover_notification_list);
        
        bg = (ImageView)findViewById(R.id.nolikesimg);
        ebg = (TextView)findViewById(R.id.nolikestext);
 
       
        DatabaseHandler db = new DatabaseHandler(getApplicationContext());

        HashMap<String,String> user = new HashMap<String, String>();
        user = db.getUserDetails();
        uid= user.get("uid");
        username= user.get("fname");

         listView = (ListView) findViewById(android.R.id.list);
         

         ImageButton back = (ImageButton)findViewById(R.id.backbutn);
         
         back.setOnClickListener(new OnClickListener()
 		{

 		@Override
 		public void onClick(View v) {
 			 Intent myIntent = new Intent(getApplicationContext(), MainTabActivity.class);
             startActivityForResult(myIntent, 0);
             finish();
 			 

 		}


 		}); 
         
         listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

	            @Override
	            public void onItemClick(AdapterView<?> parent, View view,
	                                    int position, long id) {
	            	
	            	 
	            	 
	            	String fid = oslist.get(position).get(TAG_FEED_ID);
	 				String feeduname = oslist.get(position).get(TAG_FEEDUNAME);
	 				String feedimage = oslist.get(position).get(TAG_FEEDIMAGE);
	 				String feedhashtag = oslist.get(position).get(TAG_FEEDHASHTAG);
	 				String feedprofileimage = oslist.get(position).get(TAG_FEEDPROFILEIMAGE);
	 				String feedlocation = oslist.get(position).get(TAG_FEEDLOCATION);
	 				String feeduid = oslist.get(position).get(TAG_FEEDUNIQUEID);
	            
	            	
	            	  Intent myIntent = new Intent(getApplicationContext(),
	            			  DiscoverView.class);
	            	  myIntent.putExtra("feedimage",feedimage);
	 				 myIntent.putExtra("feedhashtag",feedhashtag);
	 				 myIntent.putExtra("feedid",fid);
	 				 myIntent.putExtra("feeduname",feeduname);
	 				 myIntent.putExtra("feedprofileimage",feedprofileimage);
	 				 myIntent.putExtra("feedlocation",feedlocation);
	 				 myIntent.putExtra("feeduid",feeduid);
                   
                     startActivity(myIntent);

	            }
	        });
         
         
         
       
		
		
	   
         // Getting complete product details in background thread
	        if(isNetworkAvailable()){
	            // do network operation here     
	      	   new JSONParses().execute();
	         }else{
	            Toast.makeText(this, "No Available Network. Please try again later", Toast.LENGTH_LONG).show();
	            return;
	         }
        
        oslist = new ArrayList<HashMap<String, String>>();
        
        
    }
 

    private class JSONParses extends AsyncTask<String, String, JSONObject> {
    	 private ProgressDialog pDialog;
    	@Override
        protected void onPreExecute() {
            super.onPreExecute();
                			
           pDialog = new ProgressDialog(DiscoverNotification.this);
            pDialog.setMessage("Loading....");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show(); 
          
            
            
            
    	}
    	
    	@Override
        protected JSONObject doInBackground(String... args) {
    		
    		DJSONParserPost jParser = new DJSONParserPost();
    		 List<NameValuePair> params = new ArrayList<NameValuePair>();
  	       params.add(new BasicNameValuePair("uid", uid));
  		   JSONObject json = jParser.getJSONFromUrl(url_getuserlikes,params);
    		return json;
    		
   		    
    	}
    	 
    	
    	@Override
         protected void onPostExecute(final JSONObject json) {
    		
    		//bar.setVisibility(View.GONE);
    		pDialog.dismiss();
    		
    		 try {
    				// Getting JSON Array from URL
    			 recomments = json.getJSONArray(TAG_OS);
    			
    			// recomments = json.getJSONArray(TAG_OSFEED);
    				for(int i = 0; i < recomments.length(); i++){
    				c = recomments.getJSONObject(i);
    				
    				// Storing  JSON item in a Variable
    				String lid = c.getString(TAG_LIKE_ID);
    				String feedid = c.getString(TAG_FEED_ID);
    				String feeduserid = c.getString(TAG_FEEDUSER_ID);
    				String likeuserid = c.getString(TAG_LIKEUSERID);
    				String likeusername = c.getString(TAG_LIKEUNAME);
    				
    				String feedimage = c.getString(TAG_FEEDIMAGE);
    				String feedprofileimage = c.getString(TAG_FEEDPROFILEIMAGE);
    				String feeddesc = c.getString(TAG_FEEDHASHTAG);
    				String feeduname = c.getString(TAG_FEEDUNAME);
    				String feedlocation = c.getString(TAG_FEEDLOCATION);
    				String feeduniqueid = c.getString(TAG_FEEDUNIQUEID);
    				

    				
        				
        			
    		        // Adding value HashMap key => value
    				HashMap<String, String> map = new HashMap<String, String>();
    				
    				map.put(TAG_LIKE_ID, lid);
    				map.put(TAG_FEED_ID, feedid);
    				map.put(TAG_FEEDUSER_ID, feeduserid);
    				map.put(TAG_LIKEUSERID, likeuserid);
    				map.put(TAG_LIKEUNAME, likeusername);
    				map.put(TAG_FEEDIMAGE, feedimage);
    				map.put(TAG_FEEDPROFILEIMAGE, feedprofileimage);
    				map.put(TAG_FEEDHASHTAG, feeddesc);
    				map.put(TAG_FEEDUNAME, feeduname);
    				map.put(TAG_FEEDLOCATION, feedlocation);
    				map.put(TAG_FEEDUNIQUEID, feeduniqueid);
    				
    				
    				
    				
    				
    				oslist.add(map);
    				
    				
    			

    				
                   }
    				
    				
        				
    				
    				
    				
    	        
    ListAdapter adapter = new SimpleAdapter(
    	DiscoverNotification.this, oslist,
            R.layout.discover_notification_list_item, new String[] {
    			TAG_LIKEUNAME,TAG_FEEDIMAGE},
            new int[] { R.id.likephoto,R.id.imageView2}){
        @Override
    public View getView(int pos, View convertView, ViewGroup parent){
        View v = convertView;
        if(v== null){
            LayoutInflater vi = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v=vi.inflate(R.layout.discover_notification_list_item, null);
        }
        orderid = (TextView)v.findViewById(R.id.likephoto);
        orderid.setText(oslist.get(pos).get(TAG_LIKEUNAME)+" "+"likes your photo");
        
        String imageurl=oslist.get(pos).get(TAG_FEEDIMAGE).replace(" ", "%20");
        
        ImageView productimg = (ImageView)v.findViewById(R.id.imageView2);
       
        Picasso.with(v.getContext()).load(imageurl).into(productimg);
      
        
 
      
        return v;
    }
   
       
};
    
// updating listview
listView.setAdapter(adapter);
   
	
   
   
    //Toast.makeText(getApplicationContext(), "Total number of Items are:" + adapter.getCount() , Toast.LENGTH_LONG).show();

    		 }
    		 
    		 
    		
    		 
    		 
    		 
                             
              catch (JSONException ignored) {
      			 
      			 
      		 }
      			if (c == null) {
      				
      			  bg.setVisibility(View.VISIBLE);
      			ebg.setVisibility(View.VISIBLE);
      			 
      			}
      			else {
      				
      				bg.setVisibility(View.GONE);
	      			ebg.setVisibility(View.GONE);
      				
      				
      			}

     		

     		 
     	 
    	 }

			 
                             
                             
    }
    
    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}
