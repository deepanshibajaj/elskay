package elskay.feed;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.squareup.picasso.Picasso;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import elskay.app.EditProfile;
import elskay.app.R;

import elskay.login.DatabaseHandler;
import elskay.login.JSONParser;
import elskay.login.PublicProfile;
import elskay.login.UserProfile;

public class DiscoverView extends Activity{
	
	
	
	
	 String discoverid;
     String discoverimage;
	 String discoverdescription;
	 String discoverusername;
	 String discoverprofileimage;
	 String discoverlocation;
	 String discovercommentcount;
	 String discoverlikecount;
	 String discoveruid;
	 String discoverlikeid;
	 String uid;
     String uname;
	 Boolean clicked;
	 ImageButton disclike;
	 TextView disccommentcount;
	 TextView disclikecount;
	 
	 private ProgressDialog pDialog;
	 
	 private static final String url_get_likes = "http://128.199.176.163/elskayapp/elskayfeed/getfeed_like.php";
	 
	 private static String KEY_LIKED = "lid";
	 private static String KEY_COMMENTCOUNT = "commentcount";
	 private static String KEY_LIKECOUNT = "likecount";
	 
	 StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
	 
	  @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        overridePendingTransition(R.anim.anim_slide_in_left, 0);
	        requestWindowFeature(Window.FEATURE_NO_TITLE); 
	        setContentView(R.layout.discoverview);
	        
	        
	        
	        
	        DatabaseHandler db = new DatabaseHandler(getApplicationContext());

	        HashMap<String,String> user = new HashMap<String, String>();
	        user = db.getUserDetails();
	        uid= user.get("uid");
	        uname=user.get("fname");
	        
	        Intent intent = getIntent();
	        Bundle bundle = intent.getExtras();
	        discoverid= bundle.getString("feedid"); 
	        discoverimage=bundle.getString("feedimage");
	        discoverdescription =bundle.getString("feedhashtag");
	        discoverusername =bundle.getString("feeduname");
	        discoverprofileimage =bundle.getString("feedprofileimage");
	        discoverlocation =bundle.getString("feedlocation");
	        //discovercommentcount =bundle.getString("feedcommentcount");
	        //discoverlikecount =bundle.getString("feedlikecount");
	        discoveruid =bundle.getString("feeduid");
	        //discoverlikeid =bundle.getString("feedlikeid");
	        
	        
	        ImageView discimage = (ImageView)findViewById(R.id.discoverimage);
	        TextView discdescription = (TextView)findViewById(R.id.discoverdesc);
	        TextView discusername = (TextView)findViewById(R.id.hashtaguser);
	        disccommentcount = (TextView)findViewById(R.id.countcomment);
	        disclikecount = (TextView)findViewById(R.id.likecount);
	        TextView disclocation = (TextView)findViewById(R.id.discoverlocation);
	        ImageButton disprofileimage = (ImageButton)findViewById(R.id.profiletwo);
	        ImageButton discomment= (ImageButton)findViewById(R.id.discovercomment);
	        disclike= (ImageButton)findViewById(R.id.dislikebtn);
	        
	        
	        discdescription.setText(discoverdescription);
	        discusername.setText(discoverusername);
	        disclocation.setText(discoverlocation);
	        
	        if(isNetworkAvailable()){
                // do network operation here     
        		new Getlikes().execute();
             }else{
                Toast.makeText(getApplicationContext(), "No Available Network. Please try again later", Toast.LENGTH_LONG).show();
                return;
              
             }
	        
	        Typeface custom_font1 = Typeface.createFromAsset(getAssets(),
	         	      "AvenirNextLTPro-Bold.otf");
	        discusername.setTypeface(custom_font1);
	        
	        Typeface custom_font2 = Typeface.createFromAsset(getAssets(),
	         	      "AvenirNextLTPro-Regular.otf");
	        discdescription.setTypeface(custom_font2);
	        
	        Typeface custom_font3 = Typeface.createFromAsset(getAssets(),
	         	      "AvenirNextLTPro-Regular.otf");
	        disclocation.setTypeface(custom_font3);
	        
	        String Filename =discoverimage.replace(" ", "%20");
	        String Filenametwo =discoverprofileimage.replace(" ", "%20");
			 
	         discimage.setImageResource(R.drawable.ic_launcher);
			  Picasso.with(getApplicationContext())
	          .load(Filename)
	          .error(R.drawable.eloader)
	          .placeholder( R.drawable.eloader )
	          .into(discimage);
			  
			  disprofileimage.setImageResource(R.drawable.ic_launcher);
				 Picasso.with(getApplicationContext())
		         .load(Filenametwo)
		         .resize(100, 100)
		         //.transform(new RoundedTransformation(100, 80))
		         .transform(new CircleTransform())
		         .into(disprofileimage);  
				 
				 
				 discomment.setOnClickListener(new OnClickListener()
					{

					@Override
					public void onClick(View v) {
						
						Intent myIntent = new Intent();
						 myIntent = new Intent(getApplicationContext(), FeedComment.class);
						 //myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						 
						 myIntent.putExtra("feedimage",discoverimage);
						 myIntent.putExtra("feedhashtag",discoverdescription);
						 myIntent.putExtra("feedid",discoverid);
						 myIntent.putExtra("feeduname",discoverusername);
						 myIntent.putExtra("feedprofileimage",discoverprofileimage);
						 myIntent.putExtra("feedlocation",discoverlocation);
						 myIntent.putExtra("feeduid",discoveruid);
						 finish();
						 startActivityForResult(myIntent, 2);
						 overridePendingTransition(R.anim.anim_slide_in_left, 0); 
						  

					}


					});	
				 
				 

				 disclikecount.setOnClickListener(new OnClickListener()
					{

					@Override
					public void onClick(View v) {
						
						 Intent myIntent = new Intent();
						 myIntent = new Intent(getApplicationContext(), DiscoverViewLikes.class);
						 myIntent.putExtra("feedid",discoverid);
						 finish();
						 startActivityForResult(myIntent, 2);
						 overridePendingTransition(R.anim.anim_slide_in_left, 0); 
						  

					}


					});	
				 
				 
				 ImageButton back = (ImageButton)findViewById(R.id.backbutn);
			        
			        back.setOnClickListener(new OnClickListener()
					{

					@Override
					public void onClick(View v) {
						
						finish();

						 

					}


					}); 
			        
			        
			        disprofileimage.setOnClickListener(new OnClickListener()
					{

					@Override
					public void onClick(View v) {
						
						  if(uid.equals(discoveruid))
						   {
							    Intent myIntent = new Intent();
								 myIntent = new Intent(getApplicationContext(), UserProfile.class);
								 myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
								 startActivity(myIntent);
							
						   }
						   else{
						      Intent myIntent = new Intent();
						      myIntent = new Intent(getApplicationContext(), PublicProfile.class);
						      myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						      myIntent.putExtra("userid",discoveruid);
						      startActivity(myIntent);
						   }

					}


					});  
			        
			        
			        
			        discusername.setOnClickListener(new OnClickListener()
					{

					@Override
					public void onClick(View v) {
						
						 if(uid.equals(discoveruid))
						   {
							    Intent myIntent = new Intent();
								 myIntent = new Intent(getApplicationContext(), UserProfile.class);
								 myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
								 startActivity(myIntent);
							
						   }
						   else{
						Intent myIntent = new Intent();
						 myIntent = new Intent(getApplicationContext(), PublicProfile.class);
						 myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						myIntent.putExtra("userid",discoveruid);
						 
						 startActivity(myIntent);
						   }

					}


					});  
			        

			        disclike.setOnClickListener(new View.OnClickListener()
			         {   
						//FeedList id= actorList.get(position); 
						
			    		
			                public void onClick(View arg0)
			                {
			                	
			                	
			                	clicked=true;
			    				
			    				if (clicked == false) {
			    					disclike.setBackgroundResource(R.drawable.faviconblackborder); 
			    	            } else if (clicked == true) {
			    	            	disclike.setBackgroundResource(R.drawable.favblack);
			    	            	String counttwo=discoverlikecount;
			    			    	int coot= Integer.parseInt(counttwo);
			    			    	disclikecount.setText((coot+1)+" "+"likes");
			    	            	
			    	            }
			    				
			    				
			                	
			                   new Thread(new Runnable() {
			                         @Override
			                         public void run() {
			                             try {
			                	
			                           	   	 
			                           	 
			             	   	String result = null;
			             	   	InputStream is = null;
			             	  
			             	  
			   		        
			             
			             	   	ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

			             	 
			    				
			             	   	nameValuePairs.add(new BasicNameValuePair("fid",discoverid));
			             		nameValuePairs.add(new BasicNameValuePair("discoveruid",discoveruid));
			             	  nameValuePairs.add(new BasicNameValuePair("userid",uid));
			             	 nameValuePairs.add(new BasicNameValuePair("username",uname));
			             	

			             	   	StrictMode.setThreadPolicy(policy); 


			         	//http post
			         	try{
			         	        HttpClient httpclient = new DefaultHttpClient();
			         	        HttpPost httppost = new HttpPost("http://128.199.176.163/elskayapp/elskayfeed/feed_insertlike.php");
			         	        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			         	        HttpResponse response = httpclient.execute(httppost); 
			         	        HttpEntity entity = response.getEntity();
			         	        is = entity.getContent();

			         	        Log.e("log_tag", "connection success ");
			         	       
			         	        //Toast.makeText(getApplicationContext(), "", Toast.LENGTH_SHORT).show();
			         	   }
			                            
			            
			         	
			         	catch(Exception e)
			         	{
			         	        Log.e("log_tag", "Error in http connection "+e.toString());
			         	        Toast.makeText(getApplicationContext(), "Connection fail", Toast.LENGTH_SHORT).show();

			         	}
			         	//convert response to string
			         	try{
			         	        BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
			         	        StringBuilder sb = new StringBuilder();
			         	        String line = null;
			         	        while ((line = reader.readLine()) != null) 
			         	        {
			         	                sb.append(line + "\n");
			         	             
			         	           
			       	             
			         	        }
			         	        is.close();

			         	        result=sb.toString();
			         	      Log.i("response##########",result);
			         	    Toast.makeText(getApplicationContext(),result,Toast.LENGTH_LONG).show();
			         	      
			            
			         	}
			         	catch(Exception e)
			         	{
			         	       Log.e("log_tag", "Error converting result "+e.toString());
			            	}


			         	try{
			         		
			         					JSONObject json_data = new JSONObject(result);

			         	                CharSequence w= (CharSequence) json_data.get("re");
			         	             
			         	                Toast.makeText(getApplicationContext(), w, Toast.LENGTH_SHORT).show();
			         	                

			         	      
			         	     }
			         	catch(JSONException e)
			         	   {
			         	        Log.e("log_tag", "Error parsing data "+e.toString());
			         	        Toast.makeText(getApplicationContext(), "JsonArray fail", Toast.LENGTH_SHORT).show();
			         	    }
			         	 Thread.sleep(3000); // Let's wait for some time
			                             } catch (Exception e) {
			                                  
			                             }
			                             //progress.dismiss();
			                           
			                            
			                         }
			                     }).start(); 
			                      
			                 } 
			                
			             }); 
			   	  
	        
			       
			          
	        
	        
}
	  
	  
	  class Getlikes extends AsyncTask<String, String, JSONObject> {
     	 
          /**
           * Before starting background thread Show Progress Dialog
           * */
          @Override
          protected void onPreExecute() {
              super.onPreExecute();
              pDialog = new ProgressDialog(DiscoverView.this);
              pDialog.setMessage("Loading...");
              pDialog.setIndeterminate(false);
              pDialog.setCancelable(true);
              pDialog.show();
          }
   
          /**
           * Getting product details in background thread
           * */
          
          @Override
          protected JSONObject doInBackground(String... args) {
      		
          	 JSONParser jParser = new JSONParser();
         		 List<NameValuePair> params = new ArrayList<NameValuePair>();
       	       params.add(new BasicNameValuePair("fid",discoverid));
       	      params.add(new BasicNameValuePair("uid",uid));
       		   JSONObject json = jParser.getJSONFromUrl(url_get_likes,params);
         		   
      		return json;
      		
     		    
      	}
         
          /**
           * After completing background task Dismiss the progress dialog
           * **/
          @Override
          protected void onPostExecute(final JSONObject json) {
     		
     	
     		 pDialog.dismiss();
     		
     		 try {
     			 
     			 
     			discoverlikecount = json.getString(KEY_LIKECOUNT);
     			discovercommentcount = json.getString(KEY_COMMENTCOUNT);
     			discoverlikeid = json.getString(KEY_LIKED);
     			
     			disccommentcount.setText(discovercommentcount+" "+"comments");
  	        disclikecount.setText(discoverlikecount+" "+"likes");
  	        
  	        
  	        if ((discoverlikeid.equals("null"))){
		        	disclike.setBackgroundResource(R.drawable.faviconblackborder);
			        
			  }
			     
			   
			    else {
			    	disclike.setBackgroundResource(R.drawable.favblack);
			    	disclike.setEnabled(false); 
			    	
			    }
             
     		}
  		 
           
           catch (JSONException e) {
  			
  			e.printStackTrace();
  		}
      }
      }    
	  
	  public boolean isNetworkAvailable() {
	       ConnectivityManager connectivityManager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
	       NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	       return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	   }
	  
	 /* @Override
	  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		    super.onActivityResult(requestCode, resultCode, data);
		    if (requestCode == 2) {
		         if(resultCode == Activity.RESULT_OK){
		        	 
		        	 String stredittext=data.getStringExtra("Obj");
		        	 Toast.makeText(getApplicationContext(), "Total number of Items are:" + stredittext , Toast.LENGTH_LONG).show();
		        	 Log.d("MYINT", "value: " + stredittext);
		          disccommentcount.setText(stredittext+" "+"comments");
		    }
		} 
	  }*/
	  
	  
}
