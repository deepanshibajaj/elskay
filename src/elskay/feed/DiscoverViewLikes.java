package elskay.feed;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.bhavin.horizontallistview.HorizontalListView;
import com.squareup.picasso.Picasso;

import elskay.app.DJSONParserPost;
import elskay.app.MainTabActivity;
import elskay.app.R;
import elskay.app.RoundedTransformation;
import elskay.feed.DiscoverView;
import elskay.feed.FeedAdapter;
import elskay.feed.FeedList;
import elskay.login.DatabaseHandler;
import elskay.login.PublicProfile;
import elskay.login.UserProfile;
import elskay.orders.ManageOrderItems;
import elskay.orders.ManageOrders;


public class DiscoverViewLikes extends Activity{
	
	
	

	EditText tx;
	ImageButton button;
	ImageView img;
	String commentcount;
	String username;
	JSONObject c;
	JSONObject feedob;
	

    
    HorizontalListView hlvSimple;
    
    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
    // JSON parser class
    DJSONParserPost jsonParser = new DJSONParserPost();  
    //List<HashMap<String, String>> oslist = new ArrayList<HashMap<String, String>>();
    ArrayList<DiscoverViewLikesList> oslist;
    private static String url_getfollowers = "http://128.199.176.163/elskayapp/elskayfeed/feed_getdiscoverfeedlikes.php";

   	 
     
  
      DiscoverViewLikesAdapter adapter;
   	JSONArray recomments = null;
   	JSONArray userfeed = null;

	private String qqid;
	int countt;
	 String followerid;
     int pidd;
	 String pid ;
	 String us;
	 String fid;
	 String dbuid;
	 String dbname;
	 String followername;
     String usernameee;
	 String imgcomment;
	 String adviceques;
	 ListView listView;
	 TextView ques;
	 String ccount;
	 String discoverusername;
	 String discoverprofileimage;
	 String discoverlocation;
	 String discoveruid;
	 String feedimage;
	 private ActionBar actionBar;
	 private Menu mToolbarMenu;
	 TextView orderid;
	 ImageView bg;
     TextView ebg;
     Boolean clicked;
     Button followwuser;
	 HashMap<String,String> user;
	 HashMap<String, String> map2 = new HashMap<String, String>();
	 private boolean selected=false;
	

	 int coot;
 
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); 
        setContentView(R.layout.feedlikeslist);
        
        bg = (ImageView)findViewById(R.id.nofollowersimg);
        ebg = (TextView)findViewById(R.id.nofollowerstext);
 
        DatabaseHandler db = new DatabaseHandler(getApplicationContext());
        HashMap<String,String> user = new HashMap<String, String>();
        user = db.getUserDetails();
        dbuid= user.get("uid");
        
        oslist = new ArrayList<DiscoverViewLikesList>();
        
       // oslist = new ArrayList<HashMap<String, String>>();
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        fid= bundle.getString("feedid"); 

         listView = (ListView) findViewById(R.id.list);
        
         

         ImageButton back = (ImageButton)findViewById(R.id.backbutn);
         
         back.setOnClickListener(new OnClickListener()
 		{

 		@Override
 		public void onClick(View v) {
 			
             finish();
 			 

 		}


 		}); 
         
       
         
         listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

	            @Override
	            public void onItemClick(AdapterView<?> parent, View view,
	                                    int position, long id) {
	            	 
	            	String uid = oslist.get(position).getuserid();
	 				
	            
	   			 
	   			 if(dbuid.equals(uid))
	  		   {
	  			    Intent myIntent = new Intent();
	  				 myIntent = new Intent(getApplicationContext(), UserProfile.class);
	  				 myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	  				startActivity(myIntent);
	  			
	  		   }
	  		   else
	  		   {
	  			      Intent myIntent = new Intent();
	  				 myIntent = new Intent(getApplicationContext(), PublicProfile.class);
	  				 myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	  				 
	  				 myIntent.putExtra("userid",uid);
	  				 
	  				startActivity(myIntent);
	  		   }
	 			

	            }
	        });
      
         
         
       
		
		
	   
         // Getting complete product details in background thread
	        if(isNetworkAvailable()){
	            // do network operation here     
	      	   new JSONParses().execute();
	         }else{
	            Toast.makeText(this, "No Available Network. Please try again later", Toast.LENGTH_LONG).show();
	            return;
	         }
        
	        adapter = new DiscoverViewLikesAdapter(DiscoverViewLikes.this, R.layout.feedlikes_list_item, oslist);
	 		
	         listView.setAdapter(adapter);
        
        
        
    }
 

    private class JSONParses extends AsyncTask<String, String, JSONObject> {
    	 private ProgressDialog pDialog;
    	@Override
        protected void onPreExecute() {
            super.onPreExecute();
                			
           pDialog = new ProgressDialog(DiscoverViewLikes.this);
            pDialog.setMessage("Loading....");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show(); 
          
            
            
            
    	}
    	
    	@Override
        protected JSONObject doInBackground(String... args) {
    		
    		DJSONParserPost jParser = new DJSONParserPost();
    		 List<NameValuePair> params = new ArrayList<NameValuePair>();
  	       params.add(new BasicNameValuePair("feedid", fid));
  		   JSONObject json = jParser.getJSONFromUrl(url_getfollowers,params);
    		return json;
    		
   		    
    	}
    	 
    	
    	@Override
         protected void onPostExecute(final JSONObject json) {
    		
    		
    		pDialog.dismiss();
    		 adapter.notifyDataSetChanged();
    		
    		 try {
    				
    			 recomments = json.getJSONArray("discoverfeedlikes");
    			
    				for(int i = 0; i < recomments.length(); i++){
    				c = recomments.getJSONObject(i);
    				
    				DiscoverViewLikesList actor = new DiscoverViewLikesList();
    				actor.setusername(c.getString("firstname"));
					actor.setuserprofileimage(c.getString("profileimage"));
					actor.setuserid(c.getString("unique_id"));
					//actor.setuserfollowid(c.getString("follow"));
    		       
    				oslist.add(actor);
    				
    				
    				
                   }
    				
    				
   
    		 }
    		 
    	                    
              catch (JSONException ignored) {
      			 
      			 
      		 }
      			if (c == null) {
      				
      			  bg.setVisibility(View.VISIBLE);
      			ebg.setVisibility(View.VISIBLE);
      			 
      			}
      			else {
      				
      				bg.setVisibility(View.GONE);
	      			ebg.setVisibility(View.GONE);
      				
      				
      			}

     		

     		 
     	 
    	 }

			 
                             
                             
    }
    
    

  
    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}
