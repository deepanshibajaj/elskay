package elskay.feed;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import elskay.app.R;
import elskay.app.RoundedTransformation;


public class DiscoverViewLikesAdapter extends ArrayAdapter<DiscoverViewLikesList> {
	
	ArrayList<DiscoverViewLikesList> actorList;
	LayoutInflater vi;
	int Resource;
	ViewHolder holder = null;
	Context context;
	ProgressBar progressBar = null;
	Boolean clicked;
	String uid;
	String uname;
	String dbuid;
	String dbuname;
	int cnt;
	StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
	//private ArrayList<Boolean> mToggles;
	private int mCount = 0;

	
	public DiscoverViewLikesAdapter(Context context, int resource, ArrayList<DiscoverViewLikesList> objects) {
		super(context, resource, objects);
		vi = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		Resource = resource;
		actorList = objects;
		
		
		
	}
 
	
	@Override
	public View getView(final int position, final View convertView, ViewGroup parent) {
		// convert view = design
		
		System.out.println("getview:"+position+" "+convertView);
		View v = convertView;
		

				

		
		if (v == null) {
			holder = new ViewHolder();
			v = vi.inflate(Resource, null);
			
			
			
			holder.userrname = (TextView)v.findViewById(R.id.username);
			holder.userprofileimg = (ImageView)v.findViewById(R.id.imageViewuser);
			
		
			
			
			v.setTag(holder);
		} else {
			holder = (ViewHolder) v.getTag();
		}
		
		holder.userrname.setText(actorList.get(position).getusername());
		
		 String imageurl=actorList.get(position).getuserprofileimage();
		 String Filename =imageurl.replace(" ", "%20");
		 Picasso.with(v.getContext()).load(Filename).resize(150, 150)
	        .transform(new RoundedTransformation(100, 20))
	        .into(holder.userprofileimg);
		
	       
	       
		
		return v;
		

	}

	static class ViewHolder {
	
		public TextView userrname;
		public ImageView userprofileimg;
		
		
	

	}
	

}