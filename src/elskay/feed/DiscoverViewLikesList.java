package elskay.feed;



public class DiscoverViewLikesList {

	
	private String username;
	private String userprofileimage;
	private String userid;
	//private String userfollowid;
	
	//private boolean selected;

	public DiscoverViewLikesList() {
		// TODO Auto-generated constructor stub
	}

	public DiscoverViewLikesList(String username,String userprofileimage, String userid,String userfollowid) {
		super();
		this.username = username;
		this.userprofileimage = userprofileimage;
		this.userid = userid;
		
	}


	public String getusername() {
		return username;
	}
	
	
	public void setusername(String username) {
		this.username = username;
	}
	
	public String getuserprofileimage() {
		return userprofileimage;
	}

	public void setuserprofileimage(String userprofileimage) {
		this.userprofileimage = userprofileimage;
	}
	
	
	public String getuserid() {
		return userid;
	}

	public void setuserid(String userid) {
		this.userid = userid;
	}
	
	

}
