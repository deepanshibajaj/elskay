package elskay.feed;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import elskay.app.R;
import elskay.app.RoundedTransformation;
import elskay.login.DatabaseHandler;
import elskay.login.PublicProfile;

public class ExploreAdapter extends ArrayAdapter<FeedList> {
	
	ArrayList<FeedList> actorList;
	LayoutInflater vi;
	int Resource;
	ViewHolder holder = null;
	Context context;
	ProgressBar progressBar = null;
	Boolean clicked;
	String uid;
	String uname;
	String Filename;
	int cnt;
	StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
	//private ArrayList<Boolean> mToggles;
	private int mCount = 0;

	
	public ExploreAdapter(Context context, int resource, ArrayList<FeedList> objects) {
		super(context, resource, objects);
		vi = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		Resource = resource;
		actorList = objects;
		
		
		
	}
 
	
	@Override
	public View getView(final int position, final View convertView, ViewGroup parent) {
		// convert view = design
		
		System.out.println("getview:"+position+" "+convertView);
		View v = convertView;
		

				

		
		if (v == null) {
			holder = new ViewHolder();
			v = vi.inflate(Resource, null);
			
			
			
			
			   
			holder.disimage = (ImageView) v.findViewById(R.id.exploreimage);
			holder.disusername = (TextView) v.findViewById(R.id.exploreusername);
			holder.disdescription = (TextView) v.findViewById(R.id.exploredesc);
			holder.dislocation = (TextView) v.findViewById(R.id.exploreplace);
			
			 DatabaseHandler db = new DatabaseHandler(getContext());

		        HashMap<String,String> user = new HashMap<String, String>();
		        user = db.getUserDetails();
		        uid= user.get("uid");
		        uname=user.get("fname");
		 
			v.setTag(holder);
		} else {
			holder = (ViewHolder) v.getTag();
		}
	
		
		Filename =actorList.get(position).getImage().replace(" ", "%20");
		 
		holder.disimage.setImageResource(R.drawable.ic_launcher);
	
		  Picasso.with(getContext())
          .load(Filename)
          .error(R.drawable.eloader)
          .placeholder( R.drawable.eloader )
          .into(holder.disimage);
		  
		  holder.disimage.setOnClickListener(new OnClickListener()
			{

			@Override
			public void onClick(View v) {
				
				String image = actorList.get(position).getImage();
			
				Intent myIntent = new Intent();
				 myIntent = new Intent(getContext(), Webview.class);
				 myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				 
				 myIntent.putExtra("imagefile",image);
				 
				 vi.getContext().startActivity(myIntent);
				 

			}


			});
		  
		  
		  /*holder.disimage.setOnClickListener(new OnClickListener() {

		   @Override
		   public void onClick(View arg0) {
		    LayoutInflater layoutInflater = 
		      (LayoutInflater)getContext()
		      .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		    View popupView = layoutInflater.inflate(R.layout.webv, null);
		    final PopupWindow popupWindow = new PopupWindow(
		      popupView, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		    
		    //Update TextView in PopupWindow dynamically
		    ImageView mImageView = (ImageView)popupView.findViewById(R.id.imageweb);
		    Picasso.with(getContext())
            .load(Filename)
            .into(mImageView);
		    
		    //popupWindow.showAsDropDown(holder.disimage, 10, 10);
		    
		    
		   }

		  });*/
		 
		    
		holder.disusername.setText(actorList.get(position).getName());
		Typeface custom_font = Typeface.createFromAsset(vi.getContext().getAssets(), "AvenirNextLTPro-Regular.otf");
		holder.disusername.setTypeface(custom_font);
	
		holder.disdescription.setText(actorList.get(position).getDescription());
		Typeface custom_font2 = Typeface.createFromAsset(vi.getContext().getAssets(), "AvenirNextLTPro-Regular.otf");
		holder.disdescription.setTypeface(custom_font2);
		
		holder.dislocation.setText(actorList.get(position).getlocation());
		Typeface custom_font3 = Typeface.createFromAsset(vi.getContext().getAssets(), "AvenirNextLTPro-Regular.otf");
		holder.dislocation.setTypeface(custom_font3);
		

		
		return v;
		

	}

	static class ViewHolder {
		
		public ImageView disimage;
		public TextView disusername;
		public TextView disdescription;
		public TextView dislocation;
		
		
	

	}
	
	

}