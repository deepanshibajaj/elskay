package elskay.feed;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.ParseException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import elskay.app.DJSONParserPost;
import elskay.app.GridViewWithHeaderAndFooter;
import elskay.app.R;
import elskay.login.DatabaseHandler;
import elskay.product.EProductList;


public class ExploreElskay extends Fragment {
	
	ArrayList<FeedList> actorsList;
	
	ExploreAdapter adapter;
	private SwipeRefreshLayout swipeRefreshLayout;
	ListView listview;
	long lastStoredTimeStamp = 0;
	private AnimationDrawable progressAnimation;
	private Button showDialog;
	String uid;
	JSONObject object;
	Button refresh;
	ImageView bg;
	TextView ebg;
	private static String url = "http://128.199.176.163/elskayapp/elskayfeed/get_explorefeed.php";
	
	 @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	    }
	
	/*@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.feed_list);*/
	 
	 public View onCreateView(LayoutInflater inflater, ViewGroup container,
             Bundle savedInstanceState) {
        View vi = inflater.inflate(R.layout.explore_list, container, false);
		
        //swipeRefreshLayout = (SwipeRefreshLayout)vi.findViewById(R.id.swipe_refresh_layout);
		
	
        
        
        /**
         * Showing Swipe Refresh animation on activity create
         * As animation won't start on onCreate, post runnable is used
         */
        bg = (ImageView)vi.findViewById(R.id.elskayexplore);
        ebg = (TextView)vi.findViewById(R.id.textViewbg);
        
        
        DatabaseHandler db = new DatabaseHandler(getActivity().getApplicationContext());

        HashMap<String,String> user = new HashMap<String, String>();
        user = db.getUserDetails();
        uid= user.get("uid");
		
		actorsList = new ArrayList<FeedList>();
		
		if(isNetworkAvailable()){
            // do network operation here     
			new JSONAsyncTask().execute();
         }else{
            Toast.makeText(getContext(), "No Available Network. Please try again later", Toast.LENGTH_LONG).show();
          
         }
		 //new JSONAsyncTask().execute();
		
		//ListView listview = (ListView)vi.findViewById(R.id.list);
		ListView listview = (ListView)vi.findViewById(R.id.list);
		adapter = new ExploreAdapter(getActivity(), R.layout.explore_list_item, actorsList);
		
		listview.setAdapter(adapter);
		
		
	    //swipeRefreshLayout.setOnRefreshListener(this);
		/*swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
            	swipeRefreshLayout.setRefreshing(true);

            	 new JSONAsyncTask().execute();
            }
        }
        ); 
		
		swipeRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
			@Override
		    public void onRefresh() {
		       
			
              new Handler().postDelayed(new Runnable() {
		            @Override
		            public void run() {
		            	swipeRefreshLayout.setRefreshing(false);
		            	 adapter.notifyDataSetChanged();
		              
		            }
		        }, 2000);
		    }
	
                
          
        });*/

		 
		/*listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
	        @Override
	        public void onItemClick(AdapterView<?> parent, View rowView, int positon,long id) {
	        	
	        	ImageButton button = (ImageButton) rowView.findViewById(R.id.button1);
	        	 
	           
	                button.setTag("red");
	                button.setImageResource(R.drawable.favblack);
	                adapter.notifyDataSetChanged();
	          
	           
	        }
	    });*/ 
		
		
		 refresh = (Button)vi.findViewById(R.id.button1);
	        
		 refresh.setOnClickListener(new OnClickListener()
			{

			@Override
			public void onClick(View v) {
				
				if(isNetworkAvailable()){
		            // do network operation here     
					new JSONAsyncTask().execute();
		         }else{
		            Toast.makeText(getContext(), "No Available Network. Please try again later", Toast.LENGTH_LONG).show();
		          
		         }

				 

			}


			}); 
	       
	        
	        return vi;
	}
	
	   
 
	
	class JSONAsyncTask extends AsyncTask<String, Void, JSONObject> {
		
		ProgressDialog dialog;
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			//swipeRefreshLayout.setRefreshing(true);
			dialog = new ProgressDialog(getActivity());
			dialog.setMessage("Please give me a moment to open the box of treasure..");
			//dialog.setTitle("Connecting server");
			dialog.show();
			dialog.setCancelable(false);
			
			/*final Dialog dialog = new Dialog(getActivity());
			//Set Background of Dialog - Custom 
			dialog.getWindow().setBackgroundDrawableResource(R.drawable.dialog_background);
			//Remove the Title
			//dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
			dialog.setTitle("Please give me a moment to open the box of treasure..");
			//Set the View of the Dialog - Custom
			dialog.setContentView(R.layout.dialog);
			//Set the title of the Dialog
			//dialog.setTitle("Title...");
			ImageView progressSpinner = (ImageView) dialog.findViewById(R.id.progressSpinner);

			//Set the background of the image - In this case an animation (/res/anim folder)
			progressSpinner.setBackgroundResource(R.anim.loading_anim);
			//Get the image background and attach the AnimationDrawable to it.
			progressAnimation = (AnimationDrawable) progressSpinner.getBackground();
			//Start the animation after the dialog is displayed.
			
			progressAnimation.start();
			
			dialog.show();*/
		}
		
		
		protected JSONObject doInBackground(String... args)  {
			
				DJSONParserPost jParser = new DJSONParserPost();
		    	   List<NameValuePair> params = new ArrayList<NameValuePair>();
		    	   Log.i("params@@@@@@@",uid);
		  	       params.add(new BasicNameValuePair("followerid", uid));
		  		   JSONObject json = jParser.getJSONFromUrl(url,params);
		    		return json;
		     }
				
		
		
	   protected void onPostExecute(final JSONObject json) {
			dialog.dismiss();
			
			 adapter.notifyDataSetChanged();
	    		 try {
				
					
						JSONArray jarray = json.getJSONArray("explorepage");
					
					for (int i = 0; i < jarray.length(); i++) {
						object = jarray.getJSONObject(i);
					
                         FeedList actor = new FeedList();
						
						actor.setfid(object.getString("fid"));
						actor.setImage(object.getString("feedimage"));
						actor.setDescription(object.getString("feedhashtag"));
						actor.setName(object.getString("feeduname"));
						actor.setlocation(object.getString("feedlocation"));
						actor.setTimeStamp(object.getString("created_at"));
						actor.setProfile(object.getString("profileimage"));
						actor.setuserid(object.getString("unique_id"));
						
						
						actorsList.add(actor);
						
						
						//swipeRefreshLayout.setRefreshing(false);
					}
	    		 }
	    		 
                    catch (JSONException ignored) {
	      			 
	      			 
	      		 }
	      			if (object == null) {
	      			    refresh.setVisibility(View.VISIBLE);
	      			  bg.setVisibility(View.VISIBLE);
	      			ebg.setVisibility(View.VISIBLE);
	      			 
	      			}
	      			else {
	      				refresh.setVisibility(View.GONE);
	      				bg.setVisibility(View.GONE);
		      			ebg.setVisibility(View.GONE);
	      				
	      				
	      			}

	       		
	
	    		 }	
}
	
	
	/*@Override
    public void onRefresh() {
	   adapter.notifyDataSetInvalidated();
    	new JSONAsyncTask().execute();
       
    } */
	
	 public boolean isNetworkAvailable() {
	       ConnectivityManager connectivityManager = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
	       NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	       return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	   }
	
	}
	
	

	
	
	

