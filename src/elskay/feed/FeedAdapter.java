package elskay.feed;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import elskay.app.R;
import elskay.app.RoundedTransformation;
import elskay.login.DatabaseHandler;
import elskay.login.PublicProfile;

public class FeedAdapter extends ArrayAdapter<FeedList> {
	
	ArrayList<FeedList> actorList;
	LayoutInflater vi;
	int Resource;
	ViewHolder holder = null;
	Context context;
	ProgressBar progressBar = null;
	Boolean clicked;
	String uid;
	String uname;
	int cnt;
	StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
	//private ArrayList<Boolean> mToggles;
	private int mCount = 0;

	
	public FeedAdapter(Context context, int resource, ArrayList<FeedList> objects) {
		super(context, resource, objects);
		vi = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		Resource = resource;
		actorList = objects;
		
		
		
	}
 
	
	@Override
	public View getView(final int position, final View convertView, ViewGroup parent) {
		// convert view = design
		
		System.out.println("getview:"+position+" "+convertView);
		View v = convertView;
		

				

		
		if (v == null) {
			holder = new ViewHolder();
			v = vi.inflate(Resource, null);
			
			
			
			
			   
			holder.imageview = (ImageButton) v.findViewById(R.id.getfeedimage);
			holder.tvName = (TextView) v.findViewById(R.id.hashtaguser);
			holder.fid = (ImageButton) v.findViewById(R.id.Button01);
			holder.CountComment = (TextView) v.findViewById(R.id.countcomment);
			holder.Countlike = (TextView) v.findViewById(R.id.likecount);
			holder.tvDescription = (TextView) v.findViewById(R.id.hashtag);
			holder.likeid = (ImageButton) v.findViewById(R.id.button1);
			
			holder.likeid.setTag(position);
			
			 DatabaseHandler db = new DatabaseHandler(getContext());

		        HashMap<String,String> user = new HashMap<String, String>();
		        user = db.getUserDetails();
		        uid= user.get("uid");
		        uname=user.get("fname");
			
			
			/*Typeface custom_font3 = Typeface.createFromAsset(vi.getContext().getAssets(), "Roboto-Bold.ttf");
			holder.tvDescription.setTypeface(custom_font3);*/
			holder.profileimage = (ImageButton) v.findViewById(R.id.profiletwo); 
			v.setTag(holder);
		} else {
			holder = (ViewHolder) v.getTag();
		}
		
		//holder.cbCheck.setChecked(mToggles.get( position ));
		
		String Filename =actorList.get(position).getImage().replace(" ", "%20");
		 
		holder.imageview.setImageResource(R.drawable.ic_launcher);
		//new DownloadImageTask(holder.imageview).execute(actorList.get(position).getImage());
		  Picasso.with(getContext())
          .load(Filename)
          .error(R.drawable.eloader)
          .placeholder( R.drawable.eloader )
          .into(holder.imageview);
		  
		  holder.imageview.setOnClickListener(new OnClickListener()
			{

			@Override
			public void onClick(View v) {
				
				String image = actorList.get(position).getImage();
			
				Intent myIntent = new Intent();
				 myIntent = new Intent(getContext(), Webview.class);
				 myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				 
				 myIntent.putExtra("imagefile",image);
				 
				 vi.getContext().startActivity(myIntent);
				 

			}


			});
		  
		  
		  holder.profileimage.setOnClickListener(new OnClickListener()
			{

			@Override
			public void onClick(View v) {
				
				String userid = actorList.get(position).getuserid();
			
				Intent myIntent = new Intent();
				 myIntent = new Intent(getContext(), PublicProfile.class);
				 myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				 
				 myIntent.putExtra("userid",userid);
				 
				 vi.getContext().startActivity(myIntent);
				 

			}


			});
		  
		  
		    
		holder.tvName.setText(actorList.get(position).getName());
		holder.Countlike.setText(actorList.get(position).getlikecount()+" "+"likes");
		//holder.Countlike.setTag(actorList.get(position).getlikecount());
		
        
		holder.CountComment.setText(actorList.get(position).getcount()+" "+"comments");
		
		holder.fid.setTag(actorList.get(position).getfid());
		//holder.likeid.setTag(actorList.get(position).getfid());
		/*String count=actorList.get(position).getlikecount();
    	cnt= Integer.parseInt(count);*/
		
		
		
		
		holder.likeid.setOnClickListener(new View.OnClickListener()
         {   
			//FeedList id= actorList.get(position); 
			
    		
                public void onClick(View arg0)
                {
                	
                	
                	clicked=true;
    				
    				if (clicked == false) {
    	                actorList.get(position).setSelected(false);
    	                notifyDataSetChanged();
    	            } else if (clicked == true) {
    	            	actorList.get(position).setSelected(true);
    	            	//cnt++;
    	                notifyDataSetChanged();
    	            }
    				
    				
                	
                   new Thread(new Runnable() {
                         @Override
                         public void run() {
                             try {
                	
                           	   	 
                           	 
             	   	String result = null;
             	   	InputStream is = null;
             	  
             	  
   		        
             	  String v5 = actorList.get(position).getfid();
             	
             
             	   	ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

             	 
    				
             	   	nameValuePairs.add(new BasicNameValuePair("fid",v5));
             	  nameValuePairs.add(new BasicNameValuePair("userid",uid));
             	 nameValuePairs.add(new BasicNameValuePair("username",uname));
             	
             	     Log.i("favid send@@",v5);
             	   
           

             	   	StrictMode.setThreadPolicy(policy); 


         	//http post
         	try{
         	        HttpClient httpclient = new DefaultHttpClient();
         	        HttpPost httppost = new HttpPost("http://128.199.176.163/elskayapp/elskayfeed/feed_insertlike.php");
         	        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
         	        HttpResponse response = httpclient.execute(httppost); 
         	        HttpEntity entity = response.getEntity();
         	        is = entity.getContent();

         	        Log.e("log_tag", "connection success ");
         	       
         	        //Toast.makeText(getApplicationContext(), "", Toast.LENGTH_SHORT).show();
         	   }
                            
            
         	
         	catch(Exception e)
         	{
         	        Log.e("log_tag", "Error in http connection "+e.toString());
         	        Toast.makeText(getContext(), "Connection fail", Toast.LENGTH_SHORT).show();

         	}
         	//convert response to string
         	try{
         	        BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
         	        StringBuilder sb = new StringBuilder();
         	        String line = null;
         	        while ((line = reader.readLine()) != null) 
         	        {
         	                sb.append(line + "\n");
         	             
         	           
       	             
         	        }
         	        is.close();

         	        result=sb.toString();
         	      Log.i("response##########",result);
         	    Toast.makeText(getContext(),result,Toast.LENGTH_LONG).show();
         	      
            
         	}
         	catch(Exception e)
         	{
         	       Log.e("log_tag", "Error converting result "+e.toString());
            	}


         	try{
         		
         					JSONObject json_data = new JSONObject(result);

         	                CharSequence w= (CharSequence) json_data.get("re");
         	             
         	                Toast.makeText(getContext(), w, Toast.LENGTH_SHORT).show();
         	                

         	      
         	     }
         	catch(JSONException e)
         	   {
         	        Log.e("log_tag", "Error parsing data "+e.toString());
         	        Toast.makeText(getContext(), "JsonArray fail", Toast.LENGTH_SHORT).show();
         	    }
         	 Thread.sleep(3000); // Let's wait for some time
                             } catch (Exception e) {
                                  
                             }
                             //progress.dismiss();
                           
                            
                         }
                     }).start(); 
                      
                 } 
                
             }); 
   	  
				
		 holder.fid.setOnClickListener(new OnClickListener()
			{

			@Override
			public void onClick(View v) {
				
				String fid = actorList.get(position).getfid();
				

				String feedimage = actorList.get(position).getImage();
				String feedhashtag = actorList.get(position).getDescription();
			
			
				Intent myIntent = new Intent();
				 myIntent = new Intent(getContext(), FeedComment.class);
				 myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				 
				 myIntent.putExtra("feedimage",feedimage);
				 myIntent.putExtra("feedhashtag",feedhashtag);
				 myIntent.putExtra("feedid",fid);
				 Log.i("favid comment@@",fid);
				 
				 //myIntent.putExtra("fid","1");
				 
				 vi.getContext().startActivity(myIntent);
				 

			}


			});
		holder.tvDescription.setText(actorList.get(position).getDescription());
		holder.profileimage.setImageResource(R.drawable.ic_launcher);
		 Picasso.with(getContext())
         .load(actorList.get(position).getProfile())
         .resize(100, 100)
         //.transform(new RoundedTransformation(100, 80))
         .transform(new CircleTransform())
         .into(holder.profileimage); 
		 
		
		 /* holder.likeid.setOnClickListener(new View.OnClickListener()
			{

			@Override
			public void onClick(View arg0) {
				
				clicked=true;
				
				if (clicked == false) {
	                actorList.get(position).setSelected(false);
	                notifyDataSetChanged();
	            } else if (clicked == true) {
	            	actorList.get(position).setSelected(true);
	                notifyDataSetChanged();
	            }
				 //ViewHolder holder1 = (ViewHolder)view.getTag();
				   //arg0.setBackgroundResource(R.drawable.favblack);
				  // holder.cbCheck.setChecked(actorList.get(position).isSelected());
				

			}


			});*/
		  
		  
		  if ((actorList.get(position).isSelected() == false)  && (actorList.get(position).getlikeid().equals("null"))){
		        holder.likeid.setBackgroundResource(R.drawable.faviconblackborder);
		        
		  }
		     
		   
		    else {
		    	holder.likeid.setBackgroundResource(R.drawable.favblack);
		    	/*String count=holder.Countlike.getTag().toString();
		    	int cnt= Integer.parseInt(count);
		        cnt++;
		        holder.Countlike.setText(cnt+" "+"likes");*/
		    	/*String count=holder.Countlike.getTag().toString();
		    	cnt= Integer.parseInt(count);
		        cnt++;
		        holder.Countlike.setText(cnt+" "+"likes");*/
		    	/*String counttwo=actorList.get(position).getlikecount();
		    	int coot= Integer.parseInt(counttwo);
		    	holder.Countlike.setText((coot+1)+" "+"likes");*/
		    	
		    }
		  
		  if (actorList.get(position).isSelected() == true) 
		  {
			  String counttwo=actorList.get(position).getlikecount();
		    	int coot= Integer.parseInt(counttwo);
		    	holder.Countlike.setText((coot+1)+" "+"likes"); 
		  }
		  else
		  {
			  
		  }
		return v;
		

	}

	static class ViewHolder {
		public ImageButton profileimage;
		public ImageButton imageview;
		public TextView tvName;
		public TextView tvDescription;
		public ImageButton fid;
		public ImageButton likeid;
		public ImageButton likedid;
		public TextView CountComment;
		public TextView Countlike;
		
	

	}
	
	/*public boolean onItemLongClick(AdapterView<?> arg0, View view,
            int position, long arg3) {
        ImageButton button = (ImageButton) view.findViewById(R.id.button1);
 
        
            button.setImageResource(R.drawable.faviconblack);
        
 
        return true;
    } */
	

	
	/*public void setToggleList( ArrayList<Boolean> list ){
        this.mToggles = list;
        notifyDataSetChanged();
    }*/

}