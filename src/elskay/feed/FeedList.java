package elskay.feed;

import elskay.app.R;

public class FeedList {

	
	private String fid;
	private String ccount;
	private String name;
	private String description;
	private String image;
	private String profile;
	private String cover;
	private String userid;
	private String likeid;
	private String TimeStamp;
	private String likecount;
	private String location;
	private boolean selected;

	public FeedList() {
		// TODO Auto-generated constructor stub
	}

	public FeedList(String name,String fid, String description ,String image,String profile,String TimeStamp,String ccount,String userid,String likeid
			,String likecount,String location,String cover) {
		super();
		this.name = name;
		this.fid = fid;
		this.description = description;
		this.image = image;
		this.profile = profile;
		this.TimeStamp = TimeStamp;
		this.ccount = ccount;
		this.userid = userid;
		this.likeid = likeid;
		this.likecount = likecount;
		this.location = location;
		this.cover = cover;
		 selected = false;
	}


	public String getName() {
		return name;
	}
	
	public int getBackgroundResource() {
		return R.drawable.faviconblack;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getcount() {
		return ccount;
	}

	public void setcount(String ccount) {
		this.ccount = ccount;
	}
	
	public String getlocation() {
		return location;
	}

	public void setlocation(String location) {
		this.location = location;
	}
	
	
	public String getTimeStamp() {
		return TimeStamp;
	}

	public void setTimeStamp(String TimeStamp) {
		this.TimeStamp = TimeStamp;
	}
	
	public String getfid() {
		return fid;
	}

	public void setfid(String fid) {
		this.fid = fid;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}
	
	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}
	
	public String getcover() {
		return cover;
	}

	public void setcover(String cover) {
		this.cover= cover;
	}
	public String getuserid() {
		return userid;
	}

	public void setuserid(String userid) {
		this.userid = userid;
	}
	public String getlikeid() {
		return likeid;
	}

	public void setlikecount(String likecount) {
		this.likecount = likecount;
	}
	public String getlikecount() {
		return likecount;
	}

	public void setlikeid(String likeid) {
		this.likeid = likeid;
	}
	 public boolean isSelected() {
		    return selected;
		  }

		  public void setSelected(boolean selected) {
		    this.selected = selected;
		  }

}
