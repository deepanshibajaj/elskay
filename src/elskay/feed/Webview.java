package elskay.feed;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import elskay.app.R;


public class Webview extends Activity {
	
	private WebView myWebView;
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    requestWindowFeature(Window.FEATURE_NO_TITLE);
	    
	    setContentView(R.layout.webv);

	String url = getIntent().getStringExtra("imagefile");
	String urlfinal =url.replace(" ", "%20");
	
	
	
	ImageView mImageView = (ImageView)findViewById(R.id.imageweb);
	mImageView.setOnClickListener(new View.OnClickListener() {

		@Override
        public void onClick(View arg0) {

            finish();
        }
    });


	
	    
	 
	    	Picasso.with(getApplicationContext())
            .load(urlfinal)
            .into(mImageView);
      	  
      	  
       
	}
	
	
}
