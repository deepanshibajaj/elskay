package elskay.instafeed;

import java.io.InputStream;
import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import elskay.app.R;
import elskay.app.RoundedTransformation;

public class InstaAdapter extends ArrayAdapter<InstaList> {
	ArrayList<InstaList> actorList;
	LayoutInflater vi;
	int Resource;
	ViewHolder holder;

	public InstaAdapter(Context context, int resource, ArrayList<InstaList> objects) {
		super(context, resource, objects);
		vi = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		Resource = resource;
		actorList = objects;
		
		
		
	}
 
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// convert view = design
		View v = convertView;
		

				

		
		if (v == null) {
			holder = new ViewHolder();
			v = vi.inflate(Resource, null);
			holder.imageview = (ImageView) v.findViewById(R.id.imageVi);
			holder.tvName = (TextView) v.findViewById(R.id.hashtaguser);
			holder.tvDescription = (TextView) v.findViewById(R.id.hashtag);
			Typeface custom_font3 = Typeface.createFromAsset(vi.getContext().getAssets(), "Roboto-Bold.ttf");
			holder.tvDescription.setTypeface(custom_font3);
			holder.profileimage = (ImageView) v.findViewById(R.id.profiletwo);
			v.setTag(holder);
		} else {
			holder = (ViewHolder) v.getTag();
		}
		holder.imageview.setImageResource(R.drawable.ic_launcher);
		//new DownloadImageTask(holder.imageview).execute(actorList.get(position).getImage());
		  Picasso.with(getContext())
          .load(actorList.get(position).getImage())
          .into(holder.imageview);
		holder.tvName.setText(actorList.get(position).getName());
		holder.tvDescription.setText(actorList.get(position).getDescription());
		holder.profileimage.setImageResource(R.drawable.ic_launcher);
		 Picasso.with(getContext())
         .load(actorList.get(position).getProfile())
          .transform(new RoundedTransformation(200, 5)) 
         .into(holder.profileimage);
		return v;

	}

	static class ViewHolder {
		public ImageView profileimage;
		public ImageView imageview;
		public TextView tvName;
		public TextView tvDescription;

	}

	private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
		ImageView bmImage;

		public DownloadImageTask(ImageView bmImage) {
			this.bmImage = bmImage;
		}

		protected Bitmap doInBackground(String... urls) {
			String urldisplay = urls[0];
			Bitmap mIcon11 = null;
			try {
				InputStream in = new java.net.URL(urldisplay).openStream();
				mIcon11 = BitmapFactory.decodeStream(in);
			} catch (Exception e) {
				Log.e("Error", e.getMessage());
				e.printStackTrace();
			}
			return mIcon11;
		}

		protected void onPostExecute(Bitmap result) {
			bmImage.setImageBitmap(result);
		}

	}
	

}