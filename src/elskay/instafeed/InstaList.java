package elskay.instafeed;

public class InstaList {

	private String name;
	private String description;
	private String image;
	private String profile;

	public InstaList() {
		// TODO Auto-generated constructor stub
	}

	public InstaList(String name, String description ,String image,String profile) {
		super();
		this.name = name;
		this.description = description;
		this.image = image;
		this.profile = profile;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}
	
	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

}
