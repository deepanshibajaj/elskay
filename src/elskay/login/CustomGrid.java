package elskay.login;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import elskay.app.R;
import elskay.app.RoundedTransformation;
import elskay.feed.DiscoverView;
import elskay.feed.FeedList;
import elskay.login.DatabaseHandler;
import elskay.login.PublicProfile;

public class CustomGrid extends ArrayAdapter<FeedList> {
	
	ArrayList<FeedList> actorList;
	LayoutInflater vi;
	int Resource;
	ViewHolder holder = null;
	Context context;
	ProgressBar progressBar = null;
	Boolean clicked;
	String uid;
	String uname;
	int cnt;
	StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
	//private ArrayList<Boolean> mToggles;
	private int mCount = 0;

	
	public CustomGrid(Context context, int resource, ArrayList<FeedList> objects) {
		super(context, resource, objects);
		vi = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		Resource = resource;
		actorList = objects;
		
		
		
	}
 
	
	@Override
	public View getView(final int position, final View convertView, ViewGroup parent) {
		// convert view = design
		
		System.out.println("getview:"+position+" "+convertView);
		View v = convertView;
		

				

		
		if (v == null) {
			holder = new ViewHolder();
			v = vi.inflate(Resource, null);
			
			
			
			
			   
			holder.disimage = (ImageView) v.findViewById(R.id.userfeedimage);
			
			
			 
		 
			v.setTag(holder);
		} else {
			holder = (ViewHolder) v.getTag();
		}
	
		
		String Filename =actorList.get(position).getImage().replace(" ", "%20");
		 
		holder.disimage.setImageResource(R.drawable.ic_launcher);
	
		  Picasso.with(getContext())
          .load(Filename)
          .error(R.drawable.eloader)
          .placeholder( R.drawable.eloader )
          .into(holder.disimage);
		  
	
		  
		  holder.disimage.setOnClickListener(new OnClickListener()
			{

			@Override
			public void onClick(View v) {
				
				String fid = actorList.get(position).getfid();
				String feeduname = actorList.get(position).getName();
				String feedimage = actorList.get(position).getImage();
				String feedhashtag = actorList.get(position).getDescription();
				String feedprofileimage = actorList.get(position).getProfile();
				String feedlocation = actorList.get(position).getlocation();
				String feeduid = actorList.get(position).getuserid();
			
			
				Intent myIntent = new Intent();
				 myIntent = new Intent(getContext(), DiscoverView.class);
				 myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				 
				 myIntent.putExtra("feedimage",feedimage);
				 myIntent.putExtra("feedhashtag",feedhashtag);
				 myIntent.putExtra("feedid",fid);
				 myIntent.putExtra("feeduname",feeduname);
				 myIntent.putExtra("feedprofileimage",feedprofileimage);
				 myIntent.putExtra("feedlocation",feedlocation);
				 myIntent.putExtra("feeduid",feeduid);
			
				 vi.getContext().startActivity(myIntent);
				
				 

			}


			});
		 
		
		
		
		return v;
		

	}

	static class ViewHolder {
		
		public ImageView disimage;
		public TextView disusername;
		public TextView disdescription;
		public TextView dislocation;
		
		
	

	}
	
	

}