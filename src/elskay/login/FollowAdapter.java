package elskay.login;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import elskay.app.R;
import elskay.app.RoundedTransformation;
import elskay.login.DatabaseHandler;
import elskay.login.PublicProfile;

public class FollowAdapter extends ArrayAdapter<Followlist> {
	
	ArrayList<Followlist> actorList;
	LayoutInflater vi;
	int Resource;
	ViewHolder holder = null;
	Context context;
	ProgressBar progressBar = null;
	Boolean clicked;
	String uid;
	String uname;
	String dbuid;
	String dbuname;
	int cnt;
	StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
	//private ArrayList<Boolean> mToggles;
	private int mCount = 0;

	
	public FollowAdapter(Context context, int resource, ArrayList<Followlist> objects) {
		super(context, resource, objects);
		vi = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		Resource = resource;
		actorList = objects;
		
		
		
	}
 
	
	@Override
	public View getView(final int position, final View convertView, ViewGroup parent) {
		// convert view = design
		
		System.out.println("getview:"+position+" "+convertView);
		View v = convertView;
		

				

		
		if (v == null) {
			holder = new ViewHolder();
			v = vi.inflate(Resource, null);
			
			
			
			holder.followername = (TextView)v.findViewById(R.id.followername);
			holder.followprofileimg = (ImageView)v.findViewById(R.id.imageViewfollower);
			holder.followuser = (Button)v.findViewById(R.id.userfollow);
			
			DatabaseHandler db = new DatabaseHandler(getContext());

		        HashMap<String,String> user = new HashMap<String, String>();
		        user = db.getUserDetails();
		        dbuid= user.get("uid");
		        dbuname=user.get("fname");
			
			
			v.setTag(holder);
		} else {
			holder = (ViewHolder) v.getTag();
		}
		
		holder.followername.setText(actorList.get(position).getfollowername());
		
		 String imageurl=actorList.get(position).getfollowerprofileimage();
		 String Filename =imageurl.replace(" ", "%20");
		 Picasso.with(v.getContext()).load(Filename).resize(150, 150)
	        .transform(new RoundedTransformation(100, 20))
	        .into(holder.followprofileimg);
		 
		 String followingid =actorList.get(position).getuserfollowid();
		 String followerid =actorList.get(position).getfollowerid();
	       
	        if(dbuid.equals(followerid))
			   {
	        	holder.followuser.setVisibility(View.GONE);
			   }
	        
	        
	        holder.followuser.setOnClickListener(new View.OnClickListener()
			{


	            public void onClick(View arg0)
	            {
	            	
	            	
	            	clicked=true;
	            	if (clicked == false) {
	            		  actorList.get(position).setSelected(false);
		                notifyDataSetChanged();
		            } else if (clicked == true) {
		            	actorList.get(position).setSelected(true);
		            	//cnt++;
		                notifyDataSetChanged();
		            }
				
	            	
	               new Thread(new Runnable() {
	                     @Override
	                     public void run() {
	                         try {
	            	
	                       	   	 
	                       	 
	         	   	String result = null;
	         	   	InputStream is = null;
	         	  
	         	   String followerrname=actorList.get(position).getfollowername();
	         	   String followerrid = actorList.get(position).getfollowerid();
	         
	         	   	ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

	         	 
					
	         	   
	         	  nameValuePairs.add(new BasicNameValuePair("uid",followerrid));
	         	 nameValuePairs.add(new BasicNameValuePair("uname",followerrname));
	         	 nameValuePairs.add(new BasicNameValuePair("followerid",dbuid));
	         	 nameValuePairs.add(new BasicNameValuePair("followername",dbuname));
	         	

	         	   	StrictMode.setThreadPolicy(policy); 


	     	//http post
	     	try{
	     	        HttpClient httpclient = new DefaultHttpClient();
	     	        HttpPost httppost = new HttpPost("http://128.199.176.163/elskayapp/elskayfeed/insert_followers.php");
	     	        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
	     	        HttpResponse response = httpclient.execute(httppost); 
	     	        HttpEntity entity = response.getEntity();
	     	        is = entity.getContent();

	     	        Log.e("log_tag", "connection success ");
	     	       
	     	        //Toast.makeText(getApplicationContext(), "", Toast.LENGTH_SHORT).show();
	     	   }
	                        
	        
	     	
	     	catch(Exception e)
	     	{
	     	        Log.e("log_tag", "Error in http connection "+e.toString());
	     	        Toast.makeText(getContext(), "Connection fail", Toast.LENGTH_SHORT).show();

	     	}
	     	//convert response to string
	     	try{
	     	        BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
	     	        StringBuilder sb = new StringBuilder();
	     	        String line = null;
	     	        while ((line = reader.readLine()) != null) 
	     	        {
	     	                sb.append(line + "\n");
	     	             
	     	           
	   	             
	     	        }
	     	        is.close();

	     	        result=sb.toString();
	     	      Log.i("response##########",result);
	     	    Toast.makeText(getContext(),result,Toast.LENGTH_LONG).show();
	     	      
	        
	     	}
	     	catch(Exception e)
	     	{
	     	       Log.e("log_tag", "Error converting result "+e.toString());
	        	}


	     	try{
	     		
	     					JSONObject json_data = new JSONObject(result);

	     	                CharSequence w= (CharSequence) json_data.get("re");
	     	             
	     	                Toast.makeText(getContext(), w, Toast.LENGTH_SHORT).show();
	     	                

	     	      
	     	     }
	     	catch(JSONException e)
	     	   {
	     	        Log.e("log_tag", "Error parsing data "+e.toString());
	     	        Toast.makeText(getContext(), "JsonArray fail", Toast.LENGTH_SHORT).show();
	     	    }
	     	 Thread.sleep(3000); // Let's wait for some time
	                         } catch (Exception e) {
	                              
	                         }
	                         //progress.dismiss();
	                       
	                        
	                     }
	                 }).start(); 
	                  
	             } 
	            
	         });    
	     
	       
	        if ((actorList.get(position).isSelected() == false) && (followingid.equals("null"))){
	        	 holder.followuser.setText("+ Follow");
		        
		  }
		     
		   
		    else {
		    	 holder.followuser.setText("Following");
		    	 holder.followuser.setTextColor(Color.parseColor("#ffffff"));
		    	 holder.followuser.setBackgroundColor(Color.parseColor("#5FD5C4"));
		    	 holder.followuser.setEnabled(false); 
		    	
		    }
		
		return v;
		

	}

	static class ViewHolder {
		public ImageButton profileimage;
		public Button followuser;
		public TextView followername;
		public ImageView followprofileimg;
		
		
	

	}
	

}