package elskay.login;



public class Followlist {

	
	private String followername;
	private String followerprofileimage;
	private String followerid;
	private String userfollowid;
	
	private boolean selected;

	public Followlist() {
		// TODO Auto-generated constructor stub
	}

	public Followlist(String followername,String followerprofileimage, String followerid,String userfollowid) {
		super();
		this.followername = followername;
		this.followerprofileimage = followerprofileimage;
		this.followerid = followerid;
		this.userfollowid = userfollowid;
		 selected = false;
	}


	public String getfollowername() {
		return followername;
	}
	
	
	public void setfollowername(String followername) {
		this.followername = followername;
	}
	
	public String getfollowerprofileimage() {
		return followerprofileimage;
	}

	public void setfollowerprofileimage(String followerprofileimage) {
		this.followerprofileimage = followerprofileimage;
	}
	
	
	public String getfollowerid() {
		return followerid;
	}

	public void setfollowerid(String followerid) {
		this.followerid = followerid;
	}
	
	public String getuserfollowid() {
		return userfollowid;
	}

	public void setuserfollowid(String userfollowid) {
		this.userfollowid = userfollowid;
	}
	
	


	 public boolean isSelected() {
		    return selected;
		  }

		  public void setSelected(boolean selected) {
		    this.selected = selected;
		  }

}
