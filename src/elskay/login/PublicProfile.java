package elskay.login;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.animation.Animator;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.squareup.picasso.Picasso;

import elskay.app.GridViewWithHeaderAndFooter;
import elskay.app.R;
import elskay.app.RoundedTransformation;
import elskay.feed.DiscoverAdapter;
import elskay.feed.FeedAdapter;
import elskay.feed.FeedList;
import elskay.feed.Webview;




 
public class PublicProfile extends Activity {
 
	GridView grid;
	
    ArrayList<FeedList> actorsList;
	
    CustomGrid adapter;
 
    // Progress Dialog
    private ProgressDialog pDialog;
    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
    // JSON parser class
    JSONParser jsonParser = new JSONParser();  
  
    private static String url_get_userdetails = "http://128.199.176.163/elskayapp/elskayapp_login/login_getuserdetail.php";

   	private static final String TAG_OS = "profile";
    
      private static final String TAG_USERNAME = "firstname";
      private static final String TAG_USEREMAIL = "email";
      private static final String TAG_USERIMAGE = "profileimage";
     
      private static final String TAG_FEEDIMAGE = "feedimage";
      private static final String TAG_FEEDDESC = "feedhashtag";
      private static final String TAG_FEEDID = "fid";
      private static final String TAG_FEEDLOCATION = "feedlocation";
      private static final String TAG_FEEDUNAME = "feeduname";
      private static final String TAG_FEEDUID = "unique_id";
    //  private static final String TAG_FOLLOW = "follow";
      
      private static String KEY_FOLLOWCOUNT = "followers";
      private static String KEY_COUNTPOSTS = "postscount";
      private static String KEY_FOLLOWINGCOUNT = "following";
      
      private static final String TAG_OS_TWO = "profiledetail";
     
      private static final String TAG_MY_USERID = "unique_id";
      private static final String TAG_MY_USERNAME = "firstname";
      private static final String TAG_MY_USEREMAIL = "email";
      private static final String TAG_MY_USERIMAGE = "profileimage";
      private static final String TAG_USERCOVERIMAGE = "coverimage";
      
    private static final String TAG_OS_THREE = "profilefollow";
      
    private static final String TAG_FOLLOW = "follow";
    
      
   	JSONArray recomments = null;
   	JSONArray myprofiledetails = null;
   	JSONArray myprofilefollow = null;

	
     int pidd;
	 String uid;
	 String us;
	 TextView username;
	 TextView useremail;
	 ImageView userprofileimage;
	 ImageView usercoverimage;
	 String feedimage;
	 //String feedid;
	 String[] feedid;
	 Button follow;
	 Boolean clicked;
	 String followerid;
	 String followername;
	 Button followerscount;
	 Button followingcount;
	 TextView postscountt;
	 String UID;
	 String uimage;
	 
	
	 HashMap<String,String> user;
	 HashMap<String, String> map2 = new HashMap<String, String>();
 
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); 
       
        setContentView(R.layout.profilescreen);
        

       DatabaseHandler db = new DatabaseHandler(getApplicationContext());

        HashMap<String,String> user = new HashMap<String, String>();
        user = db.getUserDetails();
        followerid= user.get("uid"); 
        followername= user.get("fname"); 
        
        actorsList = new ArrayList<FeedList>();

        Intent intent = getIntent();
       Bundle bundle = intent.getExtras();
       uid= bundle.getString("userid"); 
        
        
        username = (TextView) findViewById(R.id.profilefname);
        useremail = (TextView) findViewById(R.id.profileemail);
        followerscount = (Button) findViewById(R.id.followers);
        followingcount = (Button) findViewById(R.id.following);
        postscountt = (TextView) findViewById(R.id.posts);
        follow = (Button) findViewById(R.id.followbtn);
         
        userprofileimage = (ImageView)findViewById(R.id.profileimage);
        usercoverimage = (ImageView)findViewById(R.id.usercoverpicture);
        usercoverimage.setColorFilter(Color.parseColor("#66000000"));
        
        userprofileimage.setOnClickListener(new OnClickListener()
		{

		@Override
		public void onClick(View v) {
			
			
		
			Intent myIntent = new Intent();
			 myIntent = new Intent(PublicProfile.this, Webview.class);
			 myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			 myIntent.putExtra("imagefile",uimage);
			 startActivity(myIntent);
			 

		}


		});
        
   
        
	      Typeface custom_font = Typeface.createFromAsset(getAssets(),
	      "AvenirNextLTPro-Demi.otf");
	      username.setTypeface(custom_font);
	      useremail.setTypeface(custom_font);
	      followerscount.setTypeface(custom_font);
	      followingcount.setTypeface(custom_font);
	      postscountt.setTypeface(custom_font);
	      
	      
	      Typeface custom_font2 = Typeface.createFromAsset(getAssets(),
	    	      "AvenirNextLTPro-Demi.otf");
	      follow.setTypeface(custom_font2);
	    	     
	        grid = (GridView)findViewById(R.id.grid);
			adapter = new CustomGrid(this, R.layout.profilefeed_grid_item, actorsList);
			
			 grid.setAdapter(adapter);
        
			
			 followerscount.setOnClickListener(new View.OnClickListener() {
		            public void onClick(View view) {
		            	
		                Intent myIntent = new Intent(getApplicationContext(), FollowersList.class);
		                myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		                myIntent.putExtra("userid",UID);
		                startActivity(myIntent);
		               
		            }

		        });
			 
			 
			 followingcount.setOnClickListener(new View.OnClickListener() {
		            public void onClick(View view) {
		            	
		                Intent myIntent = new Intent(getApplicationContext(), FollowingList.class);
		                myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		                myIntent.putExtra("userid",UID);
		                startActivity(myIntent);
		               
		            }

		        });
		        
		        
	      ImageButton back = (ImageButton)findViewById(R.id.backbutn);
	        
	        back.setOnClickListener(new OnClickListener()
			{

			@Override
			public void onClick(View v) {
				
				finish();

				 

			}


			});
	        
	        
       
	        
	        follow.setOnClickListener(new OnClickListener()
			{


                public void onClick(View arg0)
                {
                	
                	
                	clicked=true;
    				
    				if (clicked == false) {
    					follow.setText("FOLLOW"); 
    	            } else if (clicked == true) {
    	            	follow.setText("FOLLOWING");
    	            	
    	            }
    				
    				
                	
                   new Thread(new Runnable() {
                         @Override
                         public void run() {
                             try {
                	
                           	   	 
                           	 
             	   	String result = null;
             	   	InputStream is = null;
             	  
             	   String uname = username.getText().toString();
   		        
             
             	   	ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

             	 
    				
             	   
             	  nameValuePairs.add(new BasicNameValuePair("uid",uid));
             	 nameValuePairs.add(new BasicNameValuePair("uname",uname));
             	 nameValuePairs.add(new BasicNameValuePair("followerid",followerid));
             	 nameValuePairs.add(new BasicNameValuePair("followername",followername));
             	

             	   	StrictMode.setThreadPolicy(policy); 


         	//http post
         	try{
         	        HttpClient httpclient = new DefaultHttpClient();
         	        HttpPost httppost = new HttpPost("http://128.199.176.163/elskayapp/elskayfeed/insert_followers.php");
         	        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
         	        HttpResponse response = httpclient.execute(httppost); 
         	        HttpEntity entity = response.getEntity();
         	        is = entity.getContent();

         	        Log.e("log_tag", "connection success ");
         	       
         	        //Toast.makeText(getApplicationContext(), "", Toast.LENGTH_SHORT).show();
         	   }
                            
            
         	
         	catch(Exception e)
         	{
         	        Log.e("log_tag", "Error in http connection "+e.toString());
         	        Toast.makeText(getApplicationContext(), "Connection fail", Toast.LENGTH_SHORT).show();

         	}
         	//convert response to string
         	try{
         	        BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
         	        StringBuilder sb = new StringBuilder();
         	        String line = null;
         	        while ((line = reader.readLine()) != null) 
         	        {
         	                sb.append(line + "\n");
         	             
         	           
       	             
         	        }
         	        is.close();

         	        result=sb.toString();
         	      Log.i("response##########",result);
         	    Toast.makeText(getApplicationContext(),result,Toast.LENGTH_LONG).show();
         	      
            
         	}
         	catch(Exception e)
         	{
         	       Log.e("log_tag", "Error converting result "+e.toString());
            	}


         	try{
         		
         					JSONObject json_data = new JSONObject(result);

         	                CharSequence w= (CharSequence) json_data.get("re");
         	             
         	                Toast.makeText(getApplicationContext(), w, Toast.LENGTH_SHORT).show();
         	                

         	      
         	     }
         	catch(JSONException e)
         	   {
         	        Log.e("log_tag", "Error parsing data "+e.toString());
         	        Toast.makeText(getApplicationContext(), "JsonArray fail", Toast.LENGTH_SHORT).show();
         	    }
         	 Thread.sleep(3000); // Let's wait for some time
                             } catch (Exception e) {
                                  
                             }
                             //progress.dismiss();
                           
                            
                         }
                     }).start(); 
                      
                 } 
                
             }); 
	  	
	  	
	  	
        
	   
 
        // Getting complete product details in background thread
	        if(isNetworkAvailable()){
	            // do network operation here     
	      	   new JSONParses().execute();
	         }else{
	            Toast.makeText(this, "No Available Network. Please try again later", Toast.LENGTH_LONG).show();
	            return;
	         }
        
    }

    private class JSONParses extends AsyncTask<String, String, JSONObject> {
    	 private ProgressDialog pDialog;
    	@Override
        protected void onPreExecute() {
            super.onPreExecute();
                			
            pDialog = new ProgressDialog(PublicProfile.this);
            pDialog.setMessage("Please wait! It’s worth waiting!!");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
            
            
            
    	}
    	
    	@Override
        protected JSONObject doInBackground(String... args) {
    		
    		JSONParser jParser = new JSONParser();
    		 List<NameValuePair> params = new ArrayList<NameValuePair>();
  	       params.add(new BasicNameValuePair("uid", uid));
  	       params.add(new BasicNameValuePair("followerid", followerid));
  		   JSONObject json = jParser.getJSONFromUrl(url_get_userdetails,params);
    		return json;
    		
   		    
    	}
    	 
    	
    	@Override
         protected void onPostExecute(final JSONObject json) {
    		
    	
    		 pDialog.dismiss();
    		
    		 try {
    			 
    			 String followers= json.getString(KEY_FOLLOWCOUNT);
 				followerscount.setText(followers+"\nfollowers"); 
 				
 				String postcount= json.getString(KEY_COUNTPOSTS);
 				postscountt.setText(postcount+"\nPosts"); 
 				
 				String followcount= json.getString(KEY_FOLLOWINGCOUNT);
 				followingcount.setText(followcount+"\nfollowing");
 				
 				myprofiledetails = json.getJSONArray(TAG_OS_TWO);
  				for(int j = 0; j < myprofiledetails.length(); j++){
      				JSONObject object = myprofiledetails.getJSONObject(j);
  			 
      				String uname = object.getString(TAG_MY_USERNAME);
      				String uemail = object.getString(TAG_MY_USEREMAIL);
      			   uimage = object.getString(TAG_MY_USERIMAGE);
      				UID = object.getString(TAG_MY_USERID);
      				String usercoverpic =object.getString(TAG_USERCOVERIMAGE);
      				
      				username.setText(uname);
      				useremail.setText(uemail);
      				
                        String ufinalimage =uimage.replace(" ", "%20");
      				
      				Picasso.with(getApplicationContext())
      		          .load(ufinalimage)
      		          .resize(250, 250)
                        .transform(new RoundedTransformation(100, 20))
                        .centerCrop()
      		          .into(userprofileimage);
      				
      				if(usercoverpic=="null")
      				{
      					usercoverimage.setBackgroundResource(R.drawable.rg2);
      				}
      				else{
                    String ufinalcoverimage =usercoverpic.replace(" ", "%20");
      				
      				Picasso.with(getApplicationContext())
      		          .load(ufinalcoverimage)
      		           .resize(350, 350)
                        .centerCrop()
      		          .into(usercoverimage);
      				}
      				

      				
      				
  				}
  				
  				
  				
  				myprofilefollow = json.getJSONArray(TAG_OS_THREE);
  				for(int z = 0; z < myprofilefollow.length(); z++){
      				JSONObject objectfollow = myprofilefollow.getJSONObject(z);
  			 
      				String followingid = objectfollow.getString(TAG_FOLLOW);
    				Log.i("followingid",followingid);
    				if ((followingid.equals("null"))){
    					follow.setText("FOLLOW"); 
    			      }
    			     
    			   
    			    else {
    			    	follow.setText("FOLLOWING");
    			    	follow.setEnabled(false); 
    			    	
    			    }
      				
  				}
 				
 				
    				// Getting JSON Array from URL
    			 recomments = json.getJSONArray(TAG_OS);
    				for(int i = 0; i < recomments.length(); i++){
    				JSONObject c = recomments.getJSONObject(i);
    				
    				// Storing  JSON item in a Variable
    				
    				
    				/*String followingid = c.getString(TAG_FOLLOW);
    				Log.i("followingid",followingid);
    				if ((followingid.equals("null"))){
    					follow.setText("FOLLOW"); 
    			      }
    			     
    			   
    			    else {
    			    	follow.setText("FOLLOWING");
    			    	follow.setEnabled(false); 
    			    	
    			    }*/
    				
    				
    				
    				
    				 FeedList actor = new FeedList();
						
						actor.setfid(c.getString(TAG_FEEDID));
						actor.setImage(c.getString(TAG_FEEDIMAGE));
						actor.setDescription(c.getString(TAG_FEEDDESC));
						actor.setlocation(c.getString(TAG_FEEDLOCATION));
						actor.setProfile(c.getString(TAG_USERIMAGE));
						
						actor.setName(c.getString(TAG_FEEDUNAME));
						actor.setuserid(c.getString(TAG_FEEDUID));
						
						
						
						actorsList.add(actor); 
    				
    				

    				
                   }
    				
    				
    				
    

    		 }
    		 
    		 
    		
    		 
    		 
    		 
                             
                catch (JSONException e) {
     			 Toast.makeText(getBaseContext(), "Oopss!! Our engineer fell asleep,Please try again Later",
                         Toast.LENGTH_LONG).show();
     			e.printStackTrace();
     		}

     		

     		 
     	 
    	 }	 
                             
                             
    }
    
   
   
    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    } 
    
}