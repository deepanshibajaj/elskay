package elskay.login;


import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import elskay.app.MainTabActivity;
import elskay.app.R;






public class Register extends Activity {


    /**
     *  JSON Response node names.
     **/

	private SessionManager session;
    private static String KEY_SUCCESS = "success";
    private static String KEY_UID = "uid";
    private static String KEY_FIRSTNAME = "fname";
    private static String KEY_LASTNAME = "lname";
    //private static String KEY_USERNAME = "uname";
    private static String KEY_EMAIL = "email";
    private static String KEY_CREATED_AT = "created_at";
    private static String KEY_ERROR = "error";
    private static int RESULT_LOAD_IMG = 1;
    /**
     * Defining layout items.
     **/

    EditText inputFirstName;
    EditText inputLastName;
    //EditText inputUsername;
    EditText inputEmail;
    EditText inputPassword;
    Button btnRegister;
    TextView registerErrorMsg;
    Button login;
    ImageButton profileimage;
    String encodedString;
	String imgPath, imagename;
	Bitmap bitmap;
	ImageView imgView;
	TextView txtView;
	RoundImage roundedImage;
	boolean clicked=false;
	
	
  


    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); 
       setContentView(R.layout.login_register);

    /**
     * Defining all layout items
     **/
        inputFirstName = (EditText) findViewById(R.id.fname);
        inputLastName = (EditText) findViewById(R.id.lname);
        //inputUsername = (EditText) findViewById(R.id.uname);
        inputEmail = (EditText) findViewById(R.id.email);
        inputPassword = (EditText) findViewById(R.id.pword);
        btnRegister = (Button) findViewById(R.id.register);
        registerErrorMsg = (TextView) findViewById(R.id.register_error);
        profileimage = (ImageButton) findViewById(R.id.buttonLoadPicture);

        

	     
        
        inputEmail.setHintTextColor(getResources().getColor(R.color.background));
        inputPassword.setHintTextColor(getResources().getColor(R.color.background));
        inputFirstName.setHintTextColor(getResources().getColor(R.color.background));
        inputLastName.setHintTextColor(getResources().getColor(R.color.background));
        
        
        Typeface font = Typeface.createFromAsset(getAssets(),
      		  "Montserrat-Bold.ttf");
      inputPassword.setTypeface(font);
     
      
      
      Typeface font2 = Typeface.createFromAsset(getAssets(),
    		  "Montserrat-Bold.ttf");
      inputEmail.setTypeface(font2); 
      
      Typeface font5 = Typeface.createFromAsset(getAssets(),
      		  "Montserrat-Bold.ttf");
      inputFirstName.setTypeface(font5); 
      
      
      Typeface font7 = Typeface.createFromAsset(getAssets(),
    		  "Montserrat-Bold.ttf");
      inputLastName.setTypeface(font7); 
      
      
      Typeface font3 = Typeface.createFromAsset(getAssets(),
      		  "Montserrat-Regular.ttf");
      btnRegister.setTypeface(font3); 
      

      
      Typeface font6 = Typeface.createFromAsset(getAssets(),
      		  "Montserrat-Regular.ttf");
      registerErrorMsg.setTypeface(font6);


      ImageButton back = (ImageButton)findViewById(R.id.backbutn);
      
      back.setOnClickListener(new OnClickListener()
		{

		@Override
		public void onClick(View v) {
			
			finish();

		}


		});
/**
 * Button which Switches back to the login screen on clicked
 **/

        login = (Button) findViewById(R.id.bktologin);
        login.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent myIntent = new Intent(view.getContext(), Login.class);
                startActivityForResult(myIntent, 0);
                finish();
            }

        });
        
        Typeface font8 = Typeface.createFromAsset(getAssets(),
        		  "Montserrat-Regular.ttf");
        login.setTypeface(font8); 
        
        profileimage.setOnClickListener(new View.OnClickListener()
        {
        public void onClick(View view) {
        	
        	 // Create intent to Open Image applications like Gallery, Google Photos
        	selectImage();
        	//profileimage.setVisibility(View.GONE);
        	clicked=true;
        	
       }
       });
    
        
        session = new SessionManager(getApplicationContext());
        
        // SQLite database handler

 
        // Check if user is already logged in or not
        if (session.isLoggedIn()) {
            // User is already logged in. Take him to main activity
            Intent intent = new Intent(Register.this,
                    MainTabActivity.class);
            startActivity(intent);
            finish();
        }

        /**
         * Register Button click event.
         * A Toast is set to alert when the fields are empty.
         * Another toast is set to alert Username must be 5 characters.
         **/

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if ( ( !inputPassword.getText().toString().equals("")) && ( !inputFirstName.getText().toString().equals("")) && ( !inputLastName.getText().toString().equals("")) && ( !inputEmail.getText().toString().equals("")))
                {
                    if ( inputFirstName.getText().toString().length() > 2 ){
                    NetAsync(view);

                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(),
                                "Firstname should be minimum 2 characters", Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Toast.makeText(getApplicationContext(),
                            "One or more fields are empty", Toast.LENGTH_SHORT).show();
                }
            }
        });
       }
    /**
     * Async Task to check whether internet connection is working
     **/
    
    
    private void selectImage() {
		 
        final CharSequence[] options = {"Choose from Gallery","Cancel"};
 
        AlertDialog.Builder builder = new AlertDialog.Builder(Register.this);
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
            	
            	  if (options[item].equals("Choose from Gallery"))
                {
                    Intent intent = new   Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, 1);
                     
                }
                else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                    clicked=false;
                }
            }
        });
        builder.show();
    }
    
    
    @Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    super.onActivityResult(requestCode, resultCode, data);
	    try {
	        // When an Image is picked
	        if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK  && data != null && data.getData() != null)
	                {
	            // Get the Image from data
	        	 

	            Uri selectedImage = data.getData();
	            String[] filePathColumn = {MediaStore.Images.Media.DATA};

	            // Get the cursor
	            Cursor cursor = getContentResolver().query(selectedImage,
	                    filePathColumn, null, null, null);
	            // Move to first row
	            cursor.moveToFirst();

	            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
	            imgPath = cursor.getString(columnIndex);
	            cursor.close();
	           
	           
	            	imgView = (ImageView) findViewById(R.id.profileimage);
		            // Set the Image in ImageView after decoding the String
		            imgView.setImageBitmap(BitmapFactory
		                    .decodeFile(imgPath));
		            /*Bitmap bm = BitmapFactory.decodeFile(imgPath);
		            roundedImage = new RoundImage(bm);
		            imgView.setImageDrawable(roundedImage); */
		            
		            Bitmap icon = BitmapFactory.decodeFile(imgPath);
		             
		            imgView.setImageBitmap(icon);
	            
	           
	            // Get the Image's file name
	            /*String fileNameSegments[] = imgPath.split("/");
	            imagename = fileNameSegments[fileNameSegments.length - 1]; */


	        } else {
	            Toast.makeText(this, "You haven't picked Image",
	                    Toast.LENGTH_LONG).show();
	            clicked=false;
	        }
	    } catch (Exception e) {
	        Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
	                .show();
	    }
    }
    private class NetCheck extends AsyncTask<String,String,Boolean>
    {
        private ProgressDialog nDialog;

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            nDialog = new ProgressDialog(Register.this);
            nDialog.setMessage("Loading..");
            nDialog.setTitle("Checking Network");
            nDialog.setIndeterminate(false);
            nDialog.setCancelable(true);
            nDialog.show();
        }

        @Override
        protected Boolean doInBackground(String... args){


/**
 * Gets current device state and checks for working internet connection by trying Google.
 **/
            ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            if (netInfo != null && netInfo.isConnected()) {
                try {
                    URL url = new URL("http://www.google.com");
                    HttpURLConnection urlc = (HttpURLConnection) url.openConnection();
                    urlc.setConnectTimeout(3000);
                    urlc.connect();
                    if (urlc.getResponseCode() == 200) {
                        return true;
                    }
                } catch (MalformedURLException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            return false;

        }
        @Override
        protected void onPostExecute(Boolean th){

            if(th == true){
                nDialog.dismiss();
                new ProcessRegister().execute();
            }
            else{
                nDialog.dismiss();
                registerErrorMsg.setText("Error in Network Connection");
            }
        }
    }





    private class ProcessRegister extends AsyncTask<String, String, JSONObject> {

/**
 * Defining Process dialog
 **/
        private ProgressDialog pDialog;

        String email,password,fname,lname,uname,profileimage,filename;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
           // inputUsername = (EditText) findViewById(R.id.uname);
            inputPassword = (EditText) findViewById(R.id.pword);
               fname = inputFirstName.getText().toString();
               lname = inputLastName.getText().toString();
                email = inputEmail.getText().toString();
               // uname= inputUsername.getText().toString();
                password = inputPassword.getText().toString();
                
                        
                       if (clicked)
                         {  

                    	   String fileNameSegments[] = imgPath.split("/");
                           filename = fileNameSegments[fileNameSegments.length - 1];
                          
                           BitmapFactory.Options options = null;
                           options = new BitmapFactory.Options();
                           options.inSampleSize=2;
                            bitmap = BitmapFactory.decodeFile(imgPath,
                             options);
                            /*bitmap = Bitmap.createScaledBitmap(bitmap,
                                    250, 250, false);*/
                                    
                           ByteArrayOutputStream stream2 = new ByteArrayOutputStream();
                           bitmap.compress(Bitmap.CompressFormat.JPEG, 90, stream2);
                           byte[] byte_arr2 = stream2.toByteArray();
                           profileimage = Base64.encodeToString(byte_arr2,0);
                         }
                       else if (clicked=false)
                       {
                    	   bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.user);
                    	   bitmap = Bitmap.createScaledBitmap(bitmap,
                                   250, 250, false);
                    	     filename="demo.jpg";
                    	     ByteArrayOutputStream stream = new ByteArrayOutputStream();
                             bitmap.compress(Bitmap.CompressFormat.JPEG, 90, stream);
                             byte[] byte_arr = stream.toByteArray();
                             profileimage = Base64.encodeToString(byte_arr,0);
                       } 
                       else
                       {
                    	   bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.user);
                    	   bitmap = Bitmap.createScaledBitmap(bitmap,
                                   250, 250, false);
                    	     filename="demo.jpg";
                    	     ByteArrayOutputStream stream = new ByteArrayOutputStream();
                             bitmap.compress(Bitmap.CompressFormat.JPEG, 90, stream);
                             byte[] byte_arr = stream.toByteArray();
                             profileimage = Base64.encodeToString(byte_arr,0);
                       } 
            
                  
                  Log.i("profileimage@@",profileimage);
                 
                  
            pDialog = new ProgressDialog(Register.this);
            //pDialog.setTitle("Contacting Servers");
            pDialog.setMessage("Searching for a hanger to put your information in  our wardrobe.");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        
        
        
        @Override
        protected JSONObject doInBackground(String... args) {


        UserFunctions userFunction = new UserFunctions();
        JSONObject json = userFunction.registerUser(fname, lname, email, password,profileimage,filename);

            return json;


        }
       @Override
        protected void onPostExecute(JSONObject json) {
       /**
        * Checks for success message.
        **/
                try {
                    if (json.getString(KEY_SUCCESS) != null) {
                        registerErrorMsg.setText("");
                        String res = json.getString(KEY_SUCCESS);

                        String red = json.getString(KEY_ERROR);

                        if(Integer.parseInt(res) == 1){
                           // pDialog.setTitle("Getting Data");
                            //pDialog.setMessage("Loading Info");

                            registerErrorMsg.setText("Successfully Registered");


                            DatabaseHandler db = new DatabaseHandler(getApplicationContext());
                            JSONObject json_user = json.getJSONObject("user");

                            /**
                             * Removes all the previous data in the SQlite database
                             **/

                            UserFunctions logout = new UserFunctions();
                            logout.logoutUser(getApplicationContext());
                            db.addUser(json_user.getString(KEY_FIRSTNAME),json_user.getString(KEY_LASTNAME),json_user.getString(KEY_EMAIL),json_user.getString(KEY_UID),json_user.getString(KEY_CREATED_AT));
                            /**
                             * Stores registered data in SQlite Database
                             * Launch Registered screen
                             **/
                            
                            session.setLogin(true);//session
                            Intent registered = new Intent(getApplicationContext(), Registered.class);

                            /**
                             * Close all views before launching Registered screen
                            **/
                           registered.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            pDialog.dismiss();
                            startActivity(registered);


                              finish();
                        }

                        else if (Integer.parseInt(red) ==2){
                            pDialog.dismiss();
                            registerErrorMsg.setText("User already exists");
                        }
                        else if (Integer.parseInt(red) ==3){
                            pDialog.dismiss();
                            registerErrorMsg.setText("Invalid Email id");
                        }

                    }


                        else{
                        pDialog.dismiss();

                            registerErrorMsg.setText("Error occured in registration,Please Try Again");
                        }

                } catch (JSONException e) {
                    e.printStackTrace();


                }
            }}
        public void NetAsync(View view){
            new NetCheck().execute();
        }}


