package elskay.login;


import java.util.HashMap;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import elskay.app.MainTabActivity;
import elskay.app.R;


public class Registered extends Activity {

	 Button login;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); 
        setContentView(R.layout.registered);


        DatabaseHandler db = new DatabaseHandler(getApplicationContext());

        HashMap<String,String> user = new HashMap<String, String>();
        user = db.getUserDetails(); 

        /**
         * Displays the registration details in Text view
         **/

        final TextView fname = (TextView)findViewById(R.id.fname);
        final TextView email = (TextView)findViewById(R.id.email);
 
         fname.setText(user.get("fname"));
         email.setText(user.get("email"));
         
        
       
        final TextView sr = (TextView)findViewById(R.id.welcome);
        Typeface font3 = Typeface.createFromAsset(getAssets(),
        		  "Montserrat-Regular.ttf");
        sr.setTypeface(font3); 
        

    





    
    Handler handler =new Handler();
	
	handler.postDelayed(new Runnable() 
	{
		

	 
	public void run()
	{
		
		Intent openMainActivity =  new Intent(Registered.this, MainTabActivity.class);
        startActivity(openMainActivity);
        finish();
		}
	},2000);
	
	}
	
	
	    




}
