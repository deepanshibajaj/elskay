
package elskay.login;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import elskay.addtocart.ECart;
import elskay.app.BadgeDrawable;
import elskay.app.BaseActivity;
import elskay.app.EditProfile;
import elskay.app.R;
import elskay.app.RoundedTransformation;
import elskay.feed.DiscoverNotification;
import elskay.feed.FeedAsk;
import elskay.feed.FeedList;




 
public class UserProfile extends BaseActivity {
 
   GridView grid;
	
    ArrayList<FeedList> actorsList;
    
    CustomGrid adapter;
    private Menu mToolbarMenu;
    int coot;
 
    
     Button followerscount;
	 Button followingcount;
	 Button postscountt;
    // Progress Dialog
    private ProgressDialog pDialog;
    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
    // JSON parser class
    JSONParser jsonParser = new JSONParser();  
  
    private static String url_getmyprofile = "http://128.199.176.163/elskayapp/elskayapp_login/login_getmyprofile.php";
    
    
 	

   	private static final String TAG_OS = "profile";
    
      private static final String TAG_USERNAME = "firstname";
      private static final String TAG_USEREMAIL = "email";
      private static final String TAG_USERIMAGE = "profileimage";
      private static final String TAG_FEEDIMAGE = "feedimage";
      private static final String TAG_FEEDDESC = "feedhashtag";
      private static final String TAG_FEEDID = "fid";
      private static final String TAG_FEEDLOCATION = "feedlocation";
      private static final String TAG_FEEDUNAME = "feeduname";
      private static final String TAG_FEEDUID = "unique_id";
      
      private static String KEY_FOLLOWCOUNT = "followers";
      private static String KEY_COUNTPOSTS = "postscount";
      private static String KEY_FOLLOWINGCOUNT = "following";
      
      private static final String TAG_OS_TWO = "profiledetail";
      
      private static final String TAG_MY_USERID = "unique_id";
      private static final String TAG_MY_USERNAME = "firstname";
      private static final String TAG_MY_USEREMAIL = "email";
      private static final String TAG_MY_USERIMAGE = "profileimage";
      private static final String TAG_USERCOVERIMAGE = "coverimage";
    
      
   	JSONArray recomments = null;
   	JSONArray myprofiledetails = null;


	
     int pidd;
	 String uid;
	 String us;
	 TextView username;
	 TextView useremail;
	 ImageView userprofileimage;
	 ImageView usercoverimage;
	 private DrawerLayout mDrawerLayout;
	 String UID;
	 
	
	 HashMap<String,String> user;
	 HashMap<String, String> map2 = new HashMap<String, String>();
 
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

      
        getLayoutInflater().inflate(R.layout.myprofile, frameLayout);
        //setContentView(R.layout.user_profile);
        
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        coot = settings.getInt("counts",0);
        
       DatabaseHandler db = new DatabaseHandler(getApplicationContext());

        HashMap<String,String> user = new HashMap<String, String>();
        user = db.getUserDetails();
        uid= user.get("uid"); 
        
        actorsList = new ArrayList<FeedList>();
        
        getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM); 
        getActionBar().setCustomView(R.layout.actionbar_userprofile);
        TextView ac = (TextView)findViewById(R.id.acb);
        Typeface font = Typeface.createFromAsset(getAssets(),
        "AvenirNextLTPro-Regular.otf");
        ac.setTypeface(font); 
        getActionBar().setHomeButtonEnabled(true);
        
        getActionBar().setDisplayShowTitleEnabled(false);
  
        getActionBar().setDisplayHomeAsUpEnabled(true);
        
        username = (TextView) findViewById(R.id.profilefname);
        useremail = (TextView) findViewById(R.id.profileemail);
        followerscount = (Button) findViewById(R.id.followers);
        followingcount = (Button) findViewById(R.id.following);
        postscountt = (Button) findViewById(R.id.posts);
         
        userprofileimage = (ImageView)findViewById(R.id.profileimage);
        usercoverimage = (ImageView)findViewById(R.id.usercoverpic);
        usercoverimage.setColorFilter(Color.parseColor("#66000000"));
        
        userprofileimage.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
            	
                Intent myIntent = new Intent(getApplicationContext(), EditProfile.class);
                startActivity(myIntent);
               
            }

        });
        
	      Typeface custom_font = Typeface.createFromAsset(getAssets(),
	      "AvenirNextLTPro-Demi.otf");
	      username.setTypeface(custom_font);
	      useremail.setTypeface(custom_font);
	      followerscount.setTypeface(custom_font);
	      followingcount.setTypeface(custom_font);
	      postscountt.setTypeface(custom_font);
	      
        
	      grid = (GridView)findViewById(R.id.gridtwo);
			adapter = new CustomGrid(getApplicationContext(), R.layout.profilefeed_grid_item, actorsList);
			
			 grid.setAdapter(adapter);
			 
			 followerscount.setOnClickListener(new View.OnClickListener() {
		            public void onClick(View view) {
		            	
		                Intent myIntent = new Intent(getApplicationContext(), FollowersList.class);
		                myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		                myIntent.putExtra("userid",UID);
		                startActivity(myIntent);
		               
		            }

		        });
			 
			 
			 followingcount.setOnClickListener(new View.OnClickListener() {
		            public void onClick(View view) {
		            	
		                Intent myIntent = new Intent(getApplicationContext(), FollowingList.class);
		                myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		                myIntent.putExtra("userid",UID);
		                startActivity(myIntent);
		               
		            }

		        });
			 
			 ImageButton button3 = (ImageButton)findViewById(R.id.discover_ask_button);
			  
		        
		        button3.setOnClickListener(new View.OnClickListener()
			    {
			    public void onClick(View view) {
			    	
			    	Intent intent = new Intent(getApplicationContext(), FeedAsk.class);
			    	 startActivity(intent);
			   }
			   });
        
	     
        // Getting complete product details in background thread
	        if(isNetworkAvailable()){
	            // do network operation here     
	      	   new JSONParses().execute();
	         }else{
	            Toast.makeText(this, "No Available Network. Please try again later", Toast.LENGTH_LONG).show();
	            return;
	         }
        
    }

    private class JSONParses extends AsyncTask<String, String, JSONObject> {
    	 private ProgressDialog pDialog;
    	@Override
        protected void onPreExecute() {
            super.onPreExecute();
                			
            pDialog = new ProgressDialog(UserProfile.this);
            pDialog.setMessage("Please wait! It’s worth waiting!!");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
            
            
            
    	}
    	
    	@Override
        protected JSONObject doInBackground(String... args) {
    		
    		JSONParser jParser = new JSONParser();
    		 List<NameValuePair> params = new ArrayList<NameValuePair>();
  	       params.add(new BasicNameValuePair("uid", uid));
  		   JSONObject json = jParser.getJSONFromUrl(url_getmyprofile,params);
    		return json;
    		
   		    
    	}
    	 
    	
    	@Override
         protected void onPostExecute(final JSONObject json) {
    		
    	
    		 pDialog.dismiss();
    		
    		 try {
    				// Getting JSON Array from URL
    			 
    			 String followers= json.getString(KEY_FOLLOWCOUNT);
  				followerscount.setText(followers+"\nfollowers"); 
  				
  				String postcount= json.getString(KEY_COUNTPOSTS);
  				postscountt.setText(postcount+"\nPosts"); 
  				
  				String followcount= json.getString(KEY_FOLLOWINGCOUNT);
  				followingcount.setText(followcount+"\nfollowing");
    			 
    			 myprofiledetails = json.getJSONArray(TAG_OS_TWO);
  				for(int j = 0; j < myprofiledetails.length(); j++){
      				JSONObject object = myprofiledetails.getJSONObject(j);
  			 
      				String uname = object.getString(TAG_MY_USERNAME);
      				String uemail = object.getString(TAG_MY_USEREMAIL);
      				String uimage = object.getString(TAG_MY_USERIMAGE);
      				UID = object.getString(TAG_MY_USERID);
      				String usercoverpic =object.getString(TAG_USERCOVERIMAGE);
      				
      				
      				username.setText(uname);
      				useremail.setText(uemail);
      				
                        String ufinalimage =uimage.replace(" ", "%20");
      				
      				Picasso.with(getApplicationContext())
      		          .load(ufinalimage)
      		          .resize(250, 250)
                        .transform(new RoundedTransformation(100, 20))
                        .centerCrop()
      		          .into(userprofileimage);
      				
      				
      				if(usercoverpic=="null")
      				{
      					usercoverimage.setBackgroundResource(R.drawable.rg2);
      				}
      				else{
                    String ufinalcoverimage =usercoverpic.replace(" ", "%20");
      				
      				Picasso.with(getApplicationContext())
      		          .load(ufinalcoverimage)
      		           .resize(350, 350)
                        .centerCrop()
      		          .into(usercoverimage);
      				}
  				}
 				
    			 
    			 recomments = json.getJSONArray(TAG_OS);
    				for(int i = 0; i < recomments.length(); i++){
    				JSONObject c = recomments.getJSONObject(i);
    				
    				
    				
    				// Storing  JSON item in a Variable
    				
    				/*String uname = c.getString(TAG_USERNAME);
    				String uemail = c.getString(TAG_USEREMAIL);
    				String uimage = c.getString(TAG_USERIMAGE);
    				
    				
    		        
    				username.setText(uname);
    				useremail.setText(uemail);
    				
                      String ufinalimage =uimage.replace(" ", "%20");
    				
    				Picasso.with(getApplicationContext())
    		          .load(ufinalimage)
    		          .resize(250, 250)
                      .transform(new RoundedTransformation(100, 20))
                      .centerCrop()
    		          .into(userprofileimage);*/
    				
    				 FeedList actor = new FeedList();
						
						actor.setfid(c.getString(TAG_FEEDID));
						actor.setImage(c.getString(TAG_FEEDIMAGE));
						actor.setDescription(c.getString(TAG_FEEDDESC));
						actor.setlocation(c.getString(TAG_FEEDLOCATION));
						actor.setProfile(c.getString(TAG_USERIMAGE));
						actor.setName(c.getString(TAG_FEEDUNAME));
						actor.setuserid(c.getString(TAG_FEEDUID));
						
						
						
						actorsList.add(actor); 
    				 
    				

    				
                   }
    				
    				
    				
    	        
    

    		 }
    		 
    		 
    		
    		 
    		 
    		 
                             
                catch (JSONException e) {
     			 Toast.makeText(getBaseContext(), "Oopss!! Our engineer fell asleep,Please try again Later",
                         Toast.LENGTH_LONG).show();
     			e.printStackTrace();
     		}

     		

     		 
     	 
    	 }	 
                             
                             
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
     public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case R.id.action_settings: 
           
        	 Intent profile = new Intent(getApplicationContext(),ECart.class);
             startActivity(profile);
             finish();
            return true;
            
        case R.id.notification: 
            
          	 Intent notification = new Intent(getApplicationContext(),DiscoverNotification.class);
               startActivity(notification);
               finish();
              return true;  
        
      
              
        default:
            return super.onOptionsItemSelected(item);
        
 
        }
     } 
     public boolean onPrepareOptionsMenu(Menu paramMenu) {
 		mToolbarMenu = paramMenu;
 		createCartBadge(10);
 		return super.onPrepareOptionsMenu(paramMenu);
 	}
     private void createCartBadge(int paramInt) {
 		/*if (Build.VERSION.SDK_INT <= 15) {
 			return;
 		}*/
 		MenuItem cartItem = this.mToolbarMenu.findItem(R.id.action_settings);
 		LayerDrawable localLayerDrawable = (LayerDrawable) cartItem.getIcon();
 		Drawable cartBadgeDrawable = localLayerDrawable
 				.findDrawableByLayerId(R.id.ic_badge);
 		BadgeDrawable badgeDrawable;
 		if ((cartBadgeDrawable != null)
 				&& ((cartBadgeDrawable instanceof BadgeDrawable))
 				&& (paramInt < 10)) {
 			badgeDrawable = (BadgeDrawable) cartBadgeDrawable;
 		} else {
 			badgeDrawable = new BadgeDrawable(this);
 		}
 		int counttwo=3;
 		badgeDrawable.setCount(coot);
 		localLayerDrawable.mutate();
 		localLayerDrawable.setDrawableByLayerId(R.id.ic_badge, badgeDrawable);
 		cartItem.setIcon(localLayerDrawable);
 	}
   
    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    } 
    
}