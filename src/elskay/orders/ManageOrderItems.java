package elskay.orders;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.bhavin.horizontallistview.HorizontalListView;


import com.squareup.picasso.Picasso;

import elskay.addtocart.ECart;
import elskay.app.BadgeDrawable;
import elskay.app.BaseActivity;
import elskay.app.DJSONParserPost;
import elskay.app.R;
import elskay.feed.DiscoverView;
import elskay.login.DatabaseHandler;


 
public class ManageOrderItems extends Activity {
 
	EditText tx;
	ImageButton button;
	ImageView img;
	String commentcount;
	String username;
	JSONObject c;
	

    private ProgressDialog pDialog;
    private ProgressBar bar;
    
    HorizontalListView hlvSimple;
    
    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
    // JSON parser class
    DJSONParserPost jsonParser = new DJSONParserPost();  
    List<HashMap<String, String>> oslist = new ArrayList<HashMap<String, String>>();
    private static String url_get_orders = "http://128.199.176.163/elskayapp/elskayshop/user_getorderitems.php";

   	  private static final String TAG_OS = "orderitems";
      private static final String TAG_ITEMNAME = "item_name";
      private static final String TAG_ITEMPRICE = "item_price";
      private static final String TAG_ITEMIMAGE = "item_imagelink";
      private static final String TAG_ITEMID = "item_id";
      private static final String TAG_SHIPPINGID = "shipping_id";
      private static final String TAG_SHIPPINGSTATUS = "shipping_status";
      private static final String TAG_SHIPPINGPHONENUMBER = "shipping_user_phonenumber";
      private static final String TAG_SHIPPINGADDRESS = "shipping_address";
     
  
      
   	JSONArray recomments = null;

	private String qqid;
	int countt;
	
     int pidd;
	 String pid ;
	 String us;
	 String uid;
     String usernameee;
	 String imgcomment;
	 String adviceques;
	 ListView listView;
	 TextView ques;
	 String ccount;
	 String discoverusername;
	 String discoverprofileimage;
	 String discoverlocation;
	 String discoveruid;
	 private ActionBar actionBar;
	 private Menu mToolbarMenu;
	 ImageView processed;
	 ImageView shipping;
	 ImageView shipped;
	 ImageView outfordelivery;
	 ImageView completed;
	
	 HashMap<String,String> user;
	 HashMap<String, String> map2 = new HashMap<String, String>();
	 
	 int coot;
	 String orderid;
	 TextView address;
 
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); 
       
        setContentView(R.layout.manage_products_list);
        
 
        Intent intent = getIntent();
        orderid = intent.getStringExtra("orderid");
        Log.i("orderid",orderid);

         listView = (ListView) findViewById(android.R.id.list);
         
         processed = (ImageView)findViewById(R.id.processed);
         shipped = (ImageView)findViewById(R.id.shipped);
         shipping = (ImageView)findViewById(R.id.shipping);
         outfordelivery = (ImageView)findViewById(R.id.outfordelivery);
         completed = (ImageView)findViewById(R.id.completed);
         
         address = (TextView)findViewById(R.id.address);
         
         
         ImageButton back = (ImageButton)findViewById(R.id.backbutn);
         
         back.setOnClickListener(new OnClickListener()
 		{

 		@Override
 		public void onClick(View v) {
 			
 			
 			 finish();
 			
 			 

 		}


 		}); 
         
       
		
		
	   
         // Getting complete product details in background thread
	        if(isNetworkAvailable()){
	            // do network operation here     
	      	   new JSONParses().execute();
	         }else{
	            Toast.makeText(this, "No Available Network. Please try again later", Toast.LENGTH_LONG).show();
	            return;
	         }
        
        oslist = new ArrayList<HashMap<String, String>>();
        
        
    }
 

    private class JSONParses extends AsyncTask<String, String, JSONObject> {
    	 private ProgressDialog pDialog;
    	@Override
        protected void onPreExecute() {
            super.onPreExecute();
                			
           pDialog = new ProgressDialog(ManageOrderItems.this);
            pDialog.setMessage("Loading....");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show(); 
          
            
            
            
    	}
    	
    	@Override
        protected JSONObject doInBackground(String... args) {
    		
    		DJSONParserPost jParser = new DJSONParserPost();
    		 List<NameValuePair> params = new ArrayList<NameValuePair>();
  	       params.add(new BasicNameValuePair("orderid", orderid));
  		   JSONObject json = jParser.getJSONFromUrl(url_get_orders,params);
    		return json;
    		
   		    
    	}
    	 
    	
    	@Override
         protected void onPostExecute(final JSONObject json) {
    		
    		//bar.setVisibility(View.GONE);
    		pDialog.dismiss();
    		
    		 try {
    				// Getting JSON Array from URL
    			 recomments = json.getJSONArray(TAG_OS);
    				for(int i = 0; i < recomments.length(); i++){
    				c = recomments.getJSONObject(i);
    				
    				// Storing  JSON item in a Variable
    				
    				String itemname = c.getString(TAG_ITEMNAME);
    				String itemprice = c.getString(TAG_ITEMPRICE);
    				String itemimage = c.getString(TAG_ITEMIMAGE);
    				String itemid = c.getString(TAG_ITEMID);
    				
    				String shippingid = c.getString(TAG_SHIPPINGID);
    				String shippingphonenumber = c.getString(TAG_SHIPPINGPHONENUMBER);
    				String shippingaddress = c.getString(TAG_SHIPPINGADDRESS);
    				String shippingstatus = c.getString(TAG_SHIPPINGSTATUS);
    				Log.i("shippingstatus",shippingstatus);
    				
    				String lineSep = System.getProperty("line.separator");
    				String shippingadd = shippingaddress.replaceAll("<br />", lineSep);
    				 address.setText(shippingadd+"\n\n"+shippingphonenumber);
    				
    				  if (shippingstatus.equals("processing")) 
    		          { 
    					  processed.setBackgroundResource(R.drawable.waittick);
    					  shipping.setBackgroundResource(R.drawable.waittick);
    					  Animation anim = new AlphaAnimation(0.0f, 1.0f);
    				        anim.setDuration(190); //You can manage the time of the blink with this parameter
    				        anim.setStartOffset(20);
    				        anim.setRepeatMode(Animation.REVERSE);
    				        anim.setRepeatCount(Animation.INFINITE);
    				        shipping.startAnimation(anim);
    					  completed.setBackgroundResource(R.drawable.circleoutline);
    		          }
    				  else if (shippingstatus.equals("shipped")) 
    				  {
    					  processed.setBackgroundResource(R.drawable.waittick);
    					  shipped.setBackgroundResource(R.drawable.waittick);
    					  
    					  outfordelivery.setBackgroundResource(R.drawable.waittick);
    					  Animation anim = new AlphaAnimation(0.0f, 1.0f);
    				        anim.setDuration(190); //You can manage the time of the blink with this parameter
    				        anim.setStartOffset(20);
    				        anim.setRepeatMode(Animation.REVERSE);
    				        anim.setRepeatCount(Animation.INFINITE);
    				        outfordelivery.startAnimation(anim);
    					  completed.setBackgroundResource(R.drawable.circleoutline);
    				  }
    				  else if (shippingstatus.equals("completed")) 
    				  {
    					  processed.setBackgroundResource(R.drawable.waittick);
    					  shipped.setBackgroundResource(R.drawable.waittick);
    					  completed.setBackgroundResource(R.drawable.waittick);
    				  }
    				  else
    				  {
    					  processed.setBackgroundResource(R.drawable.circleoutline);
    					  shipped.setBackgroundResource(R.drawable.circleoutline);
    					  completed.setBackgroundResource(R.drawable.circleoutline);  
    				  }
    				
    		        // Adding value HashMap key => value
    				HashMap<String, String> map = new HashMap<String, String>();
    				
    				map.put(TAG_ITEMNAME, itemname);
    				map.put(TAG_ITEMPRICE, itemprice);
    				map.put(TAG_ITEMIMAGE, itemimage);
    				map.put(TAG_ITEMID, itemid);
    				
    				
    				oslist.add(map);
    				
    				
    			

    				
                   }
    				
    				
    				
    	        
    ListAdapter adapter = new SimpleAdapter(
    	ManageOrderItems.this, oslist,
            R.layout.manage_products_items, new String[] {
    			TAG_ITEMNAME,TAG_ITEMPRICE,TAG_ITEMIMAGE},
            new int[] {  R.id.oproductname, R.id.oproductprice, R.id.oproductimage}){
        @Override
    public View getView(int pos, View convertView, ViewGroup parent){
        View v = convertView;
        if(v== null){
            LayoutInflater vi = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v=vi.inflate(R.layout.manage_products_items, null);
        }
        TextView tv = (TextView)v.findViewById(R.id.oproductname);
        tv.setText(oslist.get(pos).get(TAG_ITEMNAME));
       
        
        TextView tvs = (TextView)v.findViewById(R.id.oproductprice);
        tvs.setText("₹"+oslist.get(pos).get(TAG_ITEMPRICE));
         
        String imageurl=oslist.get(pos).get(TAG_ITEMIMAGE);
        
        ImageView productimg = (ImageView)v.findViewById(R.id.oproductimage);
       
        Picasso.with(v.getContext()).load(imageurl).into(productimg);
     
      
        return v;
    }
   
       
};
    
// updating listview
listView.setAdapter(adapter);
   
	
   
   
    //Toast.makeText(getApplicationContext(), "Total number of Items are:" + adapter.getCount() , Toast.LENGTH_LONG).show();

    		 }
    		 
    		 
    		
    		 
    		 
    		 
                             
                catch (JSONException e) {
     			 Toast.makeText(getBaseContext(), "Oopss!! Our engineer fell asleep,Please try again Later",
                         Toast.LENGTH_LONG).show();
     			e.printStackTrace();
     		}

     		

     		 
     	 
    	 }

			 
                             
                             
    }
    
   
   
    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }



   

	
  
  
    
}
