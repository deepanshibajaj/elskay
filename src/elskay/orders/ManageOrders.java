package elskay.orders;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.bhavin.horizontallistview.HorizontalListView;



import com.squareup.picasso.Picasso;

import elskay.addtocart.ECart;
import elskay.app.BadgeDrawable;
import elskay.app.BaseActivity;
import elskay.app.DJSONParserPost;
import elskay.app.R;
import elskay.feed.DiscoverNotification;
import elskay.login.DatabaseHandler;


 
public class ManageOrders extends BaseActivity {
 
	EditText tx;
	ImageButton button;
	ImageView img;
	String commentcount;
	String username;
	JSONObject c;
	

    private ProgressDialog pDialog;
    private ProgressBar bar;
    
    HorizontalListView hlvSimple;
    
    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
    // JSON parser class
    DJSONParserPost jsonParser = new DJSONParserPost();  
    List<HashMap<String, String>> oslist = new ArrayList<HashMap<String, String>>();
    private static String url_get_orders = "http://128.199.176.163/elskayapp/elskayshop/user_getorder.php";

 	  private static final String TAG_OS = "yourorders";
    	private static final String TAG_ORDER_ID = "id";
    private static final String TAG_ORDER_UNIQUE_ID = "order_unique_id";
    private static final String TAG_ORDER_DATE = "order_date";
    private static final String TAG_AMOUNT = "amount";
    private static final String TAG_PAYMENT_METHOD = "payment_method";
    private static final String TAG_ITEMS = "items";
  
      
   	JSONArray recomments = null;

	private String qqid;
	int countt;
	
     int pidd;
	 String pid ;
	 String us;
	 String uid;
     String usernameee;
	 String imgcomment;
	 String adviceques;
	 ListView listView;
	 TextView ques;
	 String ccount;
	 String discoverusername;
	 String discoverprofileimage;
	 String discoverlocation;
	 String discoveruid;
	 private ActionBar actionBar;
	 private Menu mToolbarMenu;
	 TextView orderid;
	 ImageView bg;
     TextView ebg;
	 HashMap<String,String> user;
	 HashMap<String, String> map2 = new HashMap<String, String>();
	 
	 int coot;
 
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //requestWindowFeature(Window.FEATURE_NO_TITLE); 
        getLayoutInflater().inflate(R.layout.manage_orders_list, frameLayout);
        //setContentView(R.layout.manage_orders_list);
        
        bg = (ImageView)findViewById(R.id.bgorders);
        ebg = (TextView)findViewById(R.id.ordertext);
       
        DatabaseHandler db = new DatabaseHandler(getApplicationContext());

        HashMap<String,String> user = new HashMap<String, String>();
        user = db.getUserDetails();
        uid= user.get("uid");
        username= user.get("fname");

         listView = (ListView) findViewById(android.R.id.list);
         listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

	            @Override
	            public void onItemClick(AdapterView<?> parent, View view,
	                                    int position, long id) {
	            	
	            	 String orderidd=oslist.get(position).get(TAG_ORDER_ID);
	            	 Log.i("orderid",orderidd);
	            	
	            	  Intent in = new Intent(getApplicationContext(),
                             ManageOrderItems.class);
	            	  in.putExtra("orderid", orderidd);
                   
                     startActivity(in);

	            }
	        });
         
         getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM); 
         getActionBar().setCustomView(R.layout.actionbar_orders);
         
         getActionBar().setStackedBackgroundDrawable(new ColorDrawable(Color.parseColor("#ffffff")));
         TextView ac = (TextView)findViewById(R.id.acb);
         Typeface font = Typeface.createFromAsset(getAssets(),
         "AvenirNextLTPro-Regular.otf");
         ac.setTypeface(font); 
         
         actionBar = getActionBar();
         actionBar.setDisplayHomeAsUpEnabled(true); 
         Drawable res = getResources().getDrawable(R.drawable.singleline);
         getActionBar().setBackgroundDrawable(res);
         
         actionBar.setHomeButtonEnabled(true);
         actionBar.setDisplayShowTitleEnabled(false);
         
         
         SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
         coot = settings.getInt("counts",0);
         
       
		
		
	   
         // Getting complete product details in background thread
	        if(isNetworkAvailable()){
	            // do network operation here     
	      	   new JSONParses().execute();
	         }else{
	            Toast.makeText(this, "No Available Network. Please try again later", Toast.LENGTH_LONG).show();
	            return;
	         }
        
        oslist = new ArrayList<HashMap<String, String>>();
        
        
    }
 

    private class JSONParses extends AsyncTask<String, String, JSONObject> {
    	 private ProgressDialog pDialog;
    	@Override
        protected void onPreExecute() {
            super.onPreExecute();
                			
           pDialog = new ProgressDialog(ManageOrders.this);
            pDialog.setMessage("Loading....");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show(); 
          
            
            
            
    	}
    	
    	@Override
        protected JSONObject doInBackground(String... args) {
    		
    		DJSONParserPost jParser = new DJSONParserPost();
    		 List<NameValuePair> params = new ArrayList<NameValuePair>();
  	       params.add(new BasicNameValuePair("uid", uid));
  		   JSONObject json = jParser.getJSONFromUrl(url_get_orders,params);
    		return json;
    		
   		    
    	}
    	 
    	
    	@Override
         protected void onPostExecute(final JSONObject json) {
    		
    		//bar.setVisibility(View.GONE);
    		pDialog.dismiss();
    		
    		 try {
//Log.e("json",json.toString());
    				// Getting JSON Array from URL
    			 recomments = json.getJSONArray(TAG_OS);
    				for(int i = 0; i < recomments.length(); i++){
    				c = recomments.getJSONObject(i);
    				
    				// Storing  JSON item in a Variable
    				String id = c.getString(TAG_ORDER_ID);
    				String orderid = c.getString(TAG_ORDER_UNIQUE_ID);
    				String orderdate = c.getString(TAG_ORDER_DATE);
    				String orderamount = c.getString(TAG_AMOUNT);
    				String paymentmethod = c.getString(TAG_PAYMENT_METHOD);
    				String ordernoitems = c.getString(TAG_ITEMS);
    				
    		        // Adding value HashMap key => value
    				HashMap<String, String> map = new HashMap<String, String>();
    				
    				map.put(TAG_ORDER_ID, id);
    				map.put(TAG_ORDER_UNIQUE_ID, orderid);
    				map.put(TAG_ORDER_DATE, orderdate);
    				map.put(TAG_AMOUNT, orderamount);
    				map.put(TAG_PAYMENT_METHOD, paymentmethod);
    				map.put(TAG_ITEMS, ordernoitems);
    				
    				oslist.add(map);
    				
    				
    			

    				
                   }
    				
    				
    				
    	        
    ListAdapter adapter = new SimpleAdapter(
    	ManageOrders.this, oslist,
            R.layout.manage_orders_list_item, new String[] {
    			TAG_ORDER_UNIQUE_ID,TAG_ORDER_DATE,TAG_AMOUNT,TAG_PAYMENT_METHOD,TAG_ITEMS},
            new int[] {  R.id.orderid, R.id.orderdate, R.id.amount, R.id.paymentmethod, R.id.items}){
        @Override
    public View getView(int pos, View convertView, ViewGroup parent){
        View v = convertView;
        if(v== null){
            LayoutInflater vi = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v=vi.inflate(R.layout.manage_orders_list_item, null);
        }
        orderid = (TextView)v.findViewById(R.id.orderid);
        orderid.setText(oslist.get(pos).get(TAG_ORDER_UNIQUE_ID));
       
        
        TextView tvs = (TextView)v.findViewById(R.id.orderdate);
        tvs.setText(oslist.get(pos).get(TAG_ORDER_DATE));
      
        
        TextView amountt = (TextView)v.findViewById(R.id.amount);
        amountt.setText("₹"+oslist.get(pos).get(TAG_AMOUNT));
       
        
        TextView paymentmethod = (TextView)v.findViewById(R.id.paymentmethod);
        paymentmethod.setText(oslist.get(pos).get(TAG_PAYMENT_METHOD));
        
        
        TextView items = (TextView)v.findViewById(R.id.items);
        items.setText(oslist.get(pos).get(TAG_ITEMS));
     
      
        return v;
    }
   
       
};
    
// updating listview
listView.setAdapter(adapter);
   
	
   
   
    //Toast.makeText(getApplicationContext(), "Total number of Items are:" + adapter.getCount() , Toast.LENGTH_LONG).show();

    		 }
    		 
    		 
    		
    		 
    		 
    		 
                             
    		 catch (JSONException ignored) {
      			 
      			 
      		 }
      			if (c == null) {
      				
      			  bg.setVisibility(View.VISIBLE);
      			ebg.setVisibility(View.VISIBLE);
      			 
      			}
      			else {
      				
      				bg.setVisibility(View.GONE);
	      			ebg.setVisibility(View.GONE);
      				
      				
      			}

     		

     		 
     	 
    	 }

			 
                             
                             
    }
    
   
   
    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
     public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case R.id.action_settings: 
           
        	 Intent profile = new Intent(getApplicationContext(),ECart.class);
             startActivity(profile);
             finish();
            return true;
            
        case R.id.notification: 
            
          	 Intent notification = new Intent(getApplicationContext(),DiscoverNotification.class);
               startActivity(notification);
               finish();
              return true; 
        
      
              
        default:
            return super.onOptionsItemSelected(item);
        
 
        }
     } 
     public boolean onPrepareOptionsMenu(Menu paramMenu) {
 		mToolbarMenu = paramMenu;
 		createCartBadge(10);
 		return super.onPrepareOptionsMenu(paramMenu);
 	}
     private void createCartBadge(int paramInt) {
 		/*if (Build.VERSION.SDK_INT <= 15) {
 			return;
 		}*/
 		MenuItem cartItem = this.mToolbarMenu.findItem(R.id.action_settings);
 		LayerDrawable localLayerDrawable = (LayerDrawable) cartItem.getIcon();
 		Drawable cartBadgeDrawable = localLayerDrawable
 				.findDrawableByLayerId(R.id.ic_badge);
 		BadgeDrawable badgeDrawable;
 		if ((cartBadgeDrawable != null)
 				&& ((cartBadgeDrawable instanceof BadgeDrawable))
 				&& (paramInt < 10)) {
 			badgeDrawable = (BadgeDrawable) cartBadgeDrawable;
 		} else {
 			badgeDrawable = new BadgeDrawable(this);
 		}
 		int counttwo=3;
 		badgeDrawable.setCount(coot);
 		localLayerDrawable.mutate();
 		localLayerDrawable.setDrawableByLayerId(R.id.ic_badge, badgeDrawable);
 		cartItem.setIcon(localLayerDrawable);
 	}



	
  
  
    
}
