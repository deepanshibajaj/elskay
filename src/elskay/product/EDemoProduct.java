package elskay.product;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.squareup.picasso.Picasso;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import elskay.app.DJSONParserPost;
import elskay.app.GridViewWithHeaderAndFooter;
import elskay.app.R;


public class EDemoProduct extends Fragment {
	
	ArrayList<EProductList> actorsList;
	
	EProductAdapter adapter;
	
	private static String url = "http://128.199.176.163/elskayapp/getproducts.php";
	
   	private static final String TAG_OS = "product";
    private static final String TAG_NAME = "name";
    private static final String TAG_DESC = "description";
    private static final String TAG_IMAGE = "image1";
    private static final String TAG_PRICE = "price";
    private static final String TAG_PRODUCTID = "product_id";
    private static final String TAG_PRODUCTLIKECOUNT = "likecount";

	
	String pid;
	GridView grid;
	
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

	/*@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.product_list);*/
	
	 public View onCreateView(LayoutInflater inflater, ViewGroup container,
             Bundle savedInstanceState) {
        View vie = inflater.inflate(R.layout.category_grid, container, false);
		
		 /*Intent intent = getActivity().getIntent();
	        Bundle bundle = intent.getExtras();
	        pid= bundle.getString("pathid"); 
	        Log.i("pid from category@@@@@@@",pid);*/
	        
	        Bundle bundle = getArguments();
            pid = bundle.getString("pathid");
            String categoryimage=bundle.getString("catimage");
		
		actorsList = new ArrayList<EProductList>();
		
		
		if(isNetworkAvailable()){
            // do network operation here     
			new JSONAsyncTask().execute();
         }else{
            Toast.makeText(getContext(), "No Available Network. Please try again later", Toast.LENGTH_LONG).show();
          
         }
		//new JSONAsyncTask().execute();
		
		
		
	
		/*ImageView catsetimage=(ImageView)vie.findViewById(R.id.catimage);
		 Picasso.with(getContext())
          .load(categoryimage)
          .fit()
          .centerCrop()
          .into(catsetimage); */
		
		//ListView listview = (ListView)vie.findViewById(R.id.list);
		//grid=(GridView)vie.findViewById(R.id.grid);
		//adapter = new EProductAdapter(getActivity(), R.layout.product_list_item, actorsList);
		
		//listview.setAdapter(adapter);
		//grid.setAdapter(adapter);
		
		GridViewWithHeaderAndFooter grid = (GridViewWithHeaderAndFooter) vie.findViewById(R.id.grid_view);
        adapter = new EProductAdapter(getActivity(), R.layout.product_list_item, actorsList);
		
		//listview.setAdapter(adapter);
       
		

		LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
		View headerView = layoutInflater.inflate(R.layout.header_grid, null);
		
		ImageView catsetimage=(ImageView)headerView.findViewById(R.id.catimage);
		 Picasso.with(getContext())
         .load(categoryimage)
         .fit()
         .centerCrop()
         .into(catsetimage);
		
		grid.addHeaderView(headerView);
		
		 grid.setAdapter(adapter);
		
		
		
		
		
        // on seleting single product
        // launching pdfviewer activity
		grid.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                    int position, long id) {
              
            	String productid = actorsList.get(position).getcatid();
            	String productdesc = actorsList.get(position).getDescription();
            	String productprice = actorsList.get(position).getPrice();
            	String productimage = actorsList.get(position).getImage();
            	String productname = actorsList.get(position).getName();
                // Starting new intent
                Intent in = new Intent(getActivity(),
                        EProductDesc.class);
                // sending pid to next activity
                  in.putExtra("productid", productid);
                  in.putExtra("productdesc", productdesc);
                  in.putExtra("productprice", productprice);
                  in.putExtra("productimage", productimage);
                  in.putExtra("productname", productname);
                //Log.i("Path id in notice activity@@@@@@@",pid);
                // starting new activity and expecting some response back
                startActivity(in);
           }
        }); 
		  return vie;
	}


	class JSONAsyncTask extends AsyncTask<String, String, JSONObject> {
		
		 private ProgressDialog pDialog;
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			  pDialog = new ProgressDialog(getActivity());
	            pDialog.setMessage("Trying to intrigue you more than ever");
	            pDialog.setIndeterminate(false);
	            pDialog.setCancelable(true);
	            pDialog.show(); 
		}
		
		@Override
		 protected JSONObject doInBackground(String... args)  {
			
				
				
				DJSONParserPost jParser = new DJSONParserPost();
	    	   List<NameValuePair> params = new ArrayList<NameValuePair>();
	    	   Log.i("params@@@@@@@",pid);
	  	       params.add(new BasicNameValuePair("pid", pid));
	  		   JSONObject json = jParser.getJSONFromUrl(url,params);
	    		return json;

		}
		
		
		@Override
        protected void onPostExecute(final JSONObject json) {
			pDialog.dismiss();
			adapter.notifyDataSetChanged();
	    		 try {
				
					
					JSONArray jarray = json.getJSONArray(TAG_OS);
					
					for (int i = 0; i < jarray.length(); i++) {
						JSONObject object = jarray.getJSONObject(i);
					
						EProductList actor = new EProductList();
						
						actor.setName(object.getString(TAG_NAME));
						actor.setDescription(object.getString(TAG_DESC));
						actor.setImage(object.getString(TAG_IMAGE));
						actor.setcatid(object.getString(TAG_PRODUCTID));
						actor.setPrice(object.getString(TAG_PRICE));
						actor.setlikecount(object.getString(TAG_PRODUCTLIKECOUNT));
					    String productlike =object.getString(TAG_PRODUCTLIKECOUNT);
					    Log.i("productlike",productlike);
						
						
						 
						
						actorsList.add(actor);
					}
	    		 }
	    		 
					 catch (JSONException e) {
		     			 Toast.makeText(getActivity(), "Sorry ,No Data Found",
		                         Toast.LENGTH_LONG).show();
		     			e.printStackTrace();
		     		}
					 
	    		 }	 
                 
                 
	    }
	    
	 public boolean isNetworkAvailable() {
	       ConnectivityManager connectivityManager = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
	       NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	       return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	   }
		     		
	}
	
	
	
	
	
	

