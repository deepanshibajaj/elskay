package elskay.product;

import java.io.InputStream;
import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import elskay.app.R;

public class EProductAdapter extends ArrayAdapter<EProductList> {
	ArrayList<EProductList> actorList;
	LayoutInflater vi;
	int Resource;
	ViewHolder holder;

	public EProductAdapter(Context context, int resource, ArrayList<EProductList> objects) {
		super(context, resource, objects);
		vi = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		Resource = resource;
		actorList = objects;
		
		
		
	}
 
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// convert view = design
		View v = convertView;
		

				

		
		if (v == null) {
			holder = new ViewHolder();
			v = vi.inflate(Resource, null);
			holder.imageview = (ImageView) v.findViewById(R.id.imageVi);
			holder.tvName = (TextView) v.findViewById(R.id.hashtaguser);
			holder.tvDescription = (TextView) v.findViewById(R.id.pname);
			holder.productprice = (TextView) v.findViewById(R.id.pprice);
			holder.productlike = (TextView) v.findViewById(R.id.likecountt);
			
			v.setTag(holder);
		} else {
			holder = (ViewHolder) v.getTag();
		}
		holder.imageview.setImageResource(R.drawable.ic_launcher);
		//new DownloadImageTask(holder.imageview).execute(actorList.get(position).getImage());
		  Picasso.with(getContext())
          .load(actorList.get(position).getImage())
          .fit()
          .centerCrop()
          .into(holder.imageview);
		holder.tvName.setText(actorList.get(position).getDescription());
		Typeface font2 = Typeface.createFromAsset(vi.getContext().getAssets(),
		        "AvenirNextLTPro-Regular.otf");
		holder.tvName.setTypeface(font2); 
		holder.tvDescription.setText(actorList.get(position).getName());
		Typeface font = Typeface.createFromAsset(vi.getContext().getAssets(),
		        "AvenirNextLTPro-Regular.otf");
		holder.tvDescription.setTypeface(font); 
		
		holder.productprice.setText("₹"+actorList.get(position).getPrice());
		
		holder.productlike.setText(actorList.get(position).getlikecount());
	 
		return v;

	}

	static class ViewHolder {
		public TextView productprice;
		public ImageView imageview;
		public TextView tvName;
		public TextView tvDescription;
		public TextView productlike;

	}

	private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
		ImageView bmImage;

		public DownloadImageTask(ImageView bmImage) {
			this.bmImage = bmImage;
		}

		protected Bitmap doInBackground(String... urls) {
			String urldisplay = urls[0];
			Bitmap mIcon11 = null;
			try {
				InputStream in = new java.net.URL(urldisplay).openStream();
				mIcon11 = BitmapFactory.decodeStream(in);
			} catch (Exception e) {
				Log.e("Error", e.getMessage());
				e.printStackTrace();
			}
			return mIcon11;
		}

		protected void onPostExecute(Bitmap result) {
			bmImage.setImageBitmap(result);
		}

	}
	

}