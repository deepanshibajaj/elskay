package elskay.product;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import elskay.addtocart.ECart;
import elskay.app.R;
import elskay.login.DatabaseHandler;

public class EProductDesc extends Activity {
	
	 String productid;
	 String productname;
	 String productprice;
	 String productimage;
	 String productdesc;
	 String uid;
	 String uname;
	 
	 StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.anim_slide_in_left, 0);
        requestWindowFeature(Window.FEATURE_NO_TITLE); 
        
        setContentView(R.layout.product_desc);
        
        
        DatabaseHandler db = new DatabaseHandler(getApplicationContext());
        HashMap<String,String> user = new HashMap<String, String>();
        user = db.getUserDetails();
        uid= user.get("uid");
        uname= user.get("fname");
 
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        productid= bundle.getString("productid"); 
        productname= bundle.getString("productname");
        productprice= bundle.getString("productprice");
        productimage=bundle.getString("productimage");
        productdesc =bundle.getString("productdesc");
        
        
        
        TextView pprice = (TextView)findViewById(R.id.productprice);
        pprice.setText("INR"+" "+productprice);
        
        TextView pname = (TextView)findViewById(R.id.productname);
        pname.setText(productname);
        Typeface font = Typeface.createFromAsset(getAssets(),
		        "AvenirNextLTPro-Demi.otf");
        pname.setTypeface(font); 
        
        
       /* ImageView imageheart = (ImageView) findViewById(R.id.imageheart);
        Animation pulse = AnimationUtils.loadAnimation(this, R.anim.heartbounce);
        imageheart.startAnimation(pulse);*/
        
       Button loveit = (Button)findViewById(R.id.Butnlikeproduct);
        
       loveit.setOnClickListener(new OnClickListener()
		{

		@Override
		public void onClick(View v) {
			
			ImageView imageheart = (ImageView) findViewById(R.id.imageheart);
			imageheart.setVisibility(View.VISIBLE);
	        Animation pulse = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.heartbounce);
	        imageheart.startAnimation(pulse);
	        imageheart.setVisibility(View.INVISIBLE);
	        
	        
	        new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
       	
                  	   	 
                  	 
    	   	String result = null;
    	   	InputStream is = null;
    	  
    	  
	        
    	  
    	
    
    	   	ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

    	 
			
    	   	nameValuePairs.add(new BasicNameValuePair("productid",productid));
    	  nameValuePairs.add(new BasicNameValuePair("userid",uid));
    	 nameValuePairs.add(new BasicNameValuePair("username",uname));
    	
    	
    	   
  

    	   	StrictMode.setThreadPolicy(policy); 


	//http post
	try{
	        HttpClient httpclient = new DefaultHttpClient();
	        HttpPost httppost = new HttpPost("http://128.199.176.163/elskayapp/elskayshop/insert_productlikes.php");
	        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
	        HttpResponse response = httpclient.execute(httppost); 
	        HttpEntity entity = response.getEntity();
	        is = entity.getContent();

	        Log.e("log_tag", "connection success ");
	       
	        //Toast.makeText(getApplicationContext(), "", Toast.LENGTH_SHORT).show();
	   }
                   
   
	
	catch(Exception e)
	{
	        Log.e("log_tag", "Error in http connection "+e.toString());
	        Toast.makeText(getApplicationContext(), "Connection fail", Toast.LENGTH_SHORT).show();

	}
	//convert response to string
	try{
	        BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
	        StringBuilder sb = new StringBuilder();
	        String line = null;
	        while ((line = reader.readLine()) != null) 
	        {
	                sb.append(line + "\n");
	             
	           
	             
	        }
	        is.close();

	        result=sb.toString();
	      Log.i("response##########",result);
	    Toast.makeText(getApplicationContext(),result,Toast.LENGTH_LONG).show();
	      
   
	}
	catch(Exception e)
	{
	       Log.e("log_tag", "Error converting result "+e.toString());
   	}


	try{
		
					JSONObject json_data = new JSONObject(result);

	                CharSequence w= (CharSequence) json_data.get("re");
	             
	                Toast.makeText(getApplicationContext(), w, Toast.LENGTH_SHORT).show();
	                

	      
	     }
	catch(JSONException e)
	   {
	        Log.e("log_tag", "Error parsing data "+e.toString());
	        Toast.makeText(getApplicationContext(), "JsonArray fail", Toast.LENGTH_SHORT).show();
	    }
	 Thread.sleep(3000); // Let's wait for some time
                    } catch (Exception e) {
                         
                    }
                    //progress.dismiss();
                  
                   
                }
            }).start(); 
             

		}


		}); 
        
        
        ImageView productimg = (ImageView)findViewById(R.id.productimage);
        Picasso.with(getApplicationContext())
        .load(productimage)
        .into(productimg);
        
        Button like =  (Button)findViewById(R.id.Butnlikeproduct);
        Typeface font3 = Typeface.createFromAsset(getAssets(),
		        "AvenirNextLTPro-Regular.otf");
        like.setTypeface(font3);
        
        Button productidd =  (Button)findViewById(R.id.buybutton);
        productidd.setTag(productid);
        Typeface font2 = Typeface.createFromAsset(getAssets(),
		        "AvenirNextLTPro-Regular.otf");
        productidd.setTypeface(font2);
        
        
        productidd.setOnClickListener(new View.OnClickListener()
        { 
        	 ProgressDialog progress2;
        	 
               public void onClick(View view)
                 {
            	   
            	  progress2 = new ProgressDialog(EProductDesc.this);
            	  progress2.setMessage("Its intense… taking time to grasp in");
             	  progress2.show();
                  progress2.setCancelable(true);
                 
                   new Thread(new Runnable() {
                       @Override
                       public void run() {
                           try {
      
            	   	String result= null;
            	   	InputStream is = null;
            	   
            	   	ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

            	   	nameValuePairs.add(new BasicNameValuePair("productid",productid));
            		nameValuePairs.add(new BasicNameValuePair("userid",uid));
             	 
            	   	StrictMode.setThreadPolicy(policy); 
            	
        	//http post
        	try{
        	        HttpClient httpclient = new DefaultHttpClient();
        	        HttpPost httppost = new HttpPost("http://128.199.176.163/elskayapp/elskayshop/product_addtocart.php");
        	        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
        	        HttpResponse response = httpclient.execute(httppost); 
        	        HttpEntity entity = response.getEntity();
        	        is = entity.getContent();

        	        Log.e("log_tag", "connection success ");
        	        //Toast.makeText(getApplicationContext(), "", Toast.LENGTH_SHORT).show();
        	   }
        	
        	
        	catch(Exception e)
        	{
        	        Log.e("log_tag", "Error in http connection "+e.toString());
        	        Toast.makeText(getApplicationContext(), "Connection fail", Toast.LENGTH_SHORT).show();

        	}
        	//convert response to string
        	try{
        	        BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
        	        StringBuilder sb = new StringBuilder();
        	        String line = null;
        	        while ((line = reader.readLine()) != null) 
        	        {
        	                sb.append(line + "\n");
        	       	        Intent i = new Intent(getBaseContext(),ECart.class);
        	                startActivity(i);
        	                
        	                
        	        }
        	        is.close();

        	        result=sb.toString();
        	}
        	catch(Exception e)
        	{
        	       Log.e("log_tag", "Error converting result "+e.toString());
           	}

   
        	try{
        		
        					JSONObject json_data = new JSONObject(result);

        	                CharSequence w= (CharSequence) json_data.get("re");
        	              
        	                Toast.makeText(getApplicationContext(), w, Toast.LENGTH_SHORT).show();

        	      
        	     }
        	catch(JSONException e)
        	   {
        	        Log.e("log_tag", "Error parsing data "+e.toString());
        	        Toast.makeText(getApplicationContext(), "Oopss!! Our engineer fell asleep,Please try again Later", Toast.LENGTH_SHORT).show();
        	    }
        	 Thread.sleep(3000); // Let's wait for some time
                           } catch (Exception e) {
                                
                           }
                           progress2.dismiss();
                          // bar.setVisibility(View.GONE);
                       }
                   }).start();
               }
           }); 
        
        
       
  	  
        
       
        ImageButton back = (ImageButton)findViewById(R.id.backbutn);
        
        back.setOnClickListener(new OnClickListener()
		{

		@Override
		public void onClick(View v) {
			
			finish();

		}


		}); 
        

}
	
	
}