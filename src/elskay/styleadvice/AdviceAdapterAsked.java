package elskay.styleadvice;

import java.io.InputStream;
import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import elskay.app.R;
import elskay.app.RoundedTransformation;
import elskay.feed.CircleTransform;
import elskay.login.PublicProfile;

public class AdviceAdapterAsked extends ArrayAdapter<AdviceList> {
	ArrayList<AdviceList> actorListd;
	
	LayoutInflater vi;
	int Resource;
	ViewHolder holder;
	 String iddd;


	public AdviceAdapterAsked(Context context, int resource, ArrayList<AdviceList> objects) {
		super(context, resource, objects);
		vi = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		Resource = resource;
		actorListd = objects;
		
		
		
	}
 
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// convert view = design
		View v = convertView;
		

				

		
		if (v == null) {
			holder = new ViewHolder();
			v = vi.inflate(Resource, null);
			holder.imageview = (ImageView) v.findViewById(R.id.adviceimage);
			
			
			
			
			holder.tvquestion = (TextView) v.findViewById(R.id.advicequery);
			Typeface custom_font2 = Typeface.createFromAsset(vi.getContext().getAssets(), "AvenirNextLTPro-Regular.otf");
			holder.tvquestion.setTypeface(custom_font2);
			holder.tvqid = (Button) v.findViewById(R.id.advicecomment);
			
			v.setTag(holder);
		} else {
			holder = (ViewHolder) v.getTag();
		}
		holder.imageview.setImageResource(R.drawable.ic_launcher);
		//new DownloadImageTask(holder.imageview).execute(actorList.get(position).getImage());
		  Picasso.with(getContext())
          .load(actorListd.get(position).getImage().replace(" ", "%20"))
          .placeholder( R.drawable.eloader )
          //.transform(new RoundedTransformation(200, 20)) 
          .into(holder.imageview);
		
		holder.tvquestion.setText(actorListd.get(position).getquestion());
		
		 
		 
		 holder.tvqid.setText(actorListd.get(position).getcount()+" "+"Suggestions");
		
		
		holder.tvqid.setOnClickListener(new OnClickListener()
		{

		@Override
		public void onClick(View v) {
			String iddd = actorListd.get(position).getqid();
			String image = actorListd.get(position).getImage();
			String ques = actorListd.get(position).getquestion();
			Intent myIntent = new Intent();
			 myIntent = new Intent(getContext(), AdviceComment.class);
			 myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			 myIntent.putExtra("intVariableName",iddd);
			 myIntent.putExtra("imagefile",image);
			 myIntent.putExtra("question",ques);
			 vi.getContext().startActivity(myIntent);
			 

		}


		});
		
		return v;

	}

	static class ViewHolder {
		public Button tvqid;
		public ImageView imageview;
		
		public TextView tvquestion;
		
		public TextView CountComment;
	}

	private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
		ImageView bmImage;

		public DownloadImageTask(ImageView bmImage) {
			this.bmImage = bmImage;
		}

		protected Bitmap doInBackground(String... urls) {
			String urldisplay = urls[0];
			Bitmap mIcon11 = null;
			try {
				InputStream in = new java.net.URL(urldisplay).openStream();
				mIcon11 = BitmapFactory.decodeStream(in);
			} catch (Exception e) {
				Log.e("Error", e.getMessage());
				e.printStackTrace();
			}
			return mIcon11;
		}

		protected void onPostExecute(Bitmap result) {
			bmImage.setImageBitmap(result);
		}

	}
	

}