package elskay.styleadvice;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import elskay.app.MainTabActivity;
import elskay.app.R;
import elskay.login.DatabaseHandler;



 public class AdviceAsk extends Activity {

	Button ab;
	String encodedString;
	String imgPath, fileName;
	Bitmap bitmap;
	ImageView imgView;
	String uid;
	String username;
	TextView txtView;
	

	
	HashMap<String,String> user;
	ImageButton cd;
	
	private static int RESULT_LOAD_IMG = 1;

	EditText tx;
//  create a textWatcher member
private TextWatcher mTextWatcher = new TextWatcher() {
    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    @Override
    public void afterTextChanged(Editable editable) {
        // check Fields For Empty Values
        checkFieldsForEmptyValues();
        
    }
};

void checkFieldsForEmptyValues(){
	Button b = (Button) findViewById(R.id.butnquesss);
    
    String s1 = tx.getText().toString();
    

    if(s1.equals("")){
    	
        b.setEnabled(false);
        
    } else{
        b.setEnabled(true);
    }
    
   
     
}
	
	StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
	

	
	 @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        requestWindowFeature(Window.FEATURE_NO_TITLE);
	        setContentView(R.layout.advice_post); 
	
	          
	        
	          tx = (EditText)findViewById(R.id.postqu);
	          Typeface custom_font2 = Typeface.createFromAsset(getAssets(), "AvenirNextLTPro-Regular.otf");
	          tx.setTypeface(custom_font2);
	          txtView = (TextView)findViewById(R.id.pickPhoto);
	          Typeface custom_font3 = Typeface.createFromAsset(getAssets(), "AvenirNextLTPro-Regular.otf");
	          txtView.setTypeface(custom_font3);
	          imgView = (ImageView) findViewById(R.id.imgView);
	          
	          
		      int maxLength = 100;
		      InputFilter[] fArray = new InputFilter[1];
		      fArray[0] = new InputFilter.LengthFilter(maxLength);
		      tx.setFilters(fArray);
		      
		      
		      DatabaseHandler db = new DatabaseHandler(getApplicationContext());

		        HashMap<String,String> user = new HashMap<String, String>();
		        user = db.getUserDetails();
		        uid= user.get("uid");
		        username= user.get("fname");
		      
		
		      
		     
		        
		      
		       ab = (Button)findViewById(R.id.butnquesss);
		       Typeface custom_font4 = Typeface.createFromAsset(getAssets(), "AvenirNextLTPro-Regular.otf");
		       ab.setTypeface(custom_font4);
		      ab.bringToFront();
		     
		      
		   // set listeners
		      tx.addTextChangedListener(mTextWatcher);
		    
			

		      // run once to disable if empty
		      //checkFieldsForEmptyValues();
		      
		      
		      //Window window = getWindow();

				// clear FLAG_TRANSLUCENT_STATUS flag:
				//window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

				// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
				//window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

				// finally change the color
				//window.setStatusBarColor(getResources().getColor(R.color.sab_yellow));
		      
		      ImageButton back = (ImageButton)findViewById(R.id.backbutn);
		        
		        back.setOnClickListener(new OnClickListener()
				{

				@Override
				public void onClick(View v) {
					
					finish();

					 

				}


				}); 
		      
		      
		      
		      ImageButton button3 = (ImageButton) findViewById(R.id.buttonLoadPicture);

		        

		      button3.setOnClickListener(new View.OnClickListener()
		        {
		        public void onClick(View view) {
		        	
		        	 // Create intent to Open Image applications like Gallery, Google Photos
		        	selectImage();
		        	txtView.setVisibility(View.GONE);
		       }
		       });
		      
		      
		        

		     
		      
		      tx.setOnKeyListener(new OnKeyListener()
		      {
		          public boolean onKey(View v, int keyCode, KeyEvent event)
		          {
		              if (event.getAction() == KeyEvent.ACTION_DOWN)
		              {
		                  switch (keyCode)
		                  {
		                      case KeyEvent.KEYCODE_DPAD_CENTER:
		                      case KeyEvent.KEYCODE_ENTER:
		                          
		                          return false;
		                      default:
		                          break;
		                  }
		              }
		              return false;
		          }
		      });
		  
		     
		     
		    
		      
		     
		      
		      
	 ab.setOnClickListener(new View.OnClickListener()
     {
		 ProgressDialog progress;
            public void onClick(View view)
            {
            	  
                if (  ( !tx.getText().toString().equals(""))  )
                {
            	 progress = new ProgressDialog(AdviceAsk.this);
            	 progress.setMessage("Please wait! Making some space for a long talk/discussion");
            	 progress.show();
                 progress.setCancelable(true);
                
                 
               
                 new Thread(new Runnable() {
                     @Override
                     public void run() {
                         try {
            	
                        	 
                        	 
         	   	String result = null;
         	   	InputStream is = null;
         	 
         	   
         	  String v2 = tx.getText().toString();
         	  
         	 BitmapFactory.Options options = null;
             options = new BitmapFactory.Options();
             options.inSampleSize=3;
            
             bitmap = BitmapFactory.decodeFile(imgPath,
                     options);
            
            /* int size = (int) ( bitmap .getHeight() * (512.0 / bitmap .getWidth()) );
             bitmap = Bitmap.createScaledBitmap(bitmap,
            		 512, size, false); */
             
            
            /* int photoW = bitmap .getWidth();
             int photoH = bitmap .getHeight();
          
             // Determine how much to scale down the image
             int scaleFactor = Math.min(photoW/2, photoH/2);
          
             // Decode the image file into a Bitmap sized to fill the View
             options.inJustDecodeBounds = false;
             options.inSampleSize = scaleFactor;*/
           
             
             ByteArrayOutputStream stream = new ByteArrayOutputStream();
             // Must compress the Image to reduce image size to make upload easy
             bitmap.compress(Bitmap.CompressFormat.JPEG, 60, stream);
             byte[] byte_arr = stream.toByteArray();
             // Encode Image to String
            encodedString = Base64.encodeToString(byte_arr,0); 
            
         	   	ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
             
         	 
				//	nameValuePairs.add(new BasicNameValuePair("number",v1));
         	   	nameValuePairs.add(new BasicNameValuePair("question",v2));
         	  nameValuePairs.add(new BasicNameValuePair("image", encodedString));
         	 nameValuePairs.add(new BasicNameValuePair("filename", fileName));
         	 	nameValuePairs.add(new BasicNameValuePair("userid",uid));
         	 	nameValuePairs.add(new BasicNameValuePair("username",username));
         	 Log.i("question@@",v2);
         	Log.i("image@@",encodedString);
         	Log.i("filename@@",fileName);

         	   	StrictMode.setThreadPolicy(policy); 


     	//http post
     	try{
     	        HttpClient httpclient = new DefaultHttpClient();
     	        HttpPost httppost = new HttpPost("http://128.199.176.163/elskayapp/elskayadvice/eadvicepost.php");
     	        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
     	       // httppost.setEntity(new StringEntity(jsonString, "UTF8"));
     	        HttpResponse response = httpclient.execute(httppost); 
     	        HttpEntity entity = response.getEntity();
     	        is = entity.getContent();

     	        Log.e("log_tag", "connection success ");
     	       
     	        //Toast.makeText(getApplicationContext(), "", Toast.LENGTH_SHORT).show();
     	   }
                        
        
     	
     	catch(Exception e)
     	{
     	        Log.e("log_tag", "Error in http connection "+e.toString());
     	        Toast.makeText(getApplicationContext(), "Connection fail", Toast.LENGTH_SHORT).show();

     	}
     	//convert response to string
     	try{
     	        BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
     	        StringBuilder sb = new StringBuilder();
     	        String line = null;
     	        while ((line = reader.readLine()) != null) 
     	        {
     	                sb.append(line + "\n");
     	               Intent i = new Intent(getBaseContext(),MainTabActivity.class);
     	              Bundle b=new Bundle();
     	             b.putString("condition","TALK");
     	             i.putExtras(b);
   	                startActivity(i);
   	             
     	        }
     	        is.close();

     	        result=sb.toString();
     	}
     	catch(Exception e)
     	{
     	       Log.e("log_tag", "Error converting result "+e.toString());
        	}


     	try{
     		
     					JSONObject json_data = new JSONObject(result);

     	                CharSequence w= (CharSequence) json_data.get("re");
     	              
     	                Toast.makeText(getApplicationContext(), w, Toast.LENGTH_SHORT).show();

     	      
     	     }
     	catch(JSONException e)
     	   {
     	        Log.e("log_tag", "Error parsing data "+e.toString());
     	        Toast.makeText(getApplicationContext(), "Oopss!! Our engineer fell asleep,Please try again Later", Toast.LENGTH_SHORT).show();
     	    }
     	 Thread.sleep(3000); // Let's wait for some time
                         } catch (Exception e) {
                              
                         }
                         progress.dismiss();
                     }
                     
                 }).start();
                }
                 else if ( ( tx.getText().toString().equals("")) )
                 {  tx.setError("Oops ! you left this out");
                     
                 }

                 else
                 {
                     Toast.makeText(getApplicationContext(),
                             "Description or Location field is empty", Toast.LENGTH_SHORT).show();
                 }
            
             }
         });
	 

	 }
    
	/* public void loadImagefromGallery(View view) {
		    // Create intent to Open Image applications like Gallery, Google Photos
		    Intent galleryIntent = new Intent(Intent.ACTION_PICK,
		            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		    // Start the Intent
		    startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
		} */

	 
	 public void selectImage() {
		 
	        final CharSequence[] options = {"Choose from Gallery","Cancel"};
	 
	        AlertDialog.Builder builder = new AlertDialog.Builder(AdviceAsk.this);
	        builder.setTitle("Add Photo!");
	        builder.setItems(options, new DialogInterface.OnClickListener() {
	            @Override
	            public void onClick(DialogInterface dialog, int item) {
	            	
	            	  if (options[item].equals("Choose from Gallery"))
	                {
	                    Intent intent = new   Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
	                    startActivityForResult(intent, 1);
	 
	                }
	                else if (options[item].equals("Cancel")) {
	                    dialog.dismiss();
	                }
	            }
	        });
	        builder.show();
	    }

		
	 @Override
		protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		    super.onActivityResult(requestCode, resultCode, data);

		    try {
		        // When an Image is picked
		        if (requestCode == RESULT_LOAD_IMG && resultCode == Activity.RESULT_OK
		               ) {
		            // Get the Image from data
		        	 

		            Uri selectedImage = data.getData();
		            String[] filePathColumn = {MediaStore.Images.Media.DATA};

		            // Get the cursor
		            Cursor cursor = getContentResolver().query(selectedImage,
		                    filePathColumn, null, null, null);
		            // Move to first row
		            cursor.moveToFirst();

		            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
		            imgPath = cursor.getString(columnIndex);
		            cursor.close();
		            
		            // Set the Image in ImageView after decoding the String
		            imgView.setImageBitmap(BitmapFactory
		                    .decodeFile(imgPath));
		            // Get the Image's file name
		            String fileNameSegments[] = imgPath.split("/");
		            fileName = fileNameSegments[fileNameSegments.length - 1];

		        } else {
		            Toast.makeText(this, "You haven't picked Image",
		                    Toast.LENGTH_LONG).show();
		        }
		    } catch (Exception e) {
		        Toast.makeText(this, "Oopss!! Our engineer fell asleep,Please try again Later", Toast.LENGTH_LONG)
		                .show();
		    }

	 }
	
	 
		
}
            