package elskay.styleadvice;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.ParseException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import elskay.app.DJSONParserPost;
import elskay.app.GridViewWithHeaderAndFooter;
import elskay.app.R;
import elskay.login.DatabaseHandler;
import elskay.product.EProductList;


public class AdviceAsked extends Fragment {
	
	ArrayList<AdviceList> actorsListd;
	
	AdviceAdapterAsked adapter;
	private SwipeRefreshLayout swipeRefreshLayout;
	ListView listview;
	long lastStoredTimeStamp = 0;
	private AnimationDrawable progressAnimation;
	private Button showDialog;
	String uid;
	JSONObject object;
	ImageView bg;
	TextView ebg;
	Button askadvice;
	private static String url = "http://128.199.176.163/elskayapp/elskayadvice/get_useradvicedques.php";
	
	 @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	    }
	
	
	 public View onCreateView(LayoutInflater inflater, ViewGroup container,
             Bundle savedInstanceState) {
        View vi = inflater.inflate(R.layout.advice_asked_list, container, false);
		
      
        bg = (ImageView)vi.findViewById(R.id.bgadviced);
        ebg = (TextView)vi.findViewById(R.id.bgtextadviced);
        Typeface custom_font = Typeface.createFromAsset(getActivity().getAssets(), "AvenirNextLTPro-Regular.otf");
        ebg.setTypeface(custom_font);
        askadvice=(Button)vi.findViewById(R.id.askadviced);
        Typeface custom_font2 = Typeface.createFromAsset(getActivity().getAssets(), "AvenirNextLTPro-Regular.otf");
        askadvice.setTypeface(custom_font2);
        
        
        DatabaseHandler db = new DatabaseHandler(getActivity().getApplicationContext());

        HashMap<String,String> user = new HashMap<String, String>();
        user = db.getUserDetails();
        uid= user.get("uid");
		
        actorsListd = new ArrayList<AdviceList>();
		
		if(isNetworkAvailable()){
                
			new JSONAsyncTask().execute();
         }else{
            Toast.makeText(getContext(), "No Available Network. Please try again later", Toast.LENGTH_LONG).show();
          
         }
		
		ListView listview = (ListView)vi.findViewById(android.R.id.list);
		adapter = new AdviceAdapterAsked(getActivity(), R.layout.advice_asked_list_item, actorsListd);
		listview.setAdapter(adapter);
		
		
        
        
		askadvice.setOnClickListener(new View.OnClickListener()
	    {
	    public void onClick(View view) {
	    	
	    	Intent intent = new Intent(getActivity(), AdviceAsk.class);
	    	 startActivity(intent);
	   }
	   }); 
	
	    
		
	       
	        
	        return vi;
	}
	
	   
 
	
	class JSONAsyncTask extends AsyncTask<String, Void, JSONObject> {
		
		ProgressDialog dialog;
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			//swipeRefreshLayout.setRefreshing(true);
			dialog = new ProgressDialog(getActivity());
			dialog.setMessage("Please give me a moment to open the box of treasure..");
			//dialog.setTitle("Connecting server");
			dialog.show();
			dialog.setCancelable(false);
			
			
		}
		
		
		protected JSONObject doInBackground(String... args)  {
			
				DJSONParserPost jParser = new DJSONParserPost();
		    	   List<NameValuePair> params = new ArrayList<NameValuePair>();
		    	   Log.i("params@@@@@@@",uid);
		  	       params.add(new BasicNameValuePair("uid", uid));
		  		   JSONObject json = jParser.getJSONFromUrl(url,params);
		    		return json;
		     }
				
		
		
	   protected void onPostExecute(final JSONObject json) {
			dialog.dismiss();
			
			 adapter.notifyDataSetChanged();
	    		 try {
				
					
						JSONArray jarray = json.getJSONArray("adviced");
					
					for (int i = 0; i < jarray.length(); i++) {
						object = jarray.getJSONObject(i);

						AdviceList actor = new AdviceList();
						actor.setqid(object.getString("qid"));
					    actor.setquestion(object.getString("advicequestion"));
						actor.setImage(object.getString("adviceimage"));
						actor.setusername(object.getString("username"));
						actor.setuserid(object.getString("unique_id"));
						actor.setprofileimage(object.getString("profileimage"));
						actor.setcount(object.getString("count"));
						
						
						actorsListd.add(actor);
						
						
											}
	    		 }
	    		 
                catch (JSONException ignored) {
	      			 
	      			 
	      		 }
	      			if (object == null) {
	      				askadvice.setVisibility(View.VISIBLE);
	      			  bg.setVisibility(View.VISIBLE);
	      			ebg.setVisibility(View.VISIBLE);
	      			 
	      			}
	      			else {
	      				askadvice.setVisibility(View.GONE);
	      				bg.setVisibility(View.GONE);
		      			ebg.setVisibility(View.GONE);
	      				
	      				
	      			}

					 
	    		 }	
}
	
	
	
	
	 public boolean isNetworkAvailable() {
	       ConnectivityManager connectivityManager = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
	       NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	       return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	   }
	
	}
	
	

	
	
	

