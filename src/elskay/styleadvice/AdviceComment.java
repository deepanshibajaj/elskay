package elskay.styleadvice;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import elskay.app.DJSONParserPost;
import elskay.app.R;
import elskay.login.DatabaseHandler;


 
public class AdviceComment extends ListActivity {
 
	EditText tx;
	ImageButton button;
	ImageView img;

	
//  create a textWatcher member
private TextWatcher mTextWatcher = new TextWatcher() {
    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    @Override
    public void afterTextChanged(Editable editable) {
        // check Fields For Empty Values
        checkFieldsForEmptyValues();
        
    }
};

void checkFieldsForEmptyValues(){
    ImageButton b = (ImageButton) findViewById(R.id.Buttntick);
    
    String s1 = tx.getText().toString();
    

    if(s1.equals("")){
    	
        b.setEnabled(false);
        
    } else {
        b.setEnabled(true);
    }
}
	
 
    // Progress Dialog
    private ProgressDialog pDialog;
    private ProgressBar bar;
    
    
    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
    // JSON parser class
    DJSONParserPost jsonParser = new DJSONParserPost();  
    List<HashMap<String, String>> oslist = new ArrayList<HashMap<String, String>>();
    private static String url_post_comment = "http://128.199.176.163/elskayapp/elskayadvice/get_advicecomment.php";

   	private static final String TAG_OS = "recomments";
      private static final String TAG_COMMENT = "comment";
      private static final String TAG_USERNAME = "uname";
  
      
   	JSONArray recomments = null;

	private String qqid;
	
     int pidd;
	 String pid ;
	 String us;
	 String uid;
     String username;
	 String imgcomment;
	 String adviceques;
	 ListView listView;
	 TextView ques;
	
	
	 HashMap<String,String> user;
	 HashMap<String, String> map2 = new HashMap<String, String>();
 
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.anim_slide_in_left, 0);
        requestWindowFeature(Window.FEATURE_NO_TITLE); 
        
        setContentView(R.layout.advice_comment);
        
 
         Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        pid= bundle.getString("intVariableName"); 
        imgcomment=bundle.getString("imagefile");
        adviceques =bundle.getString("question");
        
        
     
        
        
        ImageButton back = (ImageButton)findViewById(R.id.backbutn);
        
        back.setOnClickListener(new OnClickListener()
		{

		@Override
		public void onClick(View v) {
			
			finish();

			 

		}


		}); 
        

         listView = (ListView) findViewById(android.R.id.list);
        
        
       /* LayoutInflater inflater = getLayoutInflater();
         ViewGroup header = (ViewGroup) inflater.inflate(R.layout.header, listView,
                 false);
         img = (ImageView)header.findViewById(R.id.cimage);
         ques = (TextView)header.findViewById(R.id.ctext);
         ques.setText(adviceques);
         Typeface custom_font3 = Typeface.createFromAsset(getAssets(),
       	      "Roboto-Regular.ttf");
         ques.setTypeface(custom_font3);
         Picasso.with(getApplicationContext())
         .load(imgcomment)
         .into(img);
         listView.addHeaderView(header, null, false); */
          
         
		
		  tx = (EditText)findViewById(R.id.postcomment);
		 tx.setOnFocusChangeListener(new View.OnFocusChangeListener() {

			    @Override
			    public void onFocusChange(View v, boolean hasFocus) {
			    	button.setVisibility(View.VISIBLE);
			    }
			    
			    	
			});
		 
		 
	      Typeface custom_font = Typeface.createFromAsset(getAssets(),
	      "AvenirNextLTPro-Regular.otf");
	      tx.setTypeface(custom_font);
	      
	      
	      
	   // set listeners
	      tx.addTextChangedListener(mTextWatcher);
	      
	      //bar = (ProgressBar) this.findViewById(R.id.progressBar);
	      

	      // run once to disable if empty
	      checkFieldsForEmptyValues();
	      
	      DatabaseHandler db = new DatabaseHandler(getApplicationContext());

	        HashMap<String,String> user = new HashMap<String, String>();
	        user = db.getUserDetails();
	        uid= user.get("uid");
	        username= user.get("fname");
	      
	        
	       
	        
		
		
 
        // Getting complete product details in background thread
	        if(isNetworkAvailable()){
	            // do network operation here     
	      	   new JSONParses().execute();
	         }else{
	            Toast.makeText(this, "No Available Network. Please try again later", Toast.LENGTH_LONG).show();
	            return;
	         }
        
        oslist = new ArrayList<HashMap<String, String>>();
        
        
        button = (ImageButton) findViewById(R.id.Buttntick);
        button.setOnClickListener(new View.OnClickListener()
        { 
        	 ProgressDialog progress2;
        	 
               public void onClick(View view)
                 {
            	   
            	  progress2 = new ProgressDialog(AdviceComment.this);
            	  progress2.setMessage("Its intense… taking time to grasp in");
             	  progress2.show();
                  progress2.setCancelable(true);
                   //bar.setVisibility(View.VISIBLE);
                  
                  
                   new Thread(new Runnable() {
                       @Override
                       public void run() {
                           try {
      
            	   	String result= null;
            	   	InputStream is = null;
            	   //	EditText editText = (EditText)findViewById(R.id.num);
            	   //	String v1 = editText.getText().toString();
            	   	
            	   	//EditText te = (EditText)findViewById(R.id.postcomment);
            	   	String c2 = tx.getText().toString();
            	   //	EditText editText2 = (EditText)findViewById(R.id.e3);
            	   //	String v3 = editText2.getText().toString(); 
            	   	
            	   
            	   	ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

            	   //	nameValuePairs.add(new BasicNameValuePair("number",v1));
            	   	nameValuePairs.add(new BasicNameValuePair("comment",c2));
            		nameValuePairs.add(new BasicNameValuePair("qid",pid));
            		nameValuePairs.add(new BasicNameValuePair("adviceuid",uid));
             	 	nameValuePairs.add(new BasicNameValuePair("adviceuname",username));
            	   //	nameValuePairs.add(new BasicNameValuePair("f3",v3));
            		
            

            	   	StrictMode.setThreadPolicy(policy); 
            	   	


        	//http post
        	try{
        	        HttpClient httpclient = new DefaultHttpClient();
        	        HttpPost httppost = new HttpPost("http://128.199.176.163/elskayapp/elskayadvice/insert_advicecomment.php");
        	        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
        	        HttpResponse response = httpclient.execute(httppost); 
        	        HttpEntity entity = response.getEntity();
        	        is = entity.getContent();

        	        Log.e("log_tag", "connection success ");
        	        //Toast.makeText(getApplicationContext(), "", Toast.LENGTH_SHORT).show();
        	   }
        	
        	
        	catch(Exception e)
        	{
        	        Log.e("log_tag", "Error in http connection "+e.toString());
        	        Toast.makeText(getApplicationContext(), "Connection fail", Toast.LENGTH_SHORT).show();

        	}
        	//convert response to string
        	try{
        	        BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
        	        StringBuilder sb = new StringBuilder();
        	        String line = null;
        	        while ((line = reader.readLine()) != null) 
        	        {
        	                sb.append(line + "\n");
        	       	        //Intent i = new Intent(getBaseContext(),NoticeActivity.class);
        	                //startActivity(i);
        	                
        	                Intent intent = getIntent();
        	                overridePendingTransition(0, 0);
        	                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        	                finish();
        	                overridePendingTransition(0, 0);
        	                startActivity(intent);
        	        }
        	        is.close();

        	        result=sb.toString();
        	}
        	catch(Exception e)
        	{
        	       Log.e("log_tag", "Error converting result "+e.toString());
           	}

   
        	try{
        		
        					JSONObject json_data = new JSONObject(result);

        	                CharSequence w= (CharSequence) json_data.get("re");
        	              
        	                Toast.makeText(getApplicationContext(), w, Toast.LENGTH_SHORT).show();

        	      
        	     }
        	catch(JSONException e)
        	   {
        	        Log.e("log_tag", "Error parsing data "+e.toString());
        	        Toast.makeText(getApplicationContext(), "Oopss!! Our engineer fell asleep,Please try again Later", Toast.LENGTH_SHORT).show();
        	    }
        	 Thread.sleep(3000); // Let's wait for some time
                           } catch (Exception e) {
                                
                           }
                           progress2.dismiss();
                          // bar.setVisibility(View.GONE);
                       }
                   }).start();
               }
           }); 
    }
 

    private class JSONParses extends AsyncTask<String, String, JSONObject> {
    	 private ProgressDialog pDialog;
    	@Override
        protected void onPreExecute() {
            super.onPreExecute();
                			
           pDialog = new ProgressDialog(AdviceComment.this);
            pDialog.setMessage("Loading....");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show(); 
            //bar.setVisibility(View.VISIBLE);
            
            
            
    	}
    	
    	@Override
        protected JSONObject doInBackground(String... args) {
    		
    		DJSONParserPost jParser = new DJSONParserPost();
    		 List<NameValuePair> params = new ArrayList<NameValuePair>();
  	       params.add(new BasicNameValuePair("pid", pid));
  		   JSONObject json = jParser.getJSONFromUrl(url_post_comment,params);
    		return json;
    		
   		    
    	}
    	 
    	
    	@Override
         protected void onPostExecute(final JSONObject json) {
    		
    		//bar.setVisibility(View.GONE);
    		pDialog.dismiss();
    		
    		 try {
    				// Getting JSON Array from URL
    			 recomments = json.getJSONArray(TAG_OS);
    				for(int i = 0; i < recomments.length(); i++){
    				JSONObject c = recomments.getJSONObject(i);
    				
    				// Storing  JSON item in a Variable
    				
    				String comment = c.getString(TAG_COMMENT);
    				String username = c.getString(TAG_USERNAME);

    				
    		        // Adding value HashMap key => value
    				HashMap<String, String> map = new HashMap<String, String>();
    				
    				map.put(TAG_COMMENT, comment);
    				map.put(TAG_USERNAME, username);
    				
    				
    				
    				oslist.add(map);
    				
    				
    				/* final TextView textview=(TextView) findViewById(R.id.countcomment); 
    			        textview.setText(count); */

    				
                   }
    				
    				
    				
    	        
    ListAdapter adapter = new SimpleAdapter(
    		AdviceComment.this, oslist,
            R.layout.advice_comment_list, new String[] {
                    TAG_COMMENT,TAG_USERNAME},
            new int[] {  R.id.advicecom, R.id.cuname}){
        @Override
    public View getView(int pos, View convertView, ViewGroup parent){
        View v = convertView;
        if(v== null){
            LayoutInflater vi = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v=vi.inflate(R.layout.advice_comment_list, null);
        }
        TextView tv = (TextView)v.findViewById(R.id.cuname);
        tv.setText(oslist.get(pos).get(TAG_USERNAME));
        Typeface custom_font3 = Typeface.createFromAsset(getAssets(),
        	      "AvenirNextLTPro-Demi.otf");
        tv.setTypeface(custom_font3);
        
        TextView tvs = (TextView)v.findViewById(R.id.advicecom);
        tvs.setText(oslist.get(pos).get(TAG_COMMENT));
        Typeface custom_font4 = Typeface.createFromAsset(getAssets(),
        	      "AvenirNextLTPro-Regular.otf");
        tvs.setTypeface(custom_font4);
        return v;
    }


};
    setListAdapter(adapter);

    		 }
    		 
    		 
    		
    		 
    		 
    		 
                             
                catch (JSONException e) {
     			 Toast.makeText(getBaseContext(), "Oopss!! Our engineer fell asleep,Please try again Later",
                         Toast.LENGTH_LONG).show();
     			e.printStackTrace();
     		}

     		

     		 
     	 
    	 }	 
                             
                             
    }
    
   
   
    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    } 
    
}
