package elskay.styleadvice;

public class AdviceList {

	private String username;
	private String question;
	private String image;
	private String qid;
	private String userid;
	private String profileimage;
	private String scount;

	public AdviceList() {
		// TODO Auto-generated constructor stub
	}

	public AdviceList(String username, String question ,String image,String qid,String userid,String profileimage,String scount) {
		super();
		this.username = username;
		this.question = question;
		this.image = image;
		this.qid = qid;
		this.userid = userid;
		this.profileimage = profileimage;
		this.scount = scount;
	}


	public String getusername() {
		return username;
	}

	public void setusername(String username) {
		this.username = username;
	}

	public String getquestion() {
		return question;
	}

	public void setquestion(String question) {
		this.question = question;
	}


	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}
	
	public String getqid() {
		return qid;
	}

	public void setqid(String qid) {
		this.qid = qid;
	}
	public String getuserid() {
		return userid;
	}

	public void setuserid(String userid) {
		this.userid = userid;
	}
	
	public String getprofileimage() {
		return profileimage;
	}

	public void setprofileimage(String profileimage) {
		this.profileimage = profileimage;
	}
	
	public String getcount() {
		return scount;
	}

	public void setcount(String scount) {
		this.scount = scount;
	}
	

}
