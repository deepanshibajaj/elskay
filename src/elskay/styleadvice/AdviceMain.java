package elskay.styleadvice;


import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.ParseException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;
import elskay.app.R;


public class AdviceMain extends Fragment {
	
	ArrayList<AdviceList> actorsListd;
	
	AdviceAdapter adapter;
	
	 @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	    }

	/*@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.advice_list); */
	 @Override
	    public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                             Bundle savedInstanceState) {
	        View v = inflater.inflate(R.layout.advice_list, container, false);
	        
		actorsListd = new ArrayList<AdviceList>();
		
		if(isNetworkAvailable()){
            // do network operation here     
			new JSONAsyncTask().execute("http://128.199.176.163/elskayapp/elskayadvice/get_adviceques.php");
         }else{
            Toast.makeText(getContext(), "No Available Network. Please try again later", Toast.LENGTH_LONG).show();
          
         }
		//new JSONAsyncTask().execute("http://128.199.176.163/elskayapp/elskayadvice/get_adviceques.php");
		
		 ImageButton button3 = (ImageButton)v. findViewById(R.id.fab_image_button);
	        
	        
	        button3.setOnClickListener(new View.OnClickListener()
		    {
		    public void onClick(View view) {
		    	
		    	Intent intent = new Intent(getActivity(), AdviceAsk.class);
		    	 startActivity(intent);
		   }
		   }); 
		
		ListView listview = (ListView)v.findViewById(android.R.id.list);
		adapter = new AdviceAdapter(getActivity(), R.layout.advice_list_item, actorsListd);
		
		listview.setAdapter(adapter);
		  return v;
	}


	class JSONAsyncTask extends AsyncTask<String, Void, Boolean> {
		
		ProgressDialog dialog;
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog = new ProgressDialog(getActivity());
			dialog.setMessage("Please give me a moment to open the box of treasure");
			//dialog.setTitle("Connecting server");
			dialog.show();
			dialog.setCancelable(false);
		}
		
		@Override
		protected Boolean doInBackground(String... urls) {
			try {
				
				//------------------>>
				HttpGet httppost = new HttpGet(urls[0]);
				HttpClient httpclient = new DefaultHttpClient();
				HttpResponse response = httpclient.execute(httppost);

				// StatusLine stat = response.getStatusLine();
				int status = response.getStatusLine().getStatusCode();

				if (status == 200) {
					HttpEntity entity = response.getEntity();
					String data = EntityUtils.toString(entity);
					
				
					JSONObject jsono = new JSONObject(data);
					JSONArray jarray = jsono.getJSONArray("advice");
					
					for (int i = 0; i < jarray.length(); i++) {
						JSONObject object = jarray.getJSONObject(i);
					
						AdviceList actor = new AdviceList();
						actor.setqid(object.getString("qid"));
					    actor.setquestion(object.getString("advicequestion"));
						actor.setImage(object.getString("adviceimage"));
						actor.setusername(object.getString("username"));
						actor.setuserid(object.getString("unique_id"));
						actor.setprofileimage(object.getString("profileimage"));
						actor.setcount(object.getString("count"));
						
						
						actorsListd.add(actor);
					}
					return true;
				}
				
				//------------------>>
				
			} catch (ParseException e1) {
				e1.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return false;
		}
		
		protected void onPostExecute(Boolean result) {
			dialog.cancel();
			adapter.notifyDataSetChanged();
			if(result == false)
				Toast.makeText(getActivity(), "Oopss!! Our engineer fell asleep,Please try again Later", Toast.LENGTH_LONG).show();

		}
	}
	
	 public boolean isNetworkAvailable() {
	       ConnectivityManager connectivityManager = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
	       NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	       return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	   }

	
	
}
