
package elskay.styleadvice;


import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.viewpagerindicator.TitlePageIndicator;

import elskay.app.R;
import elskay.category.ECategories;
import elskay.product.EDemoProduct;

public class AdviceTabs extends Fragment  {

	private FragmentActivity myContext;
	 TitlePageIndicator titleIndicator;
	private static final CharSequence[] TITLES = new CharSequence[] { "Title 1", "Title 2" };
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
	
	@Override
	public void onAttach(Activity activity) {
	    myContext=(FragmentActivity) activity;
	    super.onAttach(activity);
	}


	 @Override
	    public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                             Bundle savedInstanceState) {
	        View v = inflater.inflate(R.layout.advice_viewpager_tabs, container, false);
	         

        ViewPager pager = (ViewPager) v.findViewById(R.id.viewpager);
        PagerTabStrip Title = (PagerTabStrip)v.findViewById(R.id.viewpagertabstrip);
        Title.setTabIndicatorColor(Color.parseColor("#ffffff"));
        Typeface custom_font = Typeface.createFromAsset(getActivity().getAssets(), "AvenirNextLTPro-Regular.otf");
        for (int i = 0; i < Title.getChildCount(); ++i) {
            View nextChild = Title.getChildAt(i);
            if (nextChild instanceof TextView) {
               TextView textViewToConvert = (TextView) nextChild;
               textViewToConvert.setTypeface(custom_font);
               
               
            }
        }
        
       

        pager.setAdapter(new MyPagerAdapter(getChildFragmentManager()));
        
       
        return v;
    }

    private class MyPagerAdapter extends FragmentPagerAdapter {

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

       /* @Override
        public Fragment getItem(int pos) {
            switch(pos) {

            case 0: return new AdviceMain();
            case 1: return new FeedElskay();
            
            default: return new AdviceMain();
            }
        }

        @Override
        public int getCount() {
            return 2;
        }       
    }*/
    
      @Override
    public Fragment getItem(int index) {
 
        switch (index) {
        case 0:
            // Top Rated fragment activity
            return new AdviceMain();
        case 1:
            // Games fragment activity
            return new AdviceAsked();
      
            
       
        }
        
 
        return null;
    }
      
      @Override
      public int getCount() {
          // get item count - equal to number of tabs
          return 2;
      }
      @Override
      public CharSequence getPageTitle(int position) {
    	  switch (position) {
          case 0:
              return "Advice";
          case 1:
              return "Asked";
          
         
      }
    	   return null;
      }
      
    }
}